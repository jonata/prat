# Prat

## Preparando o ambiente do sistema operacional

### Ubuntu (18.04)

Python 3 já deve estar instalado por padrão, mas se não estiver:

```
sudo apt install python3
```

Para usar o modulo exchangelib será necessário instalar o código do kerberos

```
sudo apt install libkrb5-dev
```

Talvez seja necessario instalar o codigo do ssl

```
sudo apt install libssl-dev
```

Instale o git se não estiver instalado:

```
sudo apt install git
```

### macOS (High Sierra)

Softwares necessários:
* Xcode (pelo menos as ferramentas de linha de comando)
* Homebrew
* Python 3
* git
* Ghostscript

Certifique-se de que os softwares acima estão instalados e o usuário tem poder administrativo.

Instalando o python3

```
brew install python3
```

Instale o git se não estiver instalado:

```
brew install git
```

Instale o ghostscript se não estiver instalado:

```
brew install ghostscript
```

## Preparando o ambiente Python

Módulos Python necessários:
* PySide2
* PyInstaller
* ghostscript
* google-api-python-client
* oauth2client
* exchangelib
* beautifulsoup4

```
pip3 install PySide2 pyinstaller ghostscript google-api-python-client oauth2client exchangelib beautifulsoup4
```

Baixe o código do Prat:

```
git clone git@gitlab.com:jonata/prat.git
```

## Rodando o Prat do código

```
cd prat
python3 prat.py
```

## Gerando pacotes do Prat

Para gerar o pacote de instalação snap, há um script chamado "generate_packages.py" que vai atualizar o código, gerar o snap e subir:

```
./generate_packages.py
```

## Tradução

A tradução dos textos da interface do Prat pode ser feita pelo [Weblate](https://weblate.org/)