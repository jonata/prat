#!/usr/bin/env python3

import os
import sys
import json
from deep_translator import GoogleTranslator

new_langs = []

if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        new_langs.append(arg)

SOURCE_LANGUAGE = 'en'
LIST_OF_STRINGS = ['meps_language_mnemonic', 'language_name']

ACTUAL_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
MODULES_DIRECTORY = os.path.join(ACTUAL_DIRECTORY,'prat', 'modules')
LOCALE_DIRECTORY = os.path.join(ACTUAL_DIRECTORY,'prat', 'locale')

if os.path.isdir(MODULES_DIRECTORY) and os.path.isdir(LOCALE_DIRECTORY):
    for source_file in os.listdir(os.path.join(MODULES_DIRECTORY)):
        if not source_file.lower().startswith('__'):
            source_content = open(os.path.join(MODULES_DIRECTORY, source_file)).read()
            p = 0
            string_found = False
            while p < len(source_content):
                if source_content[p:p+3] in ['_("', "_('"]:
                    string_found = source_content[p+2:p+3]
                    p += 2
                elif string_found:
                    if source_content[p:p+1] == string_found[:1]:
                        string_found += source_content[p:p+1]
                        LIST_OF_STRINGS.append(string_found[1:-1])
                        string_found = False
                    else:
                        string_found += source_content[p:p+1]
                p += 1

    for translation_file in os.listdir(os.path.join(LOCALE_DIRECTORY)):
        if translation_file.lower().endswith('.json'):
            json_file = json.load(open(os.path.join(LOCALE_DIRECTORY, translation_file)))
            locale = os.path.splitext(os.path.basename(os.path.join(LOCALE_DIRECTORY, translation_file)))[0]

            if locale != 'en':
                for string in LIST_OF_STRINGS:
                    if not string in json_file:
                        json_file[string] = GoogleTranslator(source='en', target=locale.split('-',1)[0]).translate(string)
                json_file_list = list(json_file)
                for string in json_file_list:
                    if not string in LIST_OF_STRINGS:
                        del json_file[string]

                open(os.path.join(LOCALE_DIRECTORY, translation_file), 'w').write(json.dumps(json_file, indent=4, sort_keys=True, ensure_ascii=False))

