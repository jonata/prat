#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#######################################################################
#
# Prat
#
#######################################################################

__version__ = '21.06.01.01'

__appname__ = 'Prat'
__domain__ = 'prat.jonata.org'
__desktopid__ = 'org.jonata.Prat'
# __appid__ = ''

__author__ = 'Jonatã Bolzan Loss'
__email__ = 'prat@jonata.org'
__website__ = 'https://prat.jonata.org'
__bugreport__ = 'https://gitlab.com/jonata/prat/issues'

__ispypi__ = False
