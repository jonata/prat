#!/usr/bin/env python3

import os
import sys
from datetime import datetime

from PySide6.QtWidgets import QApplication, QWidget, QVBoxLayout, QFrame, QMainWindow, QHBoxLayout, QPushButton, QSizePolicy, QLabel, QStackedWidget
from PySide6.QtGui import QFontDatabase
from PySide6.QtCore import QSize, Qt, QPropertyAnimation, QEasingCurve, QParallelAnimationGroup

import prat
from prat.modules import public_talks, translation, midweek_meeting, start_screen, deliver, date, settings, indices, weekend_meeting, welcome_screen, congregation, publishers
from prat.modules.paths import PATH_PRAT_GRAPHICS, get_graphics_path


MENU_WIDTH = 240
LEFT_BOX_WIDTH = 240
RIGHT_BOX_WIDTH = 240
TIME_ANIMATION = 500
BTN_LEFT_BOX_COLOR = "background-color: #292e36;"
BTN_RIGHT_BOX_COLOR = "background-color: #393d45;"
MENU_SELECTED_STYLESHEET = 'border-left: 22px solid qlineargradient(spread:pad, x1:0.034, y1:0, x2:0.216, y2:0, stop:0.499 #0a0b0d, stop:0.5 #00b4bcc4); background-color: #292e36; '


class mainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        # Define default values
        self.settings = settings.load_settings()
        self.selected_date = datetime.today()
        self.selected_module = None
        self.index_w = indices.get_w_index()
        self.index_clm = indices.get_clm_index()
        translation.load_translation_files()
        self.selected_language = self.settings.get('language', 'en')
        translation.set_language(self.selected_language)
        self.assignments = {}
        self.publishers_list = publishers.load_list()
        self.list_of_deliverables = {}
        deliver.update_deliverables_list(self)
        self.public_talks_config = public_talks.load_settings()

        self.main_stylesheet = QWidget(self)
        self.main_stylesheet.setObjectName(u"main_stylesheet")

        QFontDatabase.addApplicationFont(os.path.join(PATH_PRAT_GRAPHICS, 'Ubuntu-RI.ttf'))
        QFontDatabase.addApplicationFont(os.path.join(PATH_PRAT_GRAPHICS, 'Ubuntu-R.ttf'))
        QFontDatabase.addApplicationFont(os.path.join(PATH_PRAT_GRAPHICS, 'Ubuntu-B.ttf'))
        QFontDatabase.addApplicationFont(os.path.join(PATH_PRAT_GRAPHICS, 'Ubuntu-BI.ttf'))

        self.main_stylesheet.setStyleSheet(open(os.path.join(PATH_PRAT_GRAPHICS, 'light.qss')).read().replace('PATH_PRAT_GRAPHICS/', get_graphics_path('_')[:-1]))

        self.main_vl = QVBoxLayout(self.main_stylesheet)
        self.main_vl.setObjectName(u"main_vl")
        self.main_vl.setContentsMargins(0, 0, 0, 0)
        # self.main_vl.setSizeConstraint(QLayout.SetNoConstraint)

        self.main_frame = QWidget(self.main_stylesheet)
        self.main_frame.setObjectName(u"main_frame")

        self.main_layout = QHBoxLayout(self.main_frame)
        self.main_layout.setSpacing(0)
        self.main_layout.setObjectName(u"main_layout")
        self.main_layout.setContentsMargins(0, 0, 0, 0)

        self.main_menu_fr = QWidget(self.main_frame)
        self.main_menu_fr.setObjectName(u"main_menu_fr")
        self.main_menu_fr.setMinimumWidth(60)
        self.main_menu_fr.setMaximumWidth(60)

        self.main_menu_vl = QVBoxLayout(self.main_menu_fr)
        self.main_menu_vl.setSpacing(0)
        self.main_menu_vl.setObjectName(u"main_menu_vl")
        self.main_menu_vl.setContentsMargins(0, 0, 0, 0)

        self.main_menu_frame = QWidget(self.main_menu_fr)
        self.main_menu_frame.setObjectName(u"main_menu_frame")

        self.main_menu_vlayout = QVBoxLayout(self.main_menu_frame)
        self.main_menu_vlayout.setSpacing(0)
        self.main_menu_vlayout.setObjectName(u"main_menu_vlayout")
        self.main_menu_vlayout.setContentsMargins(0, 0, 0, 0)

        start_screen.add_menu_widgets(self)

        self.modules_menu = QFrame(self.main_menu_frame)
        self.modules_menu.setObjectName(u"modules_menu")
        self.modules_menu.setFrameShape(QFrame.NoFrame)
        self.modules_menu.setFrameShadow(QFrame.Raised)

        self.modules_menu_vl = QVBoxLayout(self.modules_menu)
        self.modules_menu_vl.setSpacing(0)
        self.modules_menu_vl.setObjectName(u"modules_menu_vl")
        self.modules_menu_vl.setContentsMargins(0, 0, 0, 0)

        midweek_meeting.add_menu_widgets(self)
        weekend_meeting.add_menu_widgets(self)
        public_talks.add_menu_widgets(self)
        congregation.add_menu_widgets(self)
        publishers.add_menu_widgets(self)
        settings.add_menu_widgets(self)

        self.main_menu_vlayout.addWidget(self.modules_menu, 0, Qt.AlignTop)

        deliver.add_menu_widgets(self)

        self.main_menu_vl.addWidget(self.main_menu_frame)

        self.main_layout.addWidget(self.main_menu_fr)

        deliver.add_widgets(self)

        self.content_frame = QFrame(self.main_frame)
        self.content_frame.setObjectName(u"content_frame")
        self.content_frame.setFrameShape(QFrame.NoFrame)
        self.content_frame.setFrameShadow(QFrame.Raised)

        self.content_frame_vbox = QVBoxLayout(self.content_frame)
        self.content_frame_vbox.setSpacing(0)
        self.content_frame_vbox.setObjectName(u"content_frame_vbox")
        self.content_frame_vbox.setContentsMargins(0, 0, 0, 0)

        self.content_frame_topbar = QFrame(self.content_frame)
        self.content_frame_topbar.setObjectName(u"content_frame_topbar")
        self.content_frame_topbar.setMinimumSize(QSize(0, 50))
        self.content_frame_topbar.setMaximumSize(QSize(16777215, 50))
        self.content_frame_topbar.setFrameShape(QFrame.NoFrame)
        self.content_frame_topbar.setFrameShadow(QFrame.Raised)

        self.content_frame_topbar_hbox = QHBoxLayout(self.content_frame_topbar)
        self.content_frame_topbar_hbox.setSpacing(0)
        self.content_frame_topbar_hbox.setObjectName(u"content_frame_topbar_hbox")
        self.content_frame_topbar_hbox.setContentsMargins(0, 0, 10, 0)

        self.content_frame_topbar_title = QFrame(self.content_frame_topbar)
        self.content_frame_topbar_title.setObjectName(u"content_frame_topbar_title")

        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.content_frame_topbar_title.sizePolicy().hasHeightForWidth())
        self.content_frame_topbar_title.setSizePolicy(sizePolicy1)
        self.content_frame_topbar_title.setFrameShape(QFrame.NoFrame)
        self.content_frame_topbar_title.setFrameShadow(QFrame.Raised)

        self.content_frame_topbar_title_hbox = QHBoxLayout(self.content_frame_topbar_title)
        self.content_frame_topbar_title_hbox.setObjectName(u"content_frame_topbar_title_hbox")
        self.content_frame_topbar_title_hbox.setContentsMargins(0, 0, 9, 0)

        self.content_frame_topbar_title_label = QLabel(self.content_frame_topbar_title)
        self.content_frame_topbar_title_label.setObjectName(u"content_frame_topbar_title_label")

        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.content_frame_topbar_title_label.sizePolicy().hasHeightForWidth())
        self.content_frame_topbar_title_label.setSizePolicy(sizePolicy2)
        self.content_frame_topbar_title_label.setMaximumSize(QSize(16777215, 45))
        self.content_frame_topbar_title_label.setAlignment(Qt.AlignLeading | Qt.AlignLeft | Qt.AlignVCenter)

        self.content_frame_topbar_title_hbox.addWidget(self.content_frame_topbar_title_label)

        self.content_frame_topbar_hbox.addWidget(self.content_frame_topbar_title)

        date.load_widgets(self)

        # publishers.add_menu_widgets(self)

        # self.dateBox.raise_()
        # self.leftBox.raise_()
        # self.rightButtons.raise_()

        self.content_frame_vbox.addWidget(self.content_frame_topbar)

        self.contentBottom = QFrame(self.content_frame)
        self.contentBottom.setObjectName(u"contentBottom")
        self.contentBottom.setFrameShape(QFrame.NoFrame)
        self.contentBottom.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.contentBottom)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.content = QFrame(self.contentBottom)
        self.content.setObjectName(u"content")
        self.content.setFrameShape(QFrame.NoFrame)
        self.content.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.content)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.pagesContainer = QFrame(self.content)
        self.pagesContainer.setObjectName(u"pagesContainer")
        self.pagesContainer.setStyleSheet(u"")
        self.pagesContainer.setFrameShape(QFrame.NoFrame)
        self.pagesContainer.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.pagesContainer)
        self.verticalLayout_15.setSpacing(0)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.verticalLayout_15.setContentsMargins(10, 10, 10, 10)
        self.stackedWidget = QStackedWidget(self.pagesContainer)
        self.stackedWidget.setObjectName(u"stackedWidget")

        self.verticalLayout_15.addWidget(self.stackedWidget)
        self.horizontalLayout_4.addWidget(self.pagesContainer)

        publishers.add_panel_widgets(self)

        self.verticalLayout_6.addWidget(self.content)

        self.content_frame_vbox.addWidget(self.contentBottom)

        self.main_layout.addWidget(self.content_frame)

        self.main_vl.addWidget(self.main_frame)

        start_screen.add_widgets(self)
        welcome_screen.add_widgets(self)
        midweek_meeting.add_widgets(self)
        weekend_meeting.add_widgets(self)
        public_talks.add_widgets(self)
        congregation.add_widgets(self)
        publishers.add_widgets(self)
        settings.add_widgets(self)
        indices.add_widgets(self)

        self.setCentralWidget(self.main_stylesheet)

        self.setWindowTitle('Prat')

        self.translate_widgets()

        # self.setMinimumSize(QSize(800, 450))
        self.show()

        start_screen.main_menu_toggle_button_clicked(self)

    def closeEvent(self, event):
        if 'save_function' in self.selected_module:
            self.selected_module['save_function'](self)
        settings.save_settings(self.settings)
        publishers.save_list(self.publishers_list)
        self.close()

    def toggle_menu(self, enable):
        if enable:
            width = self.main_menu_fr.width()
            maxExtend = MENU_WIDTH
            standard = 60

            if width == 60:
                widthExtended = maxExtend
            else:
                widthExtended = standard

            self.animation = QPropertyAnimation(self.main_menu_fr, b"minimumWidth")
            self.animation.setDuration(TIME_ANIMATION)
            self.animation.setStartValue(width)
            self.animation.setEndValue(widthExtended)
            self.animation.setEasingCurve(QEasingCurve.InOutQuart)
            self.animation.start()

        self.content_frame_topbar_title_label.setText(translation._(self.selected_module['title']))

        date.showhide_date_widgets(self)

    def toggledLeftBox(self, enable):
        if enable:
            width = self.deliverables_box_frame.width()
            widthRightBox = self.extraRightBox.width()
            # maxExtend = LEFT_BOX_WIDTH
            color = BTN_LEFT_BOX_COLOR
            # standard = 0

            style = self.deliverables_main_menu_bottom_button.styleSheet()

            if width == 0:
                # widthExtended = maxExtend
                self.deliverables_main_menu_bottom_button.setStyleSheet(style + color)
                if widthRightBox != 0:
                    style = self.settingsTopBtn.styleSheet()
                    self.settingsTopBtn.setStyleSheet(style.replace(BTN_RIGHT_BOX_COLOR, ''))
            else:
                # widthExtended = standard
                self.deliverables_main_menu_bottom_button.setStyleSheet(style.replace(color, ''))

        self.start_box_animation(width, widthRightBox, "left")

    def toggleRightBox(self, enable):
        if enable:
            width = self.extraRightBox.width()
            widthLeftBox = self.deliverables_box_frame.width()
            # maxExtend = RIGHT_BOX_WIDTH
            color = BTN_RIGHT_BOX_COLOR
            # standard = 0

            style = self.settingsTopBtn.styleSheet()

            if width == 0:
                # widthExtended = maxExtend
                self.settingsTopBtn.setStyleSheet(style + color)
                if widthLeftBox != 0:
                    style = self.deliverables_main_menu_bottom_button.styleSheet()
                    self.deliverables_main_menu_bottom_button.setStyleSheet(style.replace(BTN_LEFT_BOX_COLOR, ''))
            else:
                # widthExtended = standard
                self.settingsTopBtn.setStyleSheet(style.replace(color, ''))

            self.start_box_animation(widthLeftBox, width, "right")

    def start_box_animation(self, left_box_width, right_box_width, direction):
        right_width = 0
        left_width = 0

        if left_box_width == 0 and direction == "left":
            left_width = 340
        else:
            left_width = 0
        if right_box_width == 0 and direction == "right":
            right_width = 240
        else:
            right_width = 0

        self.left_box = QPropertyAnimation(self.deliverables_box_frame, b"minimumWidth")
        self.left_box.setDuration(TIME_ANIMATION)
        self.left_box.setStartValue(left_box_width)
        self.left_box.setEndValue(left_width)
        self.left_box.setEasingCurve(QEasingCurve.InOutQuart)

        self.right_box = QPropertyAnimation(self.extraRightBox, b"minimumWidth")
        self.right_box.setDuration(TIME_ANIMATION)
        self.right_box.setStartValue(right_box_width)
        self.right_box.setEndValue(right_width)
        self.right_box.setEasingCurve(QEasingCurve.InOutQuart)

        self.group = QParallelAnimationGroup()
        self.group.addAnimation(self.left_box)
        self.group.addAnimation(self.right_box)
        self.group.start()

    def selectMenu(self, getStyle):
        select = getStyle + MENU_SELECTED_STYLESHEET
        return select

    def deselectMenu(self, getStyle):
        deselect = getStyle.replace(MENU_SELECTED_STYLESHEET, "")
        return deselect

    def selectStandardMenu(self, widget):
        for w in self.modules_menu.findChildren(QPushButton):
            if w.objectName() == widget:
                w.setStyleSheet(self.selectMenu(w.styleSheet()))

    def resetStyle(self, widget):
        for w in self.modules_menu.findChildren(QPushButton):
            if w.objectName() != widget:
                w.setStyleSheet(self.deselectMenu(w.styleSheet()))
        for w in self.main_menu_toggle_frame.findChildren(QPushButton):
            if w.objectName() != widget:
                w.setStyleSheet(self.deselectMenu(w.styleSheet()))

    def translate_widgets(self):
        start_screen.translate_widgets(self)
        welcome_screen.translate_widgets(self)
        midweek_meeting.translate_widgets(self)
        weekend_meeting.translate_widgets(self)
        public_talks.translate_widgets(self)
        congregation.translate_widgets(self)
        publishers.translate_widgets(self)
        settings.translate_widgets(self)
        deliver.translate_widgets(self)
        indices.translate_widgets(self)


def main():
    # qt_set_sequence_auto_mnemonic(False)

    # if hasattr(Qt, 'AA_EnableHighDpiScaling'):
    #     QGuiApplication.setAttribute(Qt.AA_EnableHighDpiScaling, True)
    # if hasattr(Qt, 'AA_Use96Dpi'):
    #     QGuiApplication.setAttribute(Qt.AA_Use96Dpi, True)
    # if hasattr(Qt, 'AA_ShareOpenGLContexts'):
    #     fmt = QSurfaceFormat()
    #     fmt.setDepthBufferSize(24)
    #     QSurfaceFormat.setDefaultFormat(fmt)
    #     QGuiApplication.setAttribute(Qt.AA_ShareOpenGLContexts, True)

    # # if sys.platform == 'darwin':
    # #     qApp.setStyle('Fusion')

    app = QApplication(sys.argv)
    app.setApplicationName(prat.__appname__)
    app.setApplicationVersion(prat.__version__)
    app.setDesktopFileName(prat.__desktopid__)
    app.setOrganizationDomain(prat.__domain__)
    app.setQuitOnLastWindowClosed(True)

    win = mainWindow()
    win.stylename = app.style().objectName().lower()
    # app.setActivationWindow(win)
    # app.messageReceived.connect(win.file_opener)
    # app.aboutToQuit.connect(mainWindow.cleanup)

    exit_code = app.exec()
    # if exit_code == mainWindow.EXIT_CODE_REBOOT:
    #     if sys.platform == 'win32':
    #         if hasattr(win.cutter, 'mpvWidget'):
    #             win.close()
    #         QProcess.startDetached('"{}"'.format(qApp.applicationFilePath()))
    #     else:
    #         QProcess.startDetached(' '.join(sys.argv))
    sys.exit(exit_code)


if __name__ == '__main__':
    main()