#!/usr/bin/env python3

import os
import json

from PySide6.QtWidgets import QCheckBox, QGridLayout, QLineEdit, QWidget, QVBoxLayout, QPushButton, QSizePolicy, QScrollArea, QFrame, QHBoxLayout, QLabel, QFileDialog, QComboBox
from PySide6.QtGui import QCursor
from PySide6.QtCore import Qt

from prat.modules.translation import _, get_list_of_weekdays
from prat.modules import indices
from prat.modules.paths import PATH_PRAT_USER_CONFIG_FILE, PATH_PRAT_USER_CONFIG_FOLDER, PATH_REAL_HOME, get_graphics_path
from prat.special_widgets import QComboBoxNoWheel, QLineEditDC


MODULE_INFO = {
                'title': _('Congregation'),
                'name':'congregation',
                'date_enabled': False
              }


def add_menu_widgets(self):
    self.modules_menu_button_congregation = QPushButton(self.modules_menu)
    self.modules_menu_button_congregation.setObjectName(u"modules_menu_button_congregation")
    self.modules_menu_button_congregation.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.modules_menu_button_congregation.sizePolicy().hasHeightForWidth())
    self.modules_menu_button_congregation.setSizePolicy(sizePolicy)
    self.modules_menu_button_congregation.setMinimumHeight(45)
    self.modules_menu_button_congregation.setCursor(QCursor(Qt.PointingHandCursor))
    self.modules_menu_button_congregation.setLayoutDirection(Qt.LeftToRight)
    self.modules_menu_button_congregation.setStyleSheet('background-image: url("' + get_graphics_path('congregation_icon.svg') + '");')
    self.modules_menu_button_congregation.clicked.connect(lambda: modules_menu_button_congregation_clicked(self))

    self.modules_menu_vl.addWidget(self.modules_menu_button_congregation)

def add_widgets(self):
    self.congregation_page = QWidget()
    self.congregation_page.setObjectName(u"congregation")

    self.congregation_vbox = QVBoxLayout(self.congregation_page)
    self.congregation_vbox.setObjectName(u"congregation_vbox")

    self.congregation_scroll = QScrollArea(self.congregation_page)
    self.congregation_scroll.setObjectName(u"congregation_scroll")
    self.congregation_scroll.setWidgetResizable(True)
    self.congregation_scroll.setFrameShape(QFrame.NoFrame)
    self.congregation_scroll.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

    self.congregation_scroll_widget = QWidget()
    self.congregation_scroll_widget.setObjectName(u"congregation_scroll_widget")

    sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
    sizePolicy3.setHorizontalStretch(0)
    sizePolicy3.setVerticalStretch(0)
    sizePolicy3.setHeightForWidth(self.congregation_scroll_widget.sizePolicy().hasHeightForWidth())
    self.congregation_scroll_widget.setSizePolicy(sizePolicy3)

    self.congregation_scroll_widget_vbox = QVBoxLayout(self.congregation_scroll_widget)
    self.congregation_scroll_widget_vbox.setObjectName(u"congregation_scroll_widget_vbox")


    self.congregation_name_frame = QWidget(self.congregation_scroll_widget)

    self.congregation_name_grid = QGridLayout(self.congregation_name_frame)

    self.congregation_name_label = QLabel(self.congregation_name_frame)
    self.congregation_name_label.setObjectName(u"congregation_name_label")
    self.congregation_name_label.setProperty('class', 'small_label')

    self.congregation_name_grid.addWidget(self.congregation_name_label, 0, 0, 1, 1)

    self.congregation_name = QLineEditDC(self.congregation_name_frame)
    # self.congregation_name.setEditable(True)
    self.congregation_name.editingFinished.connect(lambda: congregation_name_editingfinished(self))

    self.congregation_name_grid.addWidget(self.congregation_name, 1, 0, 1, 1)

    self.congregation_scroll_widget_vbox.addWidget(self.congregation_name_frame)

    self.congregation_midweek_meeting_frame = QWidget(self.congregation_scroll_widget)
    self.congregation_midweek_meeting_frame.setObjectName('congregation_midweek_meeting_frame')

    self.congregation_midweek_meeting_vbox = QVBoxLayout(self.congregation_midweek_meeting_frame)

    self.congregation_midweek_meeting_main_label = QLabel(self.congregation_midweek_meeting_frame)
    self.congregation_midweek_meeting_main_label.setObjectName(u"congregation_midweek_meeting_main_label")
    self.congregation_midweek_meeting_main_label.setProperty('class', 'big_label')

    self.congregation_midweek_meeting_vbox.addWidget(self.congregation_midweek_meeting_main_label)

    self.congregation_midweek_meeting_weekday_and_time_frame = QWidget(self.congregation_scroll_widget)

    self.congregation_midweek_meeting_weekday_and_time_grid = QGridLayout(self.congregation_midweek_meeting_weekday_and_time_frame)

    self.congregation_midweek_meeting_weekday_label = QLabel(self.congregation_midweek_meeting_weekday_and_time_frame)
    self.congregation_midweek_meeting_weekday_label.setObjectName(u"congregation_midweek_meeting_weekday_label")
    self.congregation_midweek_meeting_weekday_label.setProperty('class', 'small_label')

    self.congregation_midweek_meeting_weekday_and_time_grid.addWidget(self.congregation_midweek_meeting_weekday_label, 0, 0, 1, 1)

    self.congregation_midweek_meeting_weekday = QComboBoxNoWheel(self.congregation_midweek_meeting_weekday_and_time_frame)
    # self.congregation_midweek_meeting_weekday.setEditable(True)
    self.congregation_midweek_meeting_weekday.activated.connect(lambda: congregation_midweek_meeting_weekday_selected(self))

    self.congregation_midweek_meeting_weekday_and_time_grid.addWidget(self.congregation_midweek_meeting_weekday, 1, 0, 1, 1)

    self.congregation_midweek_meeting_time_label = QLabel(self.congregation_midweek_meeting_weekday_and_time_frame)
    self.congregation_midweek_meeting_time_label.setObjectName(u"congregation_midweek_meeting_time_label")
    self.congregation_midweek_meeting_time_label.setProperty('class', 'small_label')

    self.congregation_midweek_meeting_weekday_and_time_grid.addWidget(self.congregation_midweek_meeting_time_label, 0, 1, 1, 1)

    self.congregation_midweek_meeting_time = QLineEdit(self.congregation_midweek_meeting_weekday_and_time_frame)
    self.congregation_midweek_meeting_time.editingFinished.connect(lambda: congregation_midweek_meeting_time_edited(self))

    self.congregation_midweek_meeting_weekday_and_time_grid.addWidget(self.congregation_midweek_meeting_time, 1, 1, 1, 1)

    self.congregation_midweek_meeting_weekday_and_time_grid.setColumnStretch(2, 1)

    self.congregation_midweek_meeting_vbox.addWidget(self.congregation_midweek_meeting_weekday_and_time_frame)

    self.congregation_scroll_widget_vbox.addWidget(self.congregation_midweek_meeting_frame)

    self.congregation_weekend_meeting_frame = QWidget(self.congregation_scroll_widget)
    self.congregation_weekend_meeting_frame.setObjectName('congregation_weekend_meeting_frame')

    self.congregation_weekend_meeting_vbox = QVBoxLayout(self.congregation_weekend_meeting_frame)

    self.congregation_weekend_meeting_main_label = QLabel(self.congregation_weekend_meeting_frame)
    self.congregation_weekend_meeting_main_label.setObjectName(u"congregation_weekend_meeting_main_label")
    self.congregation_weekend_meeting_main_label.setProperty('class', 'big_label')

    self.congregation_weekend_meeting_vbox.addWidget(self.congregation_weekend_meeting_main_label)

    self.congregation_weekend_meeting_weekday_and_time_frame = QWidget(self.congregation_scroll_widget)

    self.congregation_weekend_meeting_weekday_and_time_grid = QGridLayout(self.congregation_weekend_meeting_weekday_and_time_frame)

    self.congregation_weekend_meeting_weekday_label = QLabel(self.congregation_weekend_meeting_weekday_and_time_frame)
    self.congregation_weekend_meeting_weekday_label.setObjectName(u"congregation_weekend_meeting_weekday_label")
    self.congregation_weekend_meeting_weekday_label.setProperty('class', 'small_label')

    self.congregation_weekend_meeting_weekday_and_time_grid.addWidget(self.congregation_weekend_meeting_weekday_label, 0, 0, 1, 1)

    self.congregation_weekend_meeting_weekday = QComboBoxNoWheel(self.congregation_weekend_meeting_weekday_and_time_frame)
    self.congregation_weekend_meeting_weekday.activated.connect(lambda: congregation_weekend_meeting_weekday_selected(self))

    self.congregation_weekend_meeting_weekday_and_time_grid.addWidget(self.congregation_weekend_meeting_weekday, 1, 0, 1, 1)

    self.congregation_weekend_meeting_time_label = QLabel(self.congregation_weekend_meeting_weekday_and_time_frame)
    self.congregation_weekend_meeting_time_label.setObjectName(u"congregation_weekend_meeting_time_label")
    self.congregation_weekend_meeting_time_label.setProperty('class', 'small_label')

    self.congregation_weekend_meeting_weekday_and_time_grid.addWidget(self.congregation_weekend_meeting_time_label, 0, 1, 1, 1)

    self.congregation_weekend_meeting_time = QLineEdit(self.congregation_weekend_meeting_weekday_and_time_frame)
    self.congregation_weekend_meeting_time.editingFinished.connect(lambda: congregation_weekend_meeting_time_edited(self))

    self.congregation_weekend_meeting_weekday_and_time_grid.addWidget(self.congregation_weekend_meeting_time, 1, 1, 1, 1)

    self.congregation_weekend_meeting_weekday_and_time_grid.setColumnStretch(2, 1)

    self.congregation_weekend_meeting_vbox.addWidget(self.congregation_weekend_meeting_weekday_and_time_frame)

    self.congregation_scroll_widget_vbox.addWidget(self.congregation_weekend_meeting_frame)

    self.congregation_scroll.setWidget(self.congregation_scroll_widget)

    self.congregation_vbox.addWidget(self.congregation_scroll)

    self.stackedWidget.addWidget(self.congregation_page)


def modules_menu_button_congregation_clicked(self):
    self.selected_module = MODULE_INFO
    update_widgets(self)
    self.stackedWidget.setCurrentWidget(self.congregation_page)
    self.toggle_menu(True)
    self.resetStyle(self.modules_menu_button_congregation.objectName())
    self.modules_menu_button_congregation.setStyleSheet(self.selectMenu(self.modules_menu_button_congregation.styleSheet()))


def translate_widgets(self):
    self.modules_menu_button_congregation.setText(_(MODULE_INFO['title']))
    self.congregation_midweek_meeting_main_label.setText(_('Midweek Meeting'))
    self.congregation_midweek_meeting_weekday_label.setText(_('Weekday'))
    self.congregation_midweek_meeting_weekday.clear()
    list_of_weekdays = get_list_of_weekdays(self.selected_language)
    self.congregation_midweek_meeting_weekday.clear()
    self.congregation_midweek_meeting_weekday.addItems(list_of_weekdays)
    self.congregation_midweek_meeting_time_label.setText(_('Starting time'))
    self.congregation_weekend_meeting_weekday_label.setText(_('Weekday'))
    self.congregation_weekend_meeting_main_label.setText(_('Weekend Meeting'))
    self.congregation_weekend_meeting_weekday.clear()
    self.congregation_weekend_meeting_weekday.addItems(list_of_weekdays)
    self.congregation_weekend_meeting_time_label.setText(_('Starting time'))
    self.congregation_name_label.setText(_('Congregation name'))


def update_widgets(self):
    self.congregation_midweek_meeting_time.setText(self.settings.get('midweek_meeting_time', ''))
    self.congregation_midweek_meeting_weekday.setCurrentIndex(self.settings.get('midweek_meeting_weekday', 3))
    self.congregation_weekend_meeting_time.setText(self.settings.get('weekend_meeting_time', ''))
    self.congregation_weekend_meeting_weekday.setCurrentIndex(self.settings.get('weekend_meeting_weekday', 5))
    self.congregation_name.setText(self.settings.get('congregation_name', ''))
    self.congregation_name.setReadOnly(bool(self.settings.get('congregation_name', '')))
    self.congregation_name.setStyleSheet(self.congregation_name.styleSheet())


def congregation_midweek_meeting_time_edited(self):
    self.settings['midweek_meeting_time'] = self.congregation_midweek_meeting_time.text()


def congregation_midweek_meeting_weekday_selected(self):
    self.settings['midweek_meeting_weekday'] = self.congregation_midweek_meeting_weekday.currentIndex()


def congregation_weekend_meeting_time_edited(self):
    self.settings['weekend_meeting_time'] = self.congregation_weekend_meeting_time.text()


def congregation_weekend_meeting_weekday_selected(self):
    self.settings['weekend_meeting_weekday'] = self.congregation_weekend_meeting_weekday.currentIndex()


def congregation_name_editingfinished(self):
    self.settings['congregation_name'] = self.congregation_name.text()
    update_widgets(self)
    self.congregation_name.setStyleSheet(self.congregation_name.styleSheet())