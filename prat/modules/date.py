#!/usr/bin/env python3

from datetime import datetime, timedelta

from PySide6.QtWidgets import QFrame, QHBoxLayout, QPushButton, QLabel
from PySide6.QtGui import QIcon
from PySide6.QtCore import QSize

from prat.modules.translation import _, get_list_of_months
from prat.modules.paths import get_graphics_path

def load_widgets(self):
    self.date_box = QFrame(self.content_frame_topbar)
    self.date_box.setObjectName(u"date_box")
    self.date_box.setFrameShape(QFrame.NoFrame)
    self.date_box.setFrameShadow(QFrame.Raised)

    self.date_box_hbox = QHBoxLayout(self.date_box)
    self.date_box_hbox.setObjectName(u"date_box_hbox")
    self.date_box_hbox.setSpacing(0)

    self.date_back_button = QPushButton(self.date_box)
    icon = QIcon()
    icon.addFile(get_graphics_path('arrow_left.svg'), QSize(), QIcon.Normal, QIcon.Off)
    self.date_back_button.setIcon(icon)
    self.date_back_button.setObjectName(u"date_back_button")
    self.date_back_button.setMinimumSize(QSize(28, 28))
    self.date_back_button.clicked.connect(lambda: date_back_button_clicked(self))
    self.date_box_hbox.addWidget(self.date_back_button)

    self.date_main_label = QLabel(self.date_box)
    self.date_main_label.setObjectName(u"date_main_label")
    self.date_box_hbox.addWidget(self.date_main_label)

    self.date_next_button = QPushButton(self.date_box)
    icon = QIcon()
    icon.addFile(get_graphics_path('arrow_right.svg'), QSize(), QIcon.Normal, QIcon.Off)
    self.date_next_button.setIcon(icon)
    self.date_next_button.setObjectName(u"date_next_button")
    self.date_next_button.setMinimumSize(QSize(28, 28))
    self.date_next_button.clicked.connect(lambda: date_next_button_clicked(self))
    self.date_box_hbox.addWidget(self.date_next_button)

    self.content_frame_topbar_hbox.addWidget(self.date_box)


def date_back_button_clicked(self):
    self.selected_date -= timedelta(days=7)
    date_update(self)


def date_next_button_clicked(self):
    self.selected_date += timedelta(days=7)
    date_update(self)


def date_update(self):
    start, end = get_week_range(self.selected_date)
    list_of_months = get_list_of_months(self.selected_language)

    date1 = '{d}'.format(d=start.day) if start.month == end.month else _('{day} of {monthname}').format(day=start.day, monthname=list_of_months[start.month])
    date2 = _('{day} of {monthname}').format(day=end.day, monthname=list_of_months[end.month])

    final_text = _('{date1} to {date2}').format(date1=date1, date2=date2)

    self.date_main_label.setText(str(final_text))
    self.selected_module['date_function'](self)


def showhide_date_widgets(self):
    if self.selected_module and self.selected_module['date_enabled']:
        self.date_box.setVisible(True)
        date_update(self)
    else:
        self.date_box.setVisible(False)


def get_week_start(date):
    return date - timedelta(days=date.weekday())


def get_week_range(date):
    start = get_week_start(date)
    end = start + timedelta(days=6)
    return [start, end]

def pretty_date(time=False):
    from datetime import datetime
    now = datetime.now()
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        if day_diff == -1:
            return _('Tomorrow')
        if day_diff > -7:
            return _('In {} days').format(str(int(day_diff*-1)))
        if day_diff > -31:
            return _('In {} weeks').format(str(int((day_diff*-1) / 7)))
        if day_diff > 365:
            return _('In {} months').format(str(int((day_diff*-1) / 30)))
        return _('In {} years').format(str(int((day_diff*-1) / 365)))
    else:
        if day_diff == 0:
            if second_diff < 10:
                return _('Just now')
            if second_diff < 60:
                return _('{} seconds ago').format(int(second_diff))
            if second_diff < 120:
                return _('A minute ago')
            if second_diff < 3600:
                return _('{} minutes ago').format(int(second_diff / 60))
            if second_diff < 7200:
                return _('An hour ago')
            if second_diff < 86400:
                return _('{} hours ago').format(int(second_diff / 3600))
        if day_diff == 1:
            return _('Yesterday')
        if day_diff < 7:
            return _('{} days ago').format(int(day_diff))
        if day_diff < 31:
            return _('{} weeks ago').format(int(day_diff / 7))
        if day_diff < 365:
            return _('{} months ago').format(int(day_diff / 30))
        return _('{} years ago').format(int(day_diff / 365))