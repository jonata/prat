#!/usr/bin/env python3

from PySide6.QtWidgets import QComboBox, QVBoxLayout, QFrame, QPushButton, QSizePolicy, QGridLayout, QLabel, QStackedWidget
from PySide6.QtGui import QCursor, QIcon
from PySide6.QtCore import QSize, Qt

from prat.modules.translation import _
from prat.modules.paths import get_graphics_path
from prat.modules import deliver_documents

MODULE_INFO = {
                'title': _('Deliver'),
                'name': 'deliver'
              }

SELECTED_DELIVERABLE = None

def add_menu_widgets(self):
    self.deliverables_main_menu_bottom_frame = QFrame(self.main_menu_frame)
    self.deliverables_main_menu_bottom_frame.setObjectName(u"deliverables_main_menu_bottom_frame")
    self.deliverables_main_menu_bottom_frame.setFrameShape(QFrame.NoFrame)
    self.deliverables_main_menu_bottom_frame.setFrameShadow(QFrame.Raised)

    self.deliverables_main_menu_bottom_frame_vbox = QVBoxLayout(self.deliverables_main_menu_bottom_frame)
    self.deliverables_main_menu_bottom_frame_vbox.setSpacing(0)
    self.deliverables_main_menu_bottom_frame_vbox.setObjectName(u"deliverables_main_menu_bottom_frame_vbox")
    self.deliverables_main_menu_bottom_frame_vbox.setContentsMargins(0, 0, 0, 0)

    self.deliverables_main_menu_bottom_button = QPushButton(self.deliverables_main_menu_bottom_frame)
    self.deliverables_main_menu_bottom_button.setObjectName(u"deliverables_main_menu_bottom_button")
    self.deliverables_main_menu_bottom_button.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.deliverables_main_menu_bottom_button.sizePolicy().hasHeightForWidth())
    self.deliverables_main_menu_bottom_button.setSizePolicy(sizePolicy)
    self.deliverables_main_menu_bottom_button.setMinimumSize(QSize(0, 45))
    self.deliverables_main_menu_bottom_button.setCursor(QCursor(Qt.PointingHandCursor))
    self.deliverables_main_menu_bottom_button.setLayoutDirection(Qt.LeftToRight)
    self.deliverables_main_menu_bottom_button.setStyleSheet('background-image: url("' + get_graphics_path('deliver_icon.svg') + '");')
    self.deliverables_main_menu_bottom_button.clicked.connect(lambda: openCloseLeftBox(self))

    self.deliverables_main_menu_bottom_frame_vbox.addWidget(self.deliverables_main_menu_bottom_button)

    self.main_menu_vlayout.addWidget(self.deliverables_main_menu_bottom_frame, 0, Qt.AlignBottom)


def add_widgets(self):
    self.deliverables_box_frame = QFrame(self.main_frame)
    self.deliverables_box_frame.setObjectName(u"deliverables_box_frame")
    # self.deliverables_box_frame.setStyleSheet('background-color: red;')
    self.deliverables_box_frame.setMinimumSize(QSize(0, 0))
    self.deliverables_box_frame.setMaximumSize(QSize(0, 16777215))
    # self.deliverables_box_frame.setFrameShape(QFrame.NoFrame)
    # self.deliverables_box_frame.setFrameShadow(QFrame.Raised)

    self.deliverables_box_frame_vbox = QVBoxLayout(self.deliverables_box_frame)
    self.deliverables_box_frame_vbox.setSpacing(0)
    self.deliverables_box_frame_vbox.setObjectName(u"deliverables_box_frame_vbox")
    self.deliverables_box_frame_vbox.setContentsMargins(0, 0, 0, 0)

    self.deliverables_box_top_frame = QFrame(self.deliverables_box_frame)
    self.deliverables_box_top_frame.setObjectName(u"deliverables_box_top_frame")
    self.deliverables_box_top_frame.setMinimumSize(QSize(0, 50))
    self.deliverables_box_top_frame.setMaximumSize(QSize(16777215, 50))
    # self.deliverables_box_top_frame.setFrameShape(QFrame.NoFrame)
    # self.deliverables_box_top_frame.setFrameShadow(QFrame.Raised)

    self.deliverables_box_top_frame_vbox = QVBoxLayout(self.deliverables_box_top_frame)
    self.deliverables_box_top_frame_vbox.setSpacing(0)
    self.deliverables_box_top_frame_vbox.setObjectName(u"deliverables_box_top_frame_vbox")
    self.deliverables_box_top_frame_vbox.setContentsMargins(0, 0, 0, 0)

    self.deliverables_box_top_grid = QGridLayout()
    self.deliverables_box_top_grid.setObjectName(u"deliverables_box_top_grid")
    self.deliverables_box_top_grid.setHorizontalSpacing(10)
    self.deliverables_box_top_grid.setVerticalSpacing(0)
    self.deliverables_box_top_grid.setContentsMargins(10, -1, 10, -1)


    self.deliverables_box_top_label = QLabel(self.deliverables_box_top_frame)
    self.deliverables_box_top_label.setObjectName(u"deliverables_box_top_label")
    self.deliverables_box_top_label.setMinimumSize(QSize(150, 0))

    self.deliverables_box_top_grid.addWidget(self.deliverables_box_top_label, 0, 0, 1, 1)

    self.deliverables_box_top_close_button = QPushButton(self.deliverables_box_top_frame)
    self.deliverables_box_top_close_button.setObjectName(u"deliverables_box_top_close_button")
    self.deliverables_box_top_close_button.setMinimumSize(QSize(28, 28))
    self.deliverables_box_top_close_button.setMaximumSize(QSize(28, 28))
    self.deliverables_box_top_close_button.setCursor(QCursor(Qt.PointingHandCursor))

    icon = QIcon()
    icon.addFile(get_graphics_path('close_icon.svg'), QSize(), QIcon.Normal, QIcon.Off)
    self.deliverables_box_top_close_button.setIcon(icon)
    self.deliverables_box_top_close_button.setIconSize(QSize(20, 20))
    self.deliverables_box_top_close_button.clicked.connect(lambda: openCloseLeftBox(self))

    self.deliverables_box_top_grid.addWidget(self.deliverables_box_top_close_button, 0, 1, 1, 1)

    self.deliverables_box_top_frame_vbox.addLayout(self.deliverables_box_top_grid)

    self.deliverables_box_frame_vbox.addWidget(self.deliverables_box_top_frame)

    # Menu for documents, image, videos etc

    self.deliverables_box_menu_frame = QFrame(self.deliverables_box_frame)
    self.deliverables_box_menu_frame.setObjectName(u"deliverables_box_menu_frame")
    self.deliverables_box_menu_frame.setMinimumSize(QSize(0, 50))
    self.deliverables_box_menu_frame.setMaximumSize(QSize(16777215, 50))
    self.deliverables_box_menu_frame.setFrameShape(QFrame.NoFrame)
    self.deliverables_box_menu_frame.setFrameShadow(QFrame.Raised)

    self.deliverables_box_menu_frame_vbox = QVBoxLayout(self.deliverables_box_menu_frame)
    self.deliverables_box_menu_frame_vbox.setSpacing(0)
    self.deliverables_box_menu_frame_vbox.setObjectName(u"deliverables_box_menu_frame_vbox")
    self.deliverables_box_menu_frame_vbox.setContentsMargins(0, 0, 0, 0)

    self.deliverables_box_menu_grid = QGridLayout()
    self.deliverables_box_menu_grid.setObjectName(u"deliverables_box_menu_grid")
    self.deliverables_box_menu_grid.setHorizontalSpacing(0)
    self.deliverables_box_menu_grid.setVerticalSpacing(0)
    self.deliverables_box_menu_grid.setSpacing(0)
    self.deliverables_box_menu_grid.setContentsMargins(0, 0, 0, 0)

    deliver_documents.add_menu_widgets(self)

    self.deliverables_box_menu_frame_vbox.addLayout(self.deliverables_box_menu_grid)

    self.deliverables_box_frame_vbox.addWidget(self.deliverables_box_menu_frame)

    # content

    self.deliverables_box_content_select_deliverable = QComboBox(self.deliverables_box_frame)
    self.deliverables_box_content_select_deliverable.setMinimumHeight(50)
    # self.deliverables_box_content_select_deliverable.setWordWrap(True)
    self.deliverables_box_content_select_deliverable.setObjectName('deliverables_box_content_select_deliverable')
    self.deliverables_box_content_select_deliverable.activated.connect(lambda: update_deliverables_stackedwidgets(self))

    self.deliverables_box_frame_vbox.addWidget(self.deliverables_box_content_select_deliverable)

    self.deliverables_box_content_frame = QFrame(self.deliverables_box_frame)
    self.deliverables_box_content_frame.setObjectName(u"deliverables_box_content_frame")
    # self.deliverables_box_content_frame.setFrameShape(QFrame.NoFrame)
    # self.deliverables_box_content_frame.setFrameShadow(QFrame.Raised)

    self.deliverables_box_content_frame_vbox = QVBoxLayout(self.deliverables_box_content_frame)
    self.deliverables_box_content_frame_vbox.setSpacing(0)
    self.deliverables_box_content_frame_vbox.setObjectName(u"deliverables_box_content_frame_vbox")
    self.deliverables_box_content_frame_vbox.setContentsMargins(0, 0, 0, 0)

    self.deliverables_box_content_stackedwidget = QStackedWidget(self.deliverables_box_content_frame)
    self.deliverables_box_content_stackedwidget.setObjectName(u"deliverables_box_content_stackedwidget")

    deliver_documents.add_stacked_widgets(self)

    self.deliverables_box_content_frame_vbox.addWidget(self.deliverables_box_content_stackedwidget)

    self.deliverables_box_frame_vbox.addWidget(self.deliverables_box_content_frame)

    # self.deliverables_box_frame_vbox.addWidget(self.deliverables_box_content_frame)

    self.main_layout.addWidget(self.deliverables_box_frame)

    first_deliv =list(self.list_of_deliverables.keys())[0]
    deliverables_box_menu_button_clicked(self, first_deliv, 'deliverables_box_menu_{d}_button'.format(d=first_deliv))

def openCloseLeftBox(self):
    self.toggledLeftBox(True)


def translate_widgets(self):
    self.deliverables_main_menu_bottom_button.setText(_(MODULE_INFO['title']))
    self.deliverables_box_top_label.setText(_(MODULE_INFO['title']))
    deliver_documents.translate_widgets(self)


def deliverables_box_menu_button_clicked(self, what_deliverable_type, widget_name):
    for w in self.deliverables_box_menu_frame.findChildren(QPushButton):
        if w.objectName() != widget_name:
            w.setStyleSheet('background-color: #6e7888;')
        else:
            w.setStyleSheet('background-color: #b4bcc4;')

    self.deliverables_box_content_select_deliverable.clear()

    deliverables_list = []
    for deliverable in self.list_of_deliverables[what_deliverable_type]:
        deliverables_list.append(self.list_of_deliverables[what_deliverable_type][deliverable]['title'])

    self.deliverables_box_content_select_deliverable.addItems(deliverables_list)

    update_deliverables_stackedwidgets(self)


def update_deliverables_stackedwidgets(self):
    widget_to_show = None
    for deliverable_type in self.list_of_deliverables:
        for deliverable in self.list_of_deliverables[deliverable_type]:
            if self.list_of_deliverables[deliverable_type][deliverable]['title'] == self.deliverables_box_content_select_deliverable.currentText():
                widget_to_show = self.list_of_deliverables[deliverable_type][deliverable]['stacked_widget']
    if widget_to_show:
        self.deliverables_box_content_stackedwidget.setCurrentWidget(widget_to_show)


def update_deliverables_list(self):
    deliver_documents.add_deliverables_to_list(self)

