#!/usr/bin/env python3

from PySide6.QtWidgets import QPushButton, QSizePolicy
from PySide6.QtGui import QCursor, QIcon
from PySide6.QtCore import QSize, Qt

from prat.modules import deliver
from prat.modules import deliver_documents_clm
from prat.modules import deliver_documents_meeting_prayer_list
from prat.modules import deliver_documents_ministerial_servants_list
from prat.modules import deliver_documents_weekend_list
from prat.modules import deliver_documents_watchtower_reader_list
from prat.modules import deliver_documents_bible_study_reader_list
from prat.modules import deliver_documents_bible_study_conductor_list
from prat.modules import deliver_documents_weekend_meeting_chairman_list
from prat.modules import deliver_documents_midweek_meeting_chairman_list
from prat.modules import deliver_documents_attendants_list
from prat.modules import deliver_documents_media_operators_list
from prat.modules import deliver_documents_audio_operators_list
from prat.modules import deliver_documents_roving_microphones_list

from prat.modules.paths import get_graphics_path


def add_deliverables_to_list(self):
    self.list_of_deliverables['documents'] = {}
    deliver_documents_clm.add_deliverables_to_list(self)
    deliver_documents_weekend_list.add_deliverables_to_list(self)
    deliver_documents_watchtower_reader_list.add_deliverables_to_list(self)
    deliver_documents_bible_study_reader_list.add_deliverables_to_list(self)
    deliver_documents_bible_study_conductor_list.add_deliverables_to_list(self)
    deliver_documents_meeting_prayer_list.add_deliverables_to_list(self)
    deliver_documents_weekend_meeting_chairman_list.add_deliverables_to_list(self)
    deliver_documents_midweek_meeting_chairman_list.add_deliverables_to_list(self)
    deliver_documents_ministerial_servants_list.add_deliverables_to_list(self)
    deliver_documents_attendants_list.add_deliverables_to_list(self)
    deliver_documents_media_operators_list.add_deliverables_to_list(self)
    deliver_documents_audio_operators_list.add_deliverables_to_list(self)
    deliver_documents_roving_microphones_list.add_deliverables_to_list(self)


def add_menu_widgets(self):
    self.deliverables_box_menu_documents_button = QPushButton(self.deliverables_box_menu_frame)
    self.deliverables_box_menu_documents_button.setObjectName(u"deliverables_box_menu_documents_button")
    self.deliverables_box_menu_documents_button.setMinimumWidth(60)
    self.deliverables_box_menu_documents_button.setCursor(QCursor(Qt.PointingHandCursor))
    sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
    sizePolicy.setWidthForHeight(self.deliverables_box_menu_documents_button.sizePolicy().hasWidthForHeight())
    self.deliverables_box_menu_documents_button.setSizePolicy(sizePolicy)

    icon = QIcon()
    icon.addFile(get_graphics_path('deliver_documents_icon.svg'), QSize(), QIcon.Normal, QIcon.Off)
    self.deliverables_box_menu_documents_button.setIcon(icon)
    self.deliverables_box_menu_documents_button.setIconSize(QSize(20, 20))
    self.deliverables_box_menu_documents_button.clicked.connect(lambda: deliver.deliverables_box_menu_button_clicked(self, 'documents', self.deliverables_box_menu_documents_button.objectName()))

    self.deliverables_box_menu_grid.addWidget(self.deliverables_box_menu_documents_button, 0, 0, 0, 0)


def add_stacked_widgets(self):
    deliver_documents_clm.add_stacked_widgets(self)
    deliver_documents_weekend_list.add_stacked_widgets(self)
    deliver_documents_watchtower_reader_list.add_stacked_widgets(self)
    deliver_documents_bible_study_reader_list.add_stacked_widgets(self)
    deliver_documents_bible_study_conductor_list.add_stacked_widgets(self)
    deliver_documents_meeting_prayer_list.add_stacked_widgets(self)
    deliver_documents_weekend_meeting_chairman_list.add_stacked_widgets(self)
    deliver_documents_midweek_meeting_chairman_list.add_stacked_widgets(self)
    deliver_documents_ministerial_servants_list.add_stacked_widgets(self)
    deliver_documents_attendants_list.add_stacked_widgets(self)
    deliver_documents_media_operators_list.add_stacked_widgets(self)
    deliver_documents_audio_operators_list.add_stacked_widgets(self)
    deliver_documents_roving_microphones_list.add_stacked_widgets(self)


def translate_widgets(self):
    deliver_documents_clm.translate_widgets(self)
    deliver_documents_weekend_list.translate_widgets(self)
    deliver_documents_watchtower_reader_list.translate_widgets(self)
    deliver_documents_bible_study_reader_list.translate_widgets(self)
    deliver_documents_bible_study_conductor_list.translate_widgets(self)
    deliver_documents_meeting_prayer_list.translate_widgets(self)
    deliver_documents_weekend_meeting_chairman_list.translate_widgets(self)
    deliver_documents_midweek_meeting_chairman_list.translate_widgets(self)
    deliver_documents_ministerial_servants_list.translate_widgets(self)
    deliver_documents_attendants_list.translate_widgets(self)
    deliver_documents_media_operators_list.translate_widgets(self)
    deliver_documents_audio_operators_list.translate_widgets(self)
    deliver_documents_roving_microphones_list.translate_widgets(self)
