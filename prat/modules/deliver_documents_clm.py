#!/usr/bin/env python3

import os
from odf import text, table
from odf.opendocument import load
from datetime import datetime, timedelta
from notifypy import Notify

from PySide6.QtWidgets import QVBoxLayout, QPushButton, QSizePolicy, QLabel, QWidget

from prat.modules.translation import _
from prat.modules.paths import PATH_PRAT_DOCUMENTS, PATH_REAL_HOME, get_graphics_path
from prat.modules.midweek_meeting import load_assignments
from prat.modules.date import get_list_of_months


def add_deliverables_to_list(self):
    self.list_of_deliverables['documents']['clm'] = {
        'title': _('Our Christian Life and Ministry - Month'),
        'description': _('Our Christian Life and Ministry - Monthly document'),
        'filepath': os.path.join(PATH_PRAT_DOCUMENTS, 'clm.odt'),
        'stacked_widget': None
    }


def add_stacked_widgets(self):
    self.deliverables_box_content_documents_clm_widget = QWidget()
    self.deliverables_box_content_documents_clm_widget.setObjectName('deliverables_box_content_documents_clm_widget')
    self.deliverables_box_content_documents_clm_widget.setStyleSheet('#deliverables_box_content_documents_clm_widget { background-image: url("' + get_graphics_path('deliver_documents_clm_preview.svg') + '"); background-repeat: no-repeat; background-position: left bottom; }')
    # self.deliverables_box_content_documents_clm_widget.setFrameShape(QFrame.NoFrame)
    # self.deliverables_box_content_documents_clm_widget.setFrameShadow(QFrame.Raised)

    self.deliverables_box_content_documents_clm_widget_vbox = QVBoxLayout(self.deliverables_box_content_documents_clm_widget)
    self.deliverables_box_content_documents_clm_widget_vbox.setSpacing(10)
    self.deliverables_box_content_documents_clm_widget_vbox.setObjectName(u"deliverables_box_content_documents_clm_widget_vbox")
    self.deliverables_box_content_documents_clm_widget_vbox.setContentsMargins(10, 10, 10, 10)

    self.deliverables_box_content_documents_clm_description = QLabel(self.deliverables_box_content_documents_clm_widget)
    self.deliverables_box_content_documents_clm_description.setWordWrap(True)
    self.deliverables_box_content_documents_clm_description.setObjectName('deliverables_box_content_documents_clm_description')

    self.deliverables_box_content_documents_clm_widget_vbox.addWidget(self.deliverables_box_content_documents_clm_description)

    self.deliverables_box_content_documents_clm_generate_button = QPushButton(self.deliverables_box_content_documents_clm_widget)
    self.deliverables_box_content_documents_clm_generate_button.setObjectName('deliverables_box_content_documents_clm_generate_button')
    self.deliverables_box_content_documents_clm_generate_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.deliverables_box_content_documents_clm_generate_button.sizePolicy().hasHeightForWidth())
    self.deliverables_box_content_documents_clm_generate_button.setSizePolicy(sizePolicy)
    self.deliverables_box_content_documents_clm_generate_button.clicked.connect(lambda: generate_document_clm(self))

    self.deliverables_box_content_documents_clm_widget_vbox.addWidget(self.deliverables_box_content_documents_clm_generate_button)

    self.deliverables_box_content_documents_clm_widget_vbox.addStretch()

    self.deliverables_box_content_stackedwidget.addWidget(self.deliverables_box_content_documents_clm_widget)

    self.list_of_deliverables['documents']['clm']['stacked_widget'] = self.deliverables_box_content_documents_clm_widget


def translate_widgets(self):
    self.deliverables_box_content_documents_clm_generate_button.setText(_('Generate'))
    self.deliverables_box_content_documents_clm_description.setText(_('Document with the selected month assignemts for the Our Christian Life and Ministry meeting'))


def generate_document_clm(self, original_filepath=os.path.join(PATH_PRAT_DOCUMENTS, 'clm.odt'), final_filepath=False):
    list_of_months = get_list_of_months(self.selected_language)

    if 'midweek_meeting' not in self.assignments:
        load_assignments(self)

    dict_to_replace = {}
    dict_to_eliminate = {}

    dict_to_replace['MainTitle'] = _('Our Christian Live and Ministry')
    dict_to_replace['CongregationName'] = self.settings.get('congregation_name', '')
    dict_to_replace['MonthName'] = list_of_months[self.selected_date.month]
    dict_to_replace['YearNumber'] = str(self.selected_date.year)
    dict_to_replace['StartingSongTitle'] = _('Opening Song')
    dict_to_replace['MiddleSongTitle'] = _('Transition Song')
    dict_to_replace['FinalSongTitle'] = _('Closing Song')
    dict_to_replace['InitialPrayTitle'] = _('Opening prayer')
    dict_to_replace['ChairmanTitle'] = _('Chairman')
    dict_to_replace['TFGWTitle'] = _("Treasures from God's Word")
    dict_to_replace['SpiritialGemsTitle'] = _('Digging for Spiritual Gems')
    dict_to_replace['BibleReadingTitle'] = _('Bible reading')
    dict_to_replace['MainHallLabel'] = _('Main hall')
    dict_to_replace['AuxiliaryClass1Label'] = _('Auxiliary class 1')
    dict_to_replace['AuxiliaryClass2Label'] = _('Auxiliary class 2')
    dict_to_replace['AYFMTitle'] = _('Apply Yourself to the Field Ministry')
    dict_to_replace['ContuctorTitle'] = _('Conductor')
    dict_to_replace['LCTitle'] = _('Living as Christians')
    dict_to_replace['LCCongregationBibleStudyTitle'] = _('Congregation Bible Study')
    dict_to_replace['LCCongregationBibleStudyReaderTitle'] = _('Reader')
    dict_to_replace['FinalPrayTitle'] = _('Closing prayer')
    dict_to_replace['AttendantsTitle'] = _('Attendants')
    dict_to_replace['PlatformOperatorTitle'] = _('Platform operator')
    dict_to_replace['CommentsMicrophoneTitle'] = _('Microphone for comments')
    dict_to_replace['AudioOperatorTitle'] = _('Audio operator')
    dict_to_replace['VideoOperatorTitle'] = _('Video operator')
    dict_to_replace['VideoconferenceOperatorsTitle'] = _('Videoconference operator')

    week_start = datetime(day=1, month=self.selected_date.month, year=self.selected_date.year)
    if self.settings.get('use_week_of', False):
        while week_start.weekday():
            week_start += timedelta(days=1)
        day = week_start
    else:
        while week_start.weekday():
            if self.settings.get('midweek_meeting_weekday', 3) >= datetime(day=1, month=self.selected_date.month, year=self.selected_date.year).weekday():
                week_start -= timedelta(days=1)
            else:
                week_start += timedelta(days=1)
        day = week_start + timedelta(days=int(self.settings.get('midweek_meeting_weekday', 3)))

    if not self.settings.get('auxiliary_class_1', False):
        dict_to_eliminate['AuxiliaryClass1'] = False

    if not self.settings.get('auxiliary_class_2', False):
        dict_to_eliminate['AuxiliaryClass2'] = False

    if (day + timedelta(days=28)).month != day.month:
        dict_to_eliminate['Week4'] = False

    w = 0
    while day.month == self.selected_date.month:
        dict_to_replace[f'DaysWeek{w}'] = str(day.day)
        dict_to_replace[f'ChairmanNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('chairman', '')
        dict_to_replace[f'InitialPrayNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('opening_prayer', '')
        dict_to_replace[f'TFGWTalkNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('tfgw', '')
        dict_to_replace[f'SpiritualGemsNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('spiritual_gems', '')

        dict_to_replace[f'BibleReadingNameMainHallWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('bible_reading', '')

        if self.settings.get('auxiliary_class_1', False):
            dict_to_replace[f'ConductorClass1Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('bible_reading', '')
            dict_to_replace[f'BibleReadingNameAuxiliaryClass1Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('bible_reading', '')
        else:
            dict_to_eliminate[f'ConductorClass1Week{w}'] = False
            dict_to_eliminate[f'BibleReadingNameAuxiliaryClass1Week{w}'] = False

        if self.settings.get('auxiliary_class_2', False):
            dict_to_replace[f'ConductorClass2Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('bible_reading', '')
            dict_to_replace[f'BibleReadingNameAuxiliaryClass2Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('bible_reading', '')
        else:
            dict_to_eliminate[f'ConductorClass2Week{w}'] = False
            dict_to_eliminate[f'BibleReadingNameAuxiliaryClass2Week{w}'] = False

        dict_to_replace[f'TFGWTalkTitleWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('tfgw', {}).get('tfgw_title', '')
        dict_to_replace[f'SpiritualGemsTitleWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('tfgw', {}).get('tfgw_spiritual_gems_title', '')
        dict_to_replace[f'BibleReadingTitleWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('tfgw', {}).get('tfgw_bible_reading_title', '')

        for p in range(4):
            p = p + 1
            if bool(self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {}).get(f'ayfm_part{p}_title', '')):
                dict_to_replace[f'Part{p}NameMainHallWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get(f'ayfm_part{p}', '')
                dict_to_replace[f'Part{p}NameAuxiliaryClass1Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('ayfm_part1', '')
                dict_to_replace[f'Part{p}NameAuxiliaryClass2Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('ayfm_part1', '')

                if not bool(self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get(f'ayfm_part{p}_is_talk', '')):
                    dict_to_replace[f'Part{p}AssistantMainHallWeek{w}'] = ' • ' + self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get(f'ayfm_part{p}_assistant', '')
                    dict_to_replace[f'Part{p}AssistantAuxiliaryClass1Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('ayfm_part1', '')
                    dict_to_replace[f'Part{p}AssistantAuxiliaryClass2Week{w}'] = ''  # self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('ayfm_part1', '')
                else:
                    dict_to_eliminate[f'Part{p}AssistantMainHallWeek{w}'] = False
                    dict_to_eliminate[f'Part{p}AssistantAuxiliaryClass1Week{w}'] = False
                    dict_to_eliminate[f'Part{p}AssistantAuxiliaryClass2Week{w}'] = False

            else:
                dict_to_eliminate[f'AYFMPart{p}Week{w}'] = False

            dict_to_replace[f'Part{p}Week{w}Title'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {}).get(f'ayfm_part{p}_title', '')

        for p in range(3):
            p = p + 1
            if bool(self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('lc', {}).get(f'lc_part{p}_title', '')):
                dict_to_replace[f'LCPart{p}NameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get(f'lc_part{p}', '')
                dict_to_replace[f'LCPart{p}TitleWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('lc', {}).get(f'lc_part{p}_title', '')
            else:
                dict_to_eliminate[f'LCPart{p}Week{w}'] = False

        dict_to_replace[f'LCCongregationBibleStudyTitleWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('lc', {}).get('lc_bible_study_title', '')
        dict_to_replace[f'LCCongregationBibleStudyNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('lc_cong_bible_study_conductor', '')
        dict_to_replace[f'LCCongregationBibleStudyReaderNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('lc_cong_bible_study_reader', '')

        dict_to_replace[f'FinalPrayNameWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('closing_prayer', '')

        dict_to_replace[f'StartingSongNumberWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('opening_song', '')
        dict_to_replace[f'MiddleSongNumberWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('transition_song', '')
        dict_to_replace[f'FinalSongNumberWeek{w}'] = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('closing_song', '')

        dict_to_replace[f'AttendantsNamesWeek{w}'] = ' • '.join(self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('attendants', []))
        dict_to_replace[f'PlatformOperatorWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('platform_operator', '')
        dict_to_replace[f'CommentsMicrophoneWeek{w}'] = ' • '.join(self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('microphone_operators', []))
        dict_to_replace[f'AudioOperatorWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('audio_operator', '')
        dict_to_replace[f'VideoOperatorWeek{w}'] = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('video_operator', '')
        dict_to_replace[f'VideoconferenceOperatorNamesWeek{w}'] = ' • '.join(self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('videoconference_attendants', []))

        w += 1
        day += timedelta(days=7)
        week_start += timedelta(days=7)

    textdoc = load(original_filepath)
    # texts = textdoc.getElementsByType(text.P)
    # for textpiece in texts:
    #     if textpiece.childNodes:
    #         for placeholder in dict_to_replace:
    #             if placeholder in textpiece.childNodes[0].data:
    #                 textpiece.childNodes[0].data = textpiece.childNodes[0].data.replace(placeholder, dict_to_replace[placeholder])

    p_elements = textdoc.getElementsByType(text.P)
    for p in p_elements:
        for attr in list(p.attributes.keys()):
            for placeholder in dict_to_eliminate:
                if attr[-1] == 'id' and placeholder in p.attributes[attr].split(',') and p in p.parentNode.childNodes:
                    p.parentNode.childNodes.remove(p)

    row_elements = textdoc.getElementsByType(table.TableRow)
    for row in row_elements:
        for attr in list(row.attributes.keys()):
            for placeholder in dict_to_eliminate:
                if attr[-1] == 'id' and placeholder in row.attributes[attr].split(',') and row in row.parentNode.childNodes:
                    row.parentNode.childNodes.remove(row)

    spans = textdoc.getElementsByType(text.Span)
    for span in spans:
        if span.childNodes:
            for attr in list(span.attributes.keys()):
                for placeholder in dict_to_replace:
                    if attr[-1] == 'id' and placeholder in span.attributes[attr].split(','):
                        span.childNodes[0].data = dict_to_replace[placeholder]
                for placeholder in dict_to_eliminate:
                    if attr[-1] == 'id' and placeholder in span.attributes[attr].split(',') and span in span.parentNode.childNodes:
                        span.parentNode.childNodes.remove(span)

    if final_filepath:
        textdoc.save(final_filepath)
    else:
        textdoc.save(os.path.join(PATH_REAL_HOME, _('Document for Our Christian Life and Ministry - {monthname} {year}.odt').format(monthname=list_of_months[self.selected_date.month], year=self.selected_date.year)))

    notification = Notify()
    notification.title = "Prat"
    notification.message = _("Document {} generated").format(_(self.list_of_deliverables['documents']['clm']['title']))
    notification.icon = get_graphics_path('prat.png')
    notification.send()