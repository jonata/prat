#!/usr/bin/env python3

import os
from odf import text
from odf.opendocument import load
from notifypy import Notify

from PySide6.QtWidgets import QVBoxLayout, QPushButton, QSizePolicy, QLabel, QWidget

from prat.modules.translation import _
from prat.modules.paths import PATH_REAL_HOME, PATH_PRAT_DOCUMENTS, get_graphics_path
# from prat.modules.midweek_meeting import load_assignments
from prat.modules.publishers import get_publishers_media_operators


def add_deliverables_to_list(self):
    self.list_of_deliverables['documents']['media_operators_list'] = {
        'title': _('List of media operators'),
        'description': _('List with all media operators'),
        'filepath': os.path.join(PATH_PRAT_DOCUMENTS, 'generic_list.odt'),
        'stacked_widget': None
    }


def add_stacked_widgets(self):
    self.deliverables_box_content_documents_media_operators_list_widget = QWidget()
    self.deliverables_box_content_documents_media_operators_list_widget.setObjectName('deliverables_box_content_documents_media_operators_list_widget')
    self.deliverables_box_content_documents_media_operators_list_widget.setStyleSheet('#deliverables_box_content_documents_media_operators_list_widget { background-image: url("' + get_graphics_path('deliver_documents_generic_list_preview.svg') + '"); background-repeat: no-repeat; background-position: left bottom; }')
    # self.deliverables_box_content_documents_media_operators_list_widget.setFrameShape(QFrame.NoFrame)
    # self.deliverables_box_content_documents_media_operators_list_widget.setFrameShadow(QFrame.Raised)

    self.deliverables_box_content_documents_media_operators_list_widget_vbox = QVBoxLayout(self.deliverables_box_content_documents_media_operators_list_widget)
    self.deliverables_box_content_documents_media_operators_list_widget_vbox.setSpacing(10)
    self.deliverables_box_content_documents_media_operators_list_widget_vbox.setObjectName(u"deliverables_box_content_documents_media_operators_list_widget_vbox")
    self.deliverables_box_content_documents_media_operators_list_widget_vbox.setContentsMargins(10, 10, 10, 10)

    self.deliverables_box_content_documents_media_operators_list_description = QLabel(self.deliverables_box_content_documents_media_operators_list_widget)
    self.deliverables_box_content_documents_media_operators_list_description.setWordWrap(True)
    self.deliverables_box_content_documents_media_operators_list_description.setObjectName('deliverables_box_content_documents_media_operators_list_description')

    self.deliverables_box_content_documents_media_operators_list_widget_vbox.addWidget(self.deliverables_box_content_documents_media_operators_list_description)

    self.deliverables_box_content_documents_media_operators_list_generate_button = QPushButton(self.deliverables_box_content_documents_media_operators_list_widget)
    self.deliverables_box_content_documents_media_operators_list_generate_button.setObjectName('deliverables_box_content_documents_media_operators_list_generate_button')
    self.deliverables_box_content_documents_media_operators_list_generate_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.deliverables_box_content_documents_media_operators_list_generate_button.sizePolicy().hasHeightForWidth())
    self.deliverables_box_content_documents_media_operators_list_generate_button.setSizePolicy(sizePolicy)
    self.deliverables_box_content_documents_media_operators_list_generate_button.clicked.connect(lambda: generate_document_media_operators_list(self))

    self.deliverables_box_content_documents_media_operators_list_widget_vbox.addWidget(self.deliverables_box_content_documents_media_operators_list_generate_button)

    self.deliverables_box_content_documents_media_operators_list_widget_vbox.addStretch()

    self.deliverables_box_content_stackedwidget.addWidget(self.deliverables_box_content_documents_media_operators_list_widget)

    self.list_of_deliverables['documents']['media_operators_list']['stacked_widget'] = self.deliverables_box_content_documents_media_operators_list_widget


def translate_widgets(self):
    self.deliverables_box_content_documents_media_operators_list_generate_button.setText(_('Generate'))
    self.deliverables_box_content_documents_media_operators_list_description.setText(_('Document with a list of approved media_operators'))


def generate_document_media_operators_list(self, final_filepath=False):
    original_filepath = self.list_of_deliverables['documents']['media_operators_list']['filepath']

    # if 'midweek_meeting' not in self.assignments:
    #     load_assignments(self)

    dict_to_replace = {}
    dict_to_eliminate = {}

    dict_to_replace['MainTitle'] = _(self.list_of_deliverables['documents']['media_operators_list']['title'])
    dict_to_replace['CongregationName'] = self.settings.get('congregation_name', '')

    list_of_ms = get_publishers_media_operators(self.publishers_list, sort=True)

    for i in range(len(list_of_ms)):
        dict_to_replace['ReaderName{}'.format(str(i + 1).zfill(2))] = list_of_ms[i]

    for i in range(len(list_of_ms), 50):
        dict_to_eliminate['ReaderName{}ListItem'.format(str(i + 1).zfill(2))] = False

    textdoc = load(original_filepath)

    row_elements = textdoc.getElementsByType(text.ListItem)
    for row in row_elements:
        for attr in list(row.attributes.keys()):
            for placeholder in dict_to_eliminate:
                if attr[-1] == 'id' and placeholder in row.attributes[attr].split(',') and row in row.parentNode.childNodes:
                    row.parentNode.childNodes.remove(row)

    p_elements = textdoc.getElementsByType(text.P)
    for p in p_elements:
        if p.childNodes:
            for attr in list(p.attributes.keys()):
                for placeholder in dict_to_replace:
                    if attr[-1] == 'id' and placeholder in p.attributes[attr].split(','):
                        p.childNodes[0].data = dict_to_replace[placeholder]

    if final_filepath:
        textdoc.save(final_filepath)
    else:
        textdoc.save(os.path.join(PATH_REAL_HOME, '{}.odt'.format(_(self.list_of_deliverables['documents']['media_operators_list']['title']))))

    notification = Notify()
    notification.title = "Prat"
    notification.message = _("Document {} generated").format(_(self.list_of_deliverables['documents']['media_operators_list']['title']))
    notification.icon = get_graphics_path('prat.png')
    notification.send()