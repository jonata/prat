#!/usr/bin/env python3

import os
from odf import text, table
from odf.opendocument import load
from datetime import datetime, timedelta
from notifypy import Notify

from PySide6.QtWidgets import QVBoxLayout, QPushButton, QSizePolicy, QLabel, QWidget

from prat.modules.translation import _
from prat.modules.paths import PATH_REAL_HOME, PATH_PRAT_DOCUMENTS, get_graphics_path
from prat.modules.weekend_meeting import load_assignments
from prat.modules.date import get_list_of_months


def add_deliverables_to_list(self):
    self.list_of_deliverables['documents']['weekend_list'] = {
        'title': _('Weekend Meeting assignments - Month'),
        'description': _('Weekend Meeting assignments - Monthly document'),
        'filepath': os.path.join(PATH_PRAT_DOCUMENTS, 'weekend_list.odt'),
        'stacked_widget': None
    }


def add_stacked_widgets(self):
    self.deliverables_box_content_documents_weekend_list_widget = QWidget()
    self.deliverables_box_content_documents_weekend_list_widget.setObjectName('deliverables_box_content_documents_weekend_list_widget')
    self.deliverables_box_content_documents_weekend_list_widget.setStyleSheet('#deliverables_box_content_documents_weekend_list_widget { background-image: url("' + get_graphics_path('deliver_documents_weekend_list_preview.svg') + '"); background-repeat: no-repeat; background-position: left bottom; }')
    # self.deliverables_box_content_documents_weekend_list_widget.setFrameShape(QFrame.NoFrame)
    # self.deliverables_box_content_documents_weekend_list_widget.setFrameShadow(QFrame.Raised)

    self.deliverables_box_content_documents_weekend_list_widget_vbox = QVBoxLayout(self.deliverables_box_content_documents_weekend_list_widget)
    self.deliverables_box_content_documents_weekend_list_widget_vbox.setSpacing(10)
    self.deliverables_box_content_documents_weekend_list_widget_vbox.setObjectName(u"deliverables_box_content_documents_weekend_list_widget_vbox")
    self.deliverables_box_content_documents_weekend_list_widget_vbox.setContentsMargins(10, 10, 10, 10)

    self.deliverables_box_content_documents_weekend_list_description = QLabel(self.deliverables_box_content_documents_weekend_list_widget)
    self.deliverables_box_content_documents_weekend_list_description.setWordWrap(True)
    self.deliverables_box_content_documents_weekend_list_description.setObjectName('deliverables_box_content_documents_weekend_list_description')

    self.deliverables_box_content_documents_weekend_list_widget_vbox.addWidget(self.deliverables_box_content_documents_weekend_list_description)

    self.deliverables_box_content_documents_weekend_list_generate_button = QPushButton(self.deliverables_box_content_documents_weekend_list_widget)
    self.deliverables_box_content_documents_weekend_list_generate_button.setObjectName('deliverables_box_content_documents_weekend_list_generate_button')
    self.deliverables_box_content_documents_weekend_list_generate_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.deliverables_box_content_documents_weekend_list_generate_button.sizePolicy().hasHeightForWidth())
    self.deliverables_box_content_documents_weekend_list_generate_button.setSizePolicy(sizePolicy)
    self.deliverables_box_content_documents_weekend_list_generate_button.clicked.connect(lambda: generate_document_weekend_list(self))

    self.deliverables_box_content_documents_weekend_list_widget_vbox.addWidget(self.deliverables_box_content_documents_weekend_list_generate_button)

    self.deliverables_box_content_documents_weekend_list_widget_vbox.addStretch()

    self.deliverables_box_content_stackedwidget.addWidget(self.deliverables_box_content_documents_weekend_list_widget)

    self.list_of_deliverables['documents']['weekend_list']['stacked_widget'] = self.deliverables_box_content_documents_weekend_list_widget


def translate_widgets(self):
    self.deliverables_box_content_documents_weekend_list_generate_button.setText(_('Generate'))
    self.deliverables_box_content_documents_weekend_list_description.setText(_('Document with the selected month assignemts for the Our Christian Life and Ministry meeting'))


def generate_document_weekend_list(self, final_filepath=False):
    original_filepath = self.list_of_deliverables['documents']['weekend_list']['filepath']
    list_of_months = get_list_of_months(self.selected_language)

    if 'weekend_meeting' not in self.assignments:
        load_assignments(self)

    dict_to_replace = {}
    dict_to_eliminate = {}

    dict_to_replace['MainTitle'] = _('Weekend meeting')
    dict_to_replace['CongregationName'] = self.settings.get('congregation_name', '')
    dict_to_replace['MonthName'] = list_of_months[self.selected_date.month]
    dict_to_replace['YearNumber'] = str(self.selected_date.year)
    dict_to_replace['OpeningSongTitle'] = _('Opening Song')
    dict_to_replace['TransitionSongTitle'] = _('Transition Song')
    dict_to_replace['ClosingSongTitle'] = _('Closing Song')
    dict_to_replace['OpeningPrayerTitle'] = _('Opening prayer')
    dict_to_replace['ChairmanTitle'] = _('Chairman')
    dict_to_replace['ClosingPrayerTitle'] = _('Closing prayer')

    dict_to_replace['SpeakerTitle'] = _("Speaker")
    dict_to_replace['PublicMeetingTitle'] = _('Public Talk')
    dict_to_replace['ConductorTitle'] = _('Conductor')
    dict_to_replace['ReaderTitle'] = _('Reader')
    dict_to_replace['WatchtowerStudyTitle'] = _('Watchtower Study')
    dict_to_replace['AttendantsTitle'] = _('Attendants')
    dict_to_replace['PlatformOperatorTitle'] = _('Platform operator')
    dict_to_replace['CommentsMicrophoneTitle'] = _('Microphone for comments')
    dict_to_replace['AudioOperatorTitle'] = _('Audio operator')
    dict_to_replace['VideoOperatorTitle'] = _('Video operator')
    dict_to_replace['VideoconferenceOperatorsTitle'] = _('Videoconference operator')

    week_start = datetime(day=1, month=self.selected_date.month, year=self.selected_date.year)
    if self.settings.get('use_week_of', False):
        while week_start.weekday():
            week_start += timedelta(days=1)
        day = week_start
    else:
        while week_start.weekday():
            if self.settings.get('weekend_meeting_weekday', 3) >= datetime(day=1, month=self.selected_date.month, year=self.selected_date.year).weekday():
                week_start -= timedelta(days=1)
            else:
                week_start += timedelta(days=1)
        day = week_start + timedelta(days=int(self.settings.get('weekend_meeting_weekday', 3)))

    if not self.settings.get('auxiliary_class_1', False):
        dict_to_eliminate['AuxiliaryClass1'] = False

    if not self.settings.get('auxiliary_class_2', False):
        dict_to_eliminate['AuxiliaryClass2'] = False

    if (day + timedelta(days=28)).month != day.month:
        dict_to_eliminate['Week4'] = False

    w = 0
    while day.month == self.selected_date.month:
        dict_to_replace[f'Day{w}'] = str(day.day)
        dict_to_replace[f'OpeningSong{w}'] = self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('opening_song', '')
        dict_to_replace[f'TransitionSong{w}'] = self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('transition_song', '')
        dict_to_replace[f'ClosingSong{w}'] = self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('closing_song', '')
        dict_to_replace[f'SpeakerName{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('speaker', '')
        dict_to_replace[f'TalkTitle{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('public_talk', '')

        dict_to_replace[f'StudySource{w}'] = self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('source', '')
        dict_to_replace[f'StudyTitle{w}'] = self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('title', '')

        dict_to_replace[f'ReaderName{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('watchtower_reader', '')
        dict_to_replace[f'ConductorName{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('watchtower_conductor', '')

        dict_to_replace[f'ChairmanName{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('chairman', '')
        dict_to_replace[f'OpeningPrayerName{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('opening_prayer', '')
        dict_to_replace[f'ClosingPrayerName{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('closing_prayer', '')

        dict_to_replace[f'AttendantsNamesWeek{w}'] = ' • '.join(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('attendants', []))
        dict_to_replace[f'PlatformOperatorWeek{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('platform_operator', '')
        dict_to_replace[f'CommentsMicrophoneWeek{w}'] = ' • '.join(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('microphone_operators', []))
        dict_to_replace[f'AudioOperatorWeek{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('audio_operator', '')
        dict_to_replace[f'VideoOperatorWeek{w}'] = self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('video_operator', '')
        dict_to_replace[f'VideoconferenceOperatorNamesWeek{w}'] = ' • '.join(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('videoconference_attendants', []))

        w += 1
        day += timedelta(days=7)
        week_start += timedelta(days=7)

    textdoc = load(original_filepath)

    p_elements = textdoc.getElementsByType(text.P)
    for p in p_elements:
        for attr in list(p.attributes.keys()):
            for placeholder in dict_to_eliminate:
                if attr[-1] == 'id' and placeholder in p.attributes[attr].split(',') and p in p.parentNode.childNodes:
                    p.parentNode.childNodes.remove(p)
            for placeholder in dict_to_replace:
                if attr[-1] == 'id' and placeholder in p.attributes[attr].split(','):
                    p.childNodes[0].data = dict_to_replace[placeholder]

    row_elements = textdoc.getElementsByType(table.TableRow)
    for row in row_elements:
        for attr in list(row.attributes.keys()):
            for placeholder in dict_to_eliminate:
                if attr[-1] == 'id' and placeholder in row.attributes[attr].split(',') and row in row.parentNode.childNodes:
                    row.parentNode.childNodes.remove(row)

    spans = textdoc.getElementsByType(text.Span)
    for span in spans:
        if span.childNodes:
            for attr in list(span.attributes.keys()):
                for placeholder in dict_to_replace:
                    if attr[-1] == 'id' and placeholder in span.attributes[attr].split(','):
                        span.childNodes[0].data = dict_to_replace[placeholder]
                for placeholder in dict_to_eliminate:
                    if attr[-1] == 'id' and placeholder in span.attributes[attr].split(',') and span in span.parentNode.childNodes:
                        span.parentNode.childNodes.remove(span)

    if final_filepath:
        textdoc.save(final_filepath)
    else:
        textdoc.save(os.path.join(PATH_REAL_HOME, _(self.list_of_deliverables['documents']['weekend_list']['title']) + ' - {monthname} {year}.odt'.format(monthname=list_of_months[self.selected_date.month], year=self.selected_date.year)))

    notification = Notify()
    notification.title = "Prat"
    notification.message = _("Document {} generated").format(_(self.list_of_deliverables['documents']['weekend_list']['title']))
    notification.icon = get_graphics_path('prat.png')
    notification.send()