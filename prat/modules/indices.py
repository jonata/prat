#!/usr/bin/python3
"""All path definitions for Prat"""

from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QFrame, QPushButton, QSizePolicy, QLabel, QSpacerItem, QFileDialog
from PySide6.QtGui import QPixmap
from PySide6.QtCore import Qt, QThread, Signal, QSize

import os
from ebooklib import epub, ITEM_DOCUMENT
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import json
import requests

from prat.modules.paths import PATH_PRAT_MWB_INDEX, PATH_PRAT_TMP, PATH_PRAT_W_INDEX, PATH_REAL_HOME, get_graphics_path
from prat.modules.translation import get_language_pairs, get_list_of_months, _

MODULE_INFO = {
    'title': _('Update indices'),
    'name': 'update_indices',
    'date_enabled': False
}


class thread_download_and_update_files(QThread):
    response = Signal(str)
    download = False
    epub_folder = False
    language = False
    list_of_issues = []

    def run(self):
        # self.response.emit('reading')
        directory = self.epub_folder if self.epub_folder else os.path.join(PATH_PRAT_TMP)

        if self.download and self.language and self.list_of_issues:
            self.response.emit('downloading')
            for issue in self.list_of_issues:
                for publication in ['mwb', 'w']:
                    res = requests.get('https://b.jw-cdn.org/apis/pub-media/GETPUBMEDIALINKS?issue={issue}&output=json&pub={pub}&fileformat=EPUB&alllangs=0&langwritten={lang}'.format(issue=issue, lang=self.language, pub=publication))
                    if type(res.json()) is dict:
                        url_to_download = res.json().get('files', {}).get(self.language, {}).get('EPUB', [])[0].get('file', {}).get('url', '')
                        with open(os.path.join(directory, url_to_download.rsplit('/')[-1]), 'wb') as epub_file:
                            epub_file.write(requests.get(url_to_download).content)

        self.response.emit('reading')

        mwb_index_file = PATH_PRAT_MWB_INDEX
        w_index_file = PATH_PRAT_W_INDEX
        list_of_mwb = []
        list_of_w = []
        for dirpath, dnames, filenames in os.walk(directory):
            for f in filenames:
                if f.lower().endswith('.epub'):
                    if f.lower().startswith('w'):
                        list_of_w.append(os.path.join(dirpath, f))
                    elif f.lower().startswith('mwb'):
                        list_of_mwb.append(os.path.join(dirpath, f))

        mwb_json = json.loads('{}')
        if os.path.isfile(mwb_index_file):
            mwb_json = json.load(open(mwb_index_file))

        for mwb_file in list_of_mwb:
            book = epub.read_epub(mwb_file)

            language = book.get_metadata('DC', 'language')[0][0]
            if language not in mwb_json:
                mwb_json[language] = {}

            language_pairs = get_language_pairs(language)

            year = get_year_from_string(book.get_metadata('DC', 'title')[0][0])

            items = book.get_items_of_type(ITEM_DOCUMENT)
            for item in items:
                html = BeautifulSoup(item.content.decode(), 'html.parser')
                if len(html.find_all("div", {"class": "section"})) > 2:
                    week_dt = get_datetime_from_string(html.title.text, language, year)
                    if week_dt:
                        week = week_dt.strftime('%Y%m%d')
                        if week not in mwb_json[language]:
                            mwb_json[language][week] = {}

                        if 'tfgw' not in mwb_json[language][week]:
                            mwb_json[language][week]['tfgw'] = {}

                        if 'ayfm' not in mwb_json[language][week]:
                            mwb_json[language][week]['ayfm'] = {}

                        if 'lc' not in mwb_json[language][week]:
                            mwb_json[language][week]['lc'] = {}

                        section_divs = html.find_all("div", {"class": "section"})

                        opening_song_part = section_divs[0].find_all("div", {"class": "pGroup"})[0].find_all('strong')[0].text
                        mwb_json[language][week]['opening_song'] = get_number_from_string(opening_song_part)

                        tfgw_parts = section_divs[1].find_all("div", {"class": "pGroup"})[0].ul.find_all('li', recursive=False)

                        tfgw_title = tfgw_parts[0].text
                        mwb_json[language][week]['tfgw']['tfgw_title'] = tfgw_title.rsplit(':', 1)[0]
                        mwb_json[language][week]['tfgw']['tfgw_time'] = clean_text(tfgw_title.rsplit(':', 1)[-1])

                        tfgw_spiritual_gems_title = tfgw_parts[1].p.text
                        mwb_json[language][week]['tfgw']['tfgw_spiritual_gems_title'] = tfgw_spiritual_gems_title.rsplit(':', 1)[0]
                        mwb_json[language][week]['tfgw']['tfgw_spiritual_gems_time'] = clean_text(tfgw_spiritual_gems_title.rsplit(':', 1)[-1])

                        if len(tfgw_parts) > 2:
                            tfgw_bible_reading_title = tfgw_parts[2].p.text
                            mwb_json[language][week]['tfgw']['tfgw_bible_reading_title'] = tfgw_bible_reading_title.split(':', 1)[0]
                            mwb_json[language][week]['tfgw']['tfgw_bible_reading_time'] = clean_text(tfgw_bible_reading_title.split(':', 1)[-1].split(')', 1)[0])

                        ayfm_parts = section_divs[2].find_all("div", {"class": "pGroup"})[0].ul.find_all('li', recursive=False)

                        for p in range(4):
                            if len(ayfm_parts) > p:
                                ayfm_part = ayfm_parts[p].p.text
                                title = ayfm_part.split(':', 1)[0]

                                for a in ayfm_parts[p].find_all('a', attrs={'data-video': True}):
                                    if a['data-video'].startswith('webpubvid://?pub=mwbv'):
                                        mwb_json[language][week]['ayfm']['ayfm_part{p}_has_video'.format(p=p + 1)] = True
                                        break

                                mwb_json[language][week]['ayfm']['ayfm_part{p}_title'.format(p=p + 1)] = title

                                if title in language_pairs.get('is_talk_keywords', '').split(',') or (ayfm_parts[p].find_all('a') and ayfm_parts[p].find_all('a')[0].text in title):
                                    mwb_json[language][week]['ayfm']['ayfm_part{p}_is_talk'.format(p=p + 1)] = True

                                mwb_json[language][week]['ayfm']['ayfm_part{p}_time'.format(p=p + 1)] = clean_text(ayfm_part.split(':', 1)[-1].split(')', 1)[0])

                        lc_parts = section_divs[3].find_all("div", {"class": "pGroup"})[0].ul.find_all('li', recursive=False)

                        transition_song_part = lc_parts[0].text
                        mwb_json[language][week]['transition_song'] = get_number_from_string(transition_song_part)

                        closing_song_part = lc_parts[-1].text
                        mwb_json[language][week]['closing_song'] = get_number_from_string(closing_song_part)

                        lc_bible_study_title = lc_parts[-3].p.text
                        mwb_json[language][week]['lc']['lc_bible_study_title'] = lc_bible_study_title.split(':', 1)[0]
                        mwb_json[language][week]['lc']['lc_bible_study_time'] = clean_text(lc_bible_study_title.split(':', 1)[-1].split(')', 1)[0])

                        lc_parts = lc_parts[1:-3]

                        for p in range(4):
                            if len(lc_parts) > p:
                                lc_part = lc_parts[p].p.text
                                mwb_json[language][week]['lc']['lc_part{p}_title'.format(p=p + 1)] = lc_part.split(':', 1)[0]
                                mwb_json[language][week]['lc']['lc_part{p}_time'.format(p=p + 1)] = clean_text(lc_part.split(':', 1)[-1].split(')', 1)[0])

        if mwb_json:
            open(mwb_index_file, 'w').write(json.dumps(mwb_json, indent=4, ensure_ascii=False))

        w_json = json.loads('{}')
        if os.path.isfile(w_index_file):
            w_json = json.load(open(w_index_file))

        for w_file in list_of_w:
            book = epub.read_epub(w_file)

            language = book.get_metadata('DC', 'language')[0][0]
            if language not in w_json:
                w_json[language] = {}

            w_source = book.get_metadata('DC', 'title')[0][0]

            temp_hrefs = {}

            items = book.get_items_of_type(ITEM_DOCUMENT)
            for item in items:
                html = BeautifulSoup(item.content.decode(), 'html.parser')
                group_toc = html.find_all("div", {"class": "groupTOC"})
                if group_toc:
                    toc_items = group_toc[0].find_all(recursive=False)
                    if len(toc_items) > 1:
                        toc_items = toc_items[1:]

                        i = 0
                        while i < len(toc_items):
                            week_string = toc_items[i].text.replace('ǀ', ':').split(':', 1)[-1]

                            week_dt = get_datetime_from_string(week_string, language)
                            if week_dt:

                                week = week_dt.strftime('%Y%m%d')
                                if week not in w_json[language]:
                                    w_json[language][week] = {}

                                page = False
                                title = toc_items[i + 1].text.strip()
                                if title.split(' ')[0].isnumeric():
                                    page = int(title.split(' ')[0])
                                    title = title.split(' ', 1)[-1].strip()

                                w_json[language][week]['title'] = title

                                if page:
                                    w_json[language][week]['page'] = page

                                w_json[language][week]['source'] = w_source

                                href = toc_items[i + 1].find_all('a')[0]['href']
                                temp_hrefs[week] = {'href': href}
                                i += 2
                            else:
                                i += 1
                if item:
                    for week in temp_hrefs:
                        if temp_hrefs[week]['href'] == item.file_name:
                            html = BeautifulSoup(item.content.decode(), 'html.parser')
                            all_p = html.find_all('p')
                            for p in all_p:
                                if p and p.find_all('span', {'id': 'pcitationsource1'}):
                                    w_json[language][week]['transition_song'] = get_number_from_string(p.text)
                                if p and p.find_all('span', {'id': 'pcitationsource2'}):
                                    w_json[language][week]['closing_song'] = get_number_from_string(p.text)
        if w_json:
            open(w_index_file, 'w').write(json.dumps(w_json, indent=4, ensure_ascii=False))

        self.response.emit('done')


def add_widgets(self):
    self.update_page = QWidget()
    self.update_page.setObjectName(u"update_page")

    self.update_page_vbox = QVBoxLayout(self.update_page)
    self.update_page_vbox.setObjectName(u"update_page_vbox")

    self.update_page_vbox.addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

    self.update_page_main_icon = QLabel(self.update_page)
    self.update_page_main_icon.setObjectName('update_page_main_icon')
    self.update_page_main_icon.setMinimumSize(QSize(128, 128))
    self.update_page_main_icon.setMaximumSize(QSize(128, 128))

    self.update_page_vbox.addWidget(self.update_page_main_icon, 0, Qt.AlignCenter)

    self.update_page_main_description = QLabel('Prat can automatically download EPUB files from jw.org and update indices (this is done in accordance with the Terms of Use of jw.org).', self.update_page)
    self.update_page_main_description.setObjectName('update_page_main_description')
    self.update_page_main_description.setWordWrap(True)
    self.update_page_main_description.setAlignment(Qt.AlignCenter)
    self.update_page_main_description.setMaximumWidth(600)
    self.update_page_main_description.setMinimumHeight(100)

    self.update_page_vbox.addWidget(self.update_page_main_description, 0, Qt.AlignCenter)

    self.update_page_download_and_update_button = QPushButton(self.update_page)
    self.update_page_download_and_update_button.setObjectName('update_page_download_and_update_button')
    self.update_page_download_and_update_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.update_page_download_and_update_button.sizePolicy().hasHeightForWidth())
    self.update_page_download_and_update_button.setSizePolicy(sizePolicy)
    self.update_page_download_and_update_button.clicked.connect(lambda: update_page_download_and_update_button_clicked(self))

    self.update_page_vbox.addWidget(self.update_page_download_and_update_button, 0, Qt.AlignCenter)

    self.update_page_vbox.addSpacing(12)

    self.update_page_main_local_update_description = QLabel('Alternatively, you can update indices with downloaded EPUB files on your computer.', self.update_page)
    self.update_page_main_local_update_description.setObjectName('update_page_main_local_update_description')
    self.update_page_main_local_update_description.setWordWrap(True)
    self.update_page_main_local_update_description.setAlignment(Qt.AlignHCenter | Qt.AlignBottom)
    self.update_page_main_local_update_description.setMaximumWidth(600)
    self.update_page_main_local_update_description.setMinimumHeight(40)

    self.update_page_vbox.addWidget(self.update_page_main_local_update_description, 0, Qt.AlignHCenter)

    self.update_page_main_local_update_path_label = QLabel(self.update_page)
    self.update_page_main_local_update_path_label.setObjectName('update_page_main_local_update_path_label')
    self.update_page_main_local_update_path_label.setAlignment(Qt.AlignHCenter | Qt.AlignTop)
    # self.update_page_main_local_update_path_label.setMinimumHeight(30)

    self.update_page_vbox.addWidget(self.update_page_main_local_update_path_label, 0, Qt.AlignCenter)

    self.update_page_update_local_files_frame = QFrame(self.update_page)

    self.update_page_update_local_files_frame_hbox = QHBoxLayout(self.update_page_update_local_files_frame)

    self.update_page_update_local_files_button = QPushButton(self.update_page_update_local_files_frame)
    self.update_page_update_local_files_button.setObjectName('update_page_update_local_files_button')
    self.update_page_update_local_files_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.update_page_update_local_files_button.sizePolicy().hasHeightForWidth())
    self.update_page_update_local_files_button.setSizePolicy(sizePolicy)
    self.update_page_update_local_files_button.clicked.connect(lambda: update_page_update_local_files_button_clicked(self))

    self.update_page_update_local_files_frame_hbox.addWidget(self.update_page_update_local_files_button, 0, Qt.AlignCenter)

    self.update_page_update_local_files_change_folder_button = QPushButton(self.update_page_update_local_files_frame)
    self.update_page_update_local_files_change_folder_button.setObjectName('update_page_update_local_files_change_folder_button')
    self.update_page_update_local_files_change_folder_button.setProperty('class', 'secondary_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.update_page_update_local_files_change_folder_button.sizePolicy().hasHeightForWidth())
    self.update_page_update_local_files_change_folder_button.setSizePolicy(sizePolicy)
    self.update_page_update_local_files_change_folder_button.clicked.connect(lambda: update_page_update_local_files_change_folder_button_clicked(self))

    self.update_page_update_local_files_frame_hbox.addWidget(self.update_page_update_local_files_change_folder_button, 0, Qt.AlignHCenter)

    self.update_page_vbox.addWidget(self.update_page_update_local_files_frame, 0, Qt.AlignCenter)

    self.update_page_vbox.addItem(QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

    self.stackedWidget.addWidget(self.update_page)

    def update_page_thread_download_and_update_files_ended(response):
        update_widgets(self, status=response)
        if response == 'done':
            self.index_w = get_w_index()
            self.index_clm = get_clm_index()

    self.update_page_thread_download_and_update_files = thread_download_and_update_files(self)
    self.update_page_thread_download_and_update_files.response.connect(update_page_thread_download_and_update_files_ended)


def get_datetime_from_string(string, language, year=False):
    list_of_months = get_list_of_months(language)
    string = string.replace('-', ' ')
    string = string.replace('–', ' ')
    string = string.replace('—', ' ')
    string = string.replace(' ', ' ')
    string = string.replace(' ', ' ')
    string = string.replace(',', ' ')
    string = string.replace('\n', '')
    string = string.replace(u'\xa0', u' ')

    if '(' and ')' in string:
        string = string.split('(', 1)[-1].split(')')[0]

    p = 0
    number = ''
    while p < len(string):
        if string[p:p + 1].isnumeric():
            number += string[p:p + 1]
        elif number:
            number = int(number)
            break
        p += 1

    month = 0
    for piece in string.lower().split(' '):
        for m in list_of_months[1:]:
            if m.lower() == piece:
                month = list_of_months.index(m)
                break
        else:
            continue
        break

    if not year:
        for piece in string.lower().replace('–', ' ').split(' '):
            if len(piece) == 4 and piece.isnumeric():
                year = int(piece)
                break

    if month and number and year:
        result = datetime(year=year, month=month, day=number)
    else:
        result = False
    return result


def get_year_from_string(string):
    result = False
    for slice in clean_text(string).split(' '):
        if len(slice) == 4 and slice.isnumeric():
            result = int(slice)
            break
    return result


def get_number_from_string(string):
    result = False
    string = string.replace('—', ' ')
    string = string.replace('-', ' ')
    for slice in clean_text(string).split(' '):
        if slice.isnumeric():
            result = int(slice)
            break
    return result


def clean_text(string):
    final_string = string
    final_string = final_string.replace('\n', '')
    final_string = final_string.replace(u'\xa0', u' ')
    while final_string.startswith((' ', '(')):
        final_string = final_string[1:]
    while final_string.endswith((' ', ')')):
        final_string = final_string[:-1]
    return final_string


def get_clm_index(clm_index_file=PATH_PRAT_MWB_INDEX):
    clm_json = json.loads('{}')
    if os.path.isfile(clm_index_file):
        clm_json = json.load(open(clm_index_file))

    return clm_json


def get_w_index(w_index_file=PATH_PRAT_W_INDEX):
    w_json = json.loads('{}')
    if os.path.isfile(w_index_file):
        w_json = json.load(open(w_index_file))

    return w_json


def get_last_update_date(mwb_index_file=PATH_PRAT_MWB_INDEX, w_index_file=PATH_PRAT_W_INDEX):
    final_date = False
    dates = []
    if os.path.isfile(PATH_PRAT_MWB_INDEX):
        dates.append(os.path.getmtime(PATH_PRAT_MWB_INDEX))
    if os.path.isfile(PATH_PRAT_W_INDEX):
        dates.append(os.path.getmtime(PATH_PRAT_W_INDEX))
    dates = sorted(dates)
    if dates:
        final_date = datetime.fromtimestamp(dates[-1])
    return final_date


def update_page_download_and_update_button_clicked(self):
    self.update_page_thread_download_and_update_files.download = True
    self.update_page_thread_download_and_update_files.language = get_language_pairs(self.selected_language).get('meps_language_mnemonic', 'E')
    self.update_page_thread_download_and_update_files.list_of_issues = [(datetime.today() + timedelta(days=30 * (m - 2))).strftime('%Y%m') for m in range(8)]
    self.update_page_thread_download_and_update_files.start()


def update_page_update_local_files_button_clicked(self):
    self.update_page_thread_download_and_update_files.download = False
    self.update_page_thread_download_and_update_files.epub_folder = self.settings.get('path_for_updates', os.path.join(PATH_REAL_HOME))
    self.update_page_thread_download_and_update_files.start()


def update_page_update_local_files_change_folder_button_clicked(self):
    selected_directory = QFileDialog.getExistingDirectory()
    if os.path.isdir(selected_directory):
        self.settings['path_for_updates'] = selected_directory
    update_widgets(self)


def update_widgets(self, status='idle'):
    self.update_page_main_local_update_path_label.setText(_('Path') + ': ' + self.settings.get('path_for_updates', os.path.join(PATH_REAL_HOME)))
    if status in ['idle', 'done']:
        if status == 'idle':
            self.update_page_main_icon.setPixmap(QPixmap(get_graphics_path('update_big_icon.svg')))
        elif status == 'done':
            self.update_page_main_icon.setPixmap(QPixmap(get_graphics_path('done_big_icon.svg')))
        self.update_page_main_description.setText(_('Prat can automatically download EPUB files from jw.org and update indices (this is done in accordance with the Terms of Use of jw.org).'))
        self.update_page_main_local_update_description.setText(_('Alternatively, you can update indices with downloaded EPUB files on your computer.'))
        self.update_page_main_local_update_path_label.setVisible(True)
        self.update_page_main_local_update_description.setVisible(True)
        self.update_page_download_and_update_button.setVisible(True)
        self.update_page_update_local_files_button.setVisible(True)
        self.update_page_update_local_files_change_folder_button.setVisible(True)
    else:
        if status == 'downloading':
            self.update_page_main_icon.setPixmap(QPixmap(get_graphics_path('download_big_icon.svg')))
            self.update_page_main_description.setText(_('Downloading EPUB files from jw.org... (this is done in accordance with the Terms of Use of jw.org).'))
        if status == 'reading':
            self.update_page_main_icon.setPixmap(QPixmap(get_graphics_path('reading_epub_big_icon.svg')))
            self.update_page_main_description.setText(_('Reading EPUB files...'))
        self.update_page_main_local_update_path_label.setVisible(False)
        self.update_page_main_local_update_description.setVisible(False)
        self.update_page_download_and_update_button.setVisible(False)
        self.update_page_update_local_files_button.setVisible(False)
        self.update_page_update_local_files_change_folder_button.setVisible(False)


def translate_widgets(self):
    self.update_page_download_and_update_button.setText(_('Download EPUBs and update'))
    self.update_page_update_local_files_button.setText(_('Update local EPUBs'))
    self.update_page_update_local_files_change_folder_button.setText(_('Change folder'))