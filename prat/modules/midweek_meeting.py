#!/usr/bin/env python3

import os
import json

from PySide6.QtWidgets import QWidget, QVBoxLayout, QFrame, QPushButton, QSizePolicy, QGridLayout, QLabel, QScrollArea, QLayout
from PySide6.QtGui import QCursor
from PySide6.QtCore import QSize, Qt

from prat.modules.translation import _
from prat.modules.date import get_week_start
from prat.modules.paths import PATH_PRAT_MIDWEEK_MEETING_ASSIGMENTS_FILE, get_graphics_path
from prat.modules.publishers import get_publishers_attendants, get_publishers_audio_operators, get_publishers_clm_chairman, get_publishers_media_operators, get_publishers_names, get_publishers_meetings_prayer, get_publishers_leadership, get_publishers_roving_microphones, get_publishers_tfgw_bible_reading, get_publishers_ayfm_demonstrations, get_publishers_ayfm_talks, get_publishers_bible_study_conductors, get_publishers_bible_study_readers
from prat.special_widgets import QComboBoxNoWheel, AssignmentsList

MODULE_INFO = {
    'title': _('Midweek Meeting'),
    'name': 'midweek_meeting',
    'date_enabled': True
}


def add_menu_widgets(self):
    self.modules_menu_button_midweek_meeting = QPushButton(self.modules_menu)
    self.modules_menu_button_midweek_meeting.setObjectName(u"modules_menu_button_midweek_meeting")
    self.modules_menu_button_midweek_meeting.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.modules_menu_button_midweek_meeting.sizePolicy().hasHeightForWidth())
    self.modules_menu_button_midweek_meeting.setSizePolicy(sizePolicy)
    self.modules_menu_button_midweek_meeting.setMinimumHeight(45)
    self.modules_menu_button_midweek_meeting.setCursor(QCursor(Qt.PointingHandCursor))
    self.modules_menu_button_midweek_meeting.setLayoutDirection(Qt.LeftToRight)
    self.modules_menu_button_midweek_meeting.setStyleSheet('background-image: url("' + get_graphics_path('midweek_meeting_icon.svg') + '");')
    self.modules_menu_button_midweek_meeting.clicked.connect(lambda: modules_menu_button_midweek_meeting_clicked(self))

    self.modules_menu_vl.addWidget(self.modules_menu_button_midweek_meeting)


def add_widgets(self):
    self.midweek_meeting = QWidget()
    self.midweek_meeting.setObjectName(u"midweek_meeting")

    self.midweek_meeting_vbox = QVBoxLayout(self.midweek_meeting)
    self.midweek_meeting_vbox.setObjectName(u"midweek_meeting_vbox")

    self.midweek_meeting_scroll = QScrollArea(self.midweek_meeting)
    self.midweek_meeting_scroll.setObjectName(u"midweek_meeting_scroll")
    self.midweek_meeting_scroll.setWidgetResizable(True)
    self.midweek_meeting_scroll.setFrameShape(QFrame.NoFrame)
    self.midweek_meeting_scroll.setAlignment(Qt.AlignLeading | Qt.AlignLeft | Qt.AlignTop)

    self.midweek_meeting_scroll_widget = QWidget()
    self.midweek_meeting_scroll_widget.setObjectName(u"midweek_meeting_scroll_widget")

    sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
    sizePolicy3.setHorizontalStretch(0)
    sizePolicy3.setVerticalStretch(0)
    sizePolicy3.setHeightForWidth(self.midweek_meeting_scroll_widget.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_scroll_widget.setSizePolicy(sizePolicy3)

    self.midweek_meeting_scroll_widget_vbox = QVBoxLayout(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_scroll_widget_vbox.setObjectName(u"midweek_meeting_scroll_widget_vbox")

    self.midweek_meeting_chairman_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_chairman_line.setObjectName('midweek_meeting_chairman_line')

    self.midweek_meeting_chairman_grid = QGridLayout(self.midweek_meeting_chairman_line)
    self.midweek_meeting_chairman_grid.setObjectName(u"chairman_line")

    self.midweek_meeting_chairman_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_chairman_label.setObjectName(u"chairman_label")

    self.midweek_meeting_chairman_grid.addWidget(self.midweek_meeting_chairman_label, 0, 0, 1, 1)

    self.midweek_meeting_chairman_name = QComboBoxNoWheel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_chairman_name.setEditable(True)
    # self.midweek_meeting_chairman_name.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_chairman_name.editTextChanged.connect(self.midweek_meeting_chairman_name.style().polish(self.midweek_meeting_chairman_name))
    self.midweek_meeting_chairman_name.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_chairman_name.lineEdit().editingFinished.connect(lambda: chairman_name_editingfinished(self))
    self.midweek_meeting_chairman_name.setObjectName(u"chairman_name")

    self.midweek_meeting_chairman_grid.addWidget(self.midweek_meeting_chairman_name, 1, 0, 1, 1)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_chairman_line)

    self.midweek_meeting_opening_prayer_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_opening_prayer_line.setObjectName('midweek_meeting_opening_prayer_line')

    self.midweek_meeting_opening_prayer_grid = QGridLayout(self.midweek_meeting_opening_prayer_line)
    self.midweek_meeting_opening_prayer_grid.setObjectName(u"opening_prayer_line")

    self.midweek_meeting_opening_prayer_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_opening_prayer_label.setObjectName(u"midweek_meeting_opening_prayer_label")

    self.midweek_meeting_opening_prayer_grid.addWidget(self.midweek_meeting_opening_prayer_label, 0, 0, 1, 1)

    self.midweek_meeting_opening_prayer = QComboBoxNoWheel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_opening_prayer.setEditable(True)
    # self.midweek_meeting_opening_prayer.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_opening_prayer.editTextChanged.connect(self.midweek_meeting_opening_prayer.style().polish(self.midweek_meeting_opening_prayer))
    self.midweek_meeting_opening_prayer.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_opening_prayer.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_opening_prayer.setObjectName(u"opening_prayer")

    self.midweek_meeting_opening_prayer_grid.addWidget(self.midweek_meeting_opening_prayer, 1, 0, 1, 1)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_opening_prayer_line)

    self.midweek_meeting_tfgw_area = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_tfgw_area.setObjectName(u"midweek_meeting_tfgw_area")
    # self.midweek_meeting_tfgw_area.setFrameShape(QFrame.StyledPanel)
    # self.midweek_meeting_tfgw_area.setFrameShadow(QFrame.Raised)

    self.midweek_meeting_tfgw_area_vbox = QVBoxLayout(self.midweek_meeting_tfgw_area)
    self.midweek_meeting_tfgw_area_vbox.setObjectName(u"midweek_meeting_tfgw_area_vbox")

    self.midweek_meeting_tfgw_label = QLabel(self.midweek_meeting_tfgw_area)
    self.midweek_meeting_tfgw_label.setObjectName(u"midweek_meeting_tfgw_label")
    sizePolicy4 = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy4.setHorizontalStretch(0)
    sizePolicy4.setVerticalStretch(0)
    sizePolicy4.setHeightForWidth(self.midweek_meeting_tfgw_label.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_tfgw_label.setSizePolicy(sizePolicy4)
    self.midweek_meeting_tfgw_label.setMinimumSize(QSize(0, 0))
    self.midweek_meeting_tfgw_label.setStyleSheet(u"QLabel { background-color: #656164; padding:8px; font-weight: bold; color:white; }")

    self.midweek_meeting_tfgw_area_vbox.addWidget(self.midweek_meeting_tfgw_label)

    self.midweek_meeting_tfgw_line = QWidget(self.midweek_meeting_tfgw_area)
    self.midweek_meeting_tfgw_line.setObjectName('midweek_meeting_tfgw_line')

    self.midweek_meeting_tfgw_grid = QGridLayout(self.midweek_meeting_tfgw_line)
    self.midweek_meeting_tfgw_grid.setObjectName(u"midweek_meeting_tfgw_line")
    self.midweek_meeting_tfgw_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_tfgw_title = QLabel(self.midweek_meeting_tfgw_line)
    self.midweek_meeting_tfgw_title.setObjectName(u"midweek_meeting_tfgw_title")
    # sizePolicy3.setHeightForWidth(self.midweek_meeting_tfgw_title.sizePolicy().hasHeightForWidth())
    # self.midweek_meeting_tfgw_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_tfgw_grid.addWidget(self.midweek_meeting_tfgw_title, 0, 0, 1, 1)

    self.midweek_meeting_tfgw = QComboBoxNoWheel(self.midweek_meeting_tfgw_line)
    self.midweek_meeting_tfgw.setEditable(True)
    # self.midweek_meeting_tfgw.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_tfgw.editTextChanged.connect(self.midweek_meeting_tfgw.style().polish(self.midweek_meeting_tfgw))
    self.midweek_meeting_tfgw.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_tfgw.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_tfgw.setObjectName(u"midweek_meeting_tfgw")
    # sizePolicy3.setHeightForWidth(self.midweek_meeting_tfgw.sizePolicy().hasHeightForWidth())
    # self.midweek_meeting_tfgw.setSizePolicy(sizePolicy3)

    self.midweek_meeting_tfgw_grid.addWidget(self.midweek_meeting_tfgw, 1, 0, 1, 1)

    self.midweek_meeting_tfgw_area_vbox.addWidget(self.midweek_meeting_tfgw_line)

    self.midweek_meeting_spiritual_gems_row = QWidget(self.midweek_meeting_tfgw_area)
    self.midweek_meeting_spiritual_gems_row.setObjectName('midweek_meeting_spiritual_gems_row')

    self.midweek_meeting_spiritual_gems_grid = QGridLayout(self.midweek_meeting_spiritual_gems_row)
    self.midweek_meeting_spiritual_gems_grid.setObjectName(u"midweek_meeting_spiritual_gems_grid")

    self.midweek_meeting_spiritual_gems_title = QLabel(self.midweek_meeting_spiritual_gems_row)
    self.midweek_meeting_spiritual_gems_title.setObjectName(u"midweek_meeting_spiritual_gems_title")

    self.midweek_meeting_spiritual_gems_grid.addWidget(self.midweek_meeting_spiritual_gems_title, 0, 0, 1, 1)

    self.midweek_meeting_spiritual_gems = QComboBoxNoWheel(self.midweek_meeting_spiritual_gems_row)
    self.midweek_meeting_spiritual_gems.setEditable(True)
    # self.midweek_meeting_spiritual_gems.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_spiritual_gems.editTextChanged.connect(self.midweek_meeting_spiritual_gems.style().polish(self.midweek_meeting_spiritual_gems))
    self.midweek_meeting_spiritual_gems.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_spiritual_gems.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_spiritual_gems.setObjectName(u"midweek_meeting_spiritual_gems")

    self.midweek_meeting_spiritual_gems_grid.addWidget(self.midweek_meeting_spiritual_gems, 1, 0, 1, 1)

    self.midweek_meeting_tfgw_area_vbox.addWidget(self.midweek_meeting_spiritual_gems_row)

    self.midweek_meeting_bible_reading_row = QWidget(self.midweek_meeting_tfgw_area)
    self.midweek_meeting_bible_reading_row.setObjectName('midweek_meeting_bible_reading_row')

    self.midweek_meeting_bible_reading_grid = QGridLayout(self.midweek_meeting_bible_reading_row)
    self.midweek_meeting_bible_reading_grid.setObjectName(u"midweek_meeting_bible_reading_grid")

    self.midweek_meeting_bible_reading_title = QLabel(self.midweek_meeting_bible_reading_row)
    self.midweek_meeting_bible_reading_title.setObjectName(u"midweek_meeting_bible_reading_title")

    self.midweek_meeting_bible_reading_grid.addWidget(self.midweek_meeting_bible_reading_title, 1, 0, 1, 1)

    self.midweek_meeting_bible_reading = QComboBoxNoWheel(self.midweek_meeting_bible_reading_row)
    self.midweek_meeting_bible_reading.setEditable(True)
    # self.midweek_meeting_bible_reading.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_bible_reading.editTextChanged.connect(self.midweek_meeting_bible_reading.style().polish(self.midweek_meeting_bible_reading))
    self.midweek_meeting_bible_reading.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_bible_reading.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_bible_reading.setObjectName(u"midweek_meeting_bible_reading")

    self.midweek_meeting_bible_reading_grid.addWidget(self.midweek_meeting_bible_reading, 2, 0, 1, 1)

    self.midweek_meeting_tfgw_area_vbox.addWidget(self.midweek_meeting_bible_reading_row)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_tfgw_area)

    self.midweek_meeting_ayfm_area = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_ayfm_area.setObjectName(u"midweek_meeting_ayfm_area")
    # self.midweek_meeting_ayfm_area.setFrameShape(QFrame.StyledPanel)
    # self.midweek_meeting_ayfm_area.setFrameShadow(QFrame.Raised)

    self.midweek_meeting_ayfm_area_vbox = QVBoxLayout(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_area_vbox.setObjectName(u"midweek_meeting_ayfm_area_vbox")

    self.midweek_meeting_ayfm_label = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_label.setObjectName(u"midweek_meeting_ayfm_label")
    self.midweek_meeting_ayfm_label.setSizePolicy(sizePolicy4)
    self.midweek_meeting_ayfm_label.setMinimumSize(QSize(0, 0))
    self.midweek_meeting_ayfm_label.setStyleSheet(u"QLabel { background-color: #a56803; padding:8px; font-weight: bold; color:white;}")

    self.midweek_meeting_ayfm_area_vbox.addWidget(self.midweek_meeting_ayfm_label)

    self.midweek_meeting_ayfm_part1_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_ayfm_part1_line.setObjectName('midweek_meeting_ayfm_part1_line')

    self.midweek_meeting_ayfm_part1_grid = QGridLayout(self.midweek_meeting_ayfm_part1_line)
    self.midweek_meeting_ayfm_part1_grid.setObjectName(u"midweek_meeting_ayfm_part1_line")
    self.midweek_meeting_ayfm_part1_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_ayfm_part1_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part1_title.setObjectName(u"midweek_meeting_ayfm_part1_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part1_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part1_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part1_grid.addWidget(self.midweek_meeting_ayfm_part1_title, 0, 0, 1, 1)

    self.midweek_meeting_ayfm_part1 = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part1.setEditable(True)
    # self.midweek_meeting_ayfm_part1.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part1.editTextChanged.connect(self.midweek_meeting_ayfm_part1.style().polish(self.midweek_meeting_ayfm_part1))
    self.midweek_meeting_ayfm_part1.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part1.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part1.setObjectName(u"midweek_meeting_ayfm_part1")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part1.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part1.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part1_grid.addWidget(self.midweek_meeting_ayfm_part1, 1, 0, 1, 1)

    self.midweek_meeting_ayfm_part1_assistant_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part1_assistant_title.setObjectName(u"midweek_meeting_ayfm_part1_assistant_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part1_assistant_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part1_assistant_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part1_grid.addWidget(self.midweek_meeting_ayfm_part1_assistant_title, 0, 1, 1, 1)

    self.midweek_meeting_ayfm_part1_assistant = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part1_assistant.setEditable(True)
    # self.midweek_meeting_ayfm_part1_assistant.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part1_assistant.editTextChanged.connect(self.midweek_meeting_ayfm_part1_assistant.style().polish(self.midweek_meeting_ayfm_part1))
    self.midweek_meeting_ayfm_part1_assistant.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part1_assistant.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part1_assistant.setObjectName(u"midweek_meeting_ayfm_part1")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part1_assistant.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part1_assistant.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part1_grid.addWidget(self.midweek_meeting_ayfm_part1_assistant, 1, 1, 1, 1)

    self.midweek_meeting_ayfm_area_vbox.addWidget(self.midweek_meeting_ayfm_part1_line)

    self.midweek_meeting_ayfm_part2_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_ayfm_part2_line.setObjectName('midweek_meeting_ayfm_part2_line')

    self.midweek_meeting_ayfm_part2_grid = QGridLayout(self.midweek_meeting_ayfm_part2_line)
    self.midweek_meeting_ayfm_part2_grid.setObjectName(u"midweek_meeting_ayfm_part2_line")
    self.midweek_meeting_ayfm_part2_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_ayfm_part2_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part2_title.setObjectName(u"midweek_meeting_ayfm_part2_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part2_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part2_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part2_grid.addWidget(self.midweek_meeting_ayfm_part2_title, 0, 0, 1, 1)

    self.midweek_meeting_ayfm_part2 = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part2.setEditable(True)
    # self.midweek_meeting_ayfm_part2.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part2.editTextChanged.connect(self.midweek_meeting_ayfm_part2.style().polish(self.midweek_meeting_ayfm_part2))
    self.midweek_meeting_ayfm_part2.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part2.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part2.setObjectName(u"midweek_meeting_ayfm_part2")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part2.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part2.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part2_grid.addWidget(self.midweek_meeting_ayfm_part2, 1, 0, 1, 1)

    self.midweek_meeting_ayfm_part2_assistant_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part2_assistant_title.setObjectName(u"midweek_meeting_ayfm_part2_assistant_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part2_assistant_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part2_assistant_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part2_grid.addWidget(self.midweek_meeting_ayfm_part2_assistant_title, 0, 1, 1, 1)

    self.midweek_meeting_ayfm_part2_assistant = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part2_assistant.setEditable(True)
    # self.midweek_meeting_ayfm_part2_assistant.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part2_assistant.editTextChanged.connect(self.midweek_meeting_ayfm_part2_assistant.style().polish(self.midweek_meeting_ayfm_part2))
    self.midweek_meeting_ayfm_part2_assistant.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part2_assistant.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part2_assistant.setObjectName(u"midweek_meeting_ayfm_part2")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part2_assistant.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part2_assistant.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part2_grid.addWidget(self.midweek_meeting_ayfm_part2_assistant, 1, 1, 1, 1)

    self.midweek_meeting_ayfm_area_vbox.addWidget(self.midweek_meeting_ayfm_part2_line)

    self.midweek_meeting_ayfm_part3_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_ayfm_part3_line.setObjectName('midweek_meeting_ayfm_part3_line')

    self.midweek_meeting_ayfm_part3_grid = QGridLayout(self.midweek_meeting_ayfm_part3_line)
    self.midweek_meeting_ayfm_part3_grid.setObjectName(u"midweek_meeting_ayfm_part3_line")
    self.midweek_meeting_ayfm_part3_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_ayfm_part3_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part3_title.setObjectName(u"midweek_meeting_ayfm_part3_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part3_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part3_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part3_grid.addWidget(self.midweek_meeting_ayfm_part3_title, 0, 0, 1, 1)

    self.midweek_meeting_ayfm_part3 = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part3.setEditable(True)
    # self.midweek_meeting_ayfm_part3.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part3.editTextChanged.connect(self.midweek_meeting_ayfm_part3.style().polish(self.midweek_meeting_ayfm_part3))
    self.midweek_meeting_ayfm_part3.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part3.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part3.setObjectName(u"midweek_meeting_ayfm_part3")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part3.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part3.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part3_grid.addWidget(self.midweek_meeting_ayfm_part3, 1, 0, 1, 1)

    self.midweek_meeting_ayfm_part3_assistant_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part3_assistant_title.setObjectName(u"midweek_meeting_ayfm_part3_assistant_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part3_assistant_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part3_assistant_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part3_grid.addWidget(self.midweek_meeting_ayfm_part3_assistant_title, 0, 1, 1, 1)

    self.midweek_meeting_ayfm_part3_assistant = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part3_assistant.setEditable(True)
    # self.midweek_meeting_ayfm_part3_assistant.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part3_assistant.editTextChanged.connect(self.midweek_meeting_ayfm_part3_assistant.style().polish(self.midweek_meeting_ayfm_part3))
    self.midweek_meeting_ayfm_part3_assistant.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part3_assistant.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part3_assistant.setObjectName(u"midweek_meeting_ayfm_part3")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part3_assistant.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part3_assistant.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part3_grid.addWidget(self.midweek_meeting_ayfm_part3_assistant, 1, 1, 1, 1)

    self.midweek_meeting_ayfm_area_vbox.addWidget(self.midweek_meeting_ayfm_part3_line)

    self.midweek_meeting_ayfm_part4_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_ayfm_part4_line.setObjectName('midweek_meeting_ayfm_part4_line')

    self.midweek_meeting_ayfm_part4_grid = QGridLayout(self.midweek_meeting_ayfm_part4_line)
    self.midweek_meeting_ayfm_part4_grid.setObjectName(u"midweek_meeting_ayfm_part4_line")
    self.midweek_meeting_ayfm_part4_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_ayfm_part4_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part4_title.setObjectName(u"midweek_meeting_ayfm_part4_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part4_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part4_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part4_grid.addWidget(self.midweek_meeting_ayfm_part4_title, 0, 0, 1, 1)

    self.midweek_meeting_ayfm_part4 = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part4.setEditable(True)
    # self.midweek_meeting_ayfm_part4.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part4.editTextChanged.connect(self.midweek_meeting_ayfm_part4.style().polish(self.midweek_meeting_ayfm_part4))
    self.midweek_meeting_ayfm_part4.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part4.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part4.setObjectName(u"midweek_meeting_ayfm_part4")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part4.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part4.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part4_grid.addWidget(self.midweek_meeting_ayfm_part4, 1, 0, 1, 1)

    self.midweek_meeting_ayfm_part4_assistant_title = QLabel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part4_assistant_title.setObjectName(u"midweek_meeting_ayfm_part4_assistant_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part4_assistant_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part4_assistant_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part4_grid.addWidget(self.midweek_meeting_ayfm_part4_assistant_title, 0, 1, 1, 1)

    self.midweek_meeting_ayfm_part4_assistant = QComboBoxNoWheel(self.midweek_meeting_ayfm_area)
    self.midweek_meeting_ayfm_part4_assistant.setEditable(True)
    # self.midweek_meeting_ayfm_part4_assistant.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_ayfm_part4_assistant.editTextChanged.connect(self.midweek_meeting_ayfm_part4_assistant.style().polish(self.midweek_meeting_ayfm_part4))
    self.midweek_meeting_ayfm_part4_assistant.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_ayfm_part4_assistant.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_ayfm_part4_assistant.setObjectName(u"midweek_meeting_ayfm_part4")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_ayfm_part4_assistant.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_ayfm_part4_assistant.setSizePolicy(sizePolicy3)

    self.midweek_meeting_ayfm_part4_grid.addWidget(self.midweek_meeting_ayfm_part4_assistant, 1, 1, 1, 1)

    self.midweek_meeting_ayfm_area_vbox.addWidget(self.midweek_meeting_ayfm_part4_line)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_ayfm_area)

    self.midweek_meeting_lc_area = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_lc_area.setObjectName(u"midweek_meeting_lc_area")
    # self.midweek_meeting_lc_area.setFrameShape(QFrame.StyledPanel)
    # self.midweek_meeting_lc_area.setFrameShadow(QFrame.Raised)

    self.midweek_meeting_lc_area_vbox = QVBoxLayout(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_area_vbox.setObjectName(u"midweek_meeting_lc_area_vbox")

    self.midweek_meeting_lc_label = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_label.setObjectName(u"midweek_meeting_lc_label")
    self.midweek_meeting_lc_label.setSizePolicy(sizePolicy4)
    self.midweek_meeting_lc_label.setMinimumSize(QSize(0, 0))
    self.midweek_meeting_lc_label.setStyleSheet(u"QLabel { background-color: #99131e; padding:8px; font-weight: bold; color:white;}")

    self.midweek_meeting_lc_area_vbox.addWidget(self.midweek_meeting_lc_label)

    self.midweek_meeting_lc_part1_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_lc_part1_line.setObjectName('midweek_meeting_lc_part1_line')

    self.midweek_meeting_lc_part1_grid = QGridLayout(self.midweek_meeting_lc_part1_line)
    self.midweek_meeting_lc_part1_grid.setObjectName(u"midweek_meeting_lc_part1_line")
    self.midweek_meeting_lc_part1_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_lc_part1_title = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_part1_title.setObjectName(u"midweek_meeting_lc_part1_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_part1_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_part1_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_part1_grid.addWidget(self.midweek_meeting_lc_part1_title, 0, 0, 1, 1)

    self.midweek_meeting_lc_part1 = QComboBoxNoWheel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_part1.setEditable(True)
    # self.midweek_meeting_lc_part1.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_lc_part1.editTextChanged.connect(self.midweek_meeting_lc_part1.style().polish(self.midweek_meeting_lc_part1))
    self.midweek_meeting_lc_part1.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_lc_part1.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_lc_part1.setObjectName(u"midweek_meeting_lc_part1")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_part1.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_part1.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_part1_grid.addWidget(self.midweek_meeting_lc_part1, 1, 0, 1, 1)

    self.midweek_meeting_lc_area_vbox.addWidget(self.midweek_meeting_lc_part1_line)

    self.midweek_meeting_lc_part2_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_lc_part2_line.setObjectName('midweek_meeting_lc_part2_line')

    self.midweek_meeting_lc_part2_grid = QGridLayout(self.midweek_meeting_lc_part2_line)
    self.midweek_meeting_lc_part2_grid.setObjectName(u"midweek_meeting_lc_part2_line")
    self.midweek_meeting_lc_part2_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_lc_part2_title = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_part2_title.setObjectName(u"midweek_meeting_lc_part2_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_part2_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_part2_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_part2_grid.addWidget(self.midweek_meeting_lc_part2_title, 0, 0, 1, 1)

    self.midweek_meeting_lc_part2 = QComboBoxNoWheel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_part2.setEditable(True)
    # self.midweek_meeting_lc_part2.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_lc_part2.editTextChanged.connect(self.midweek_meeting_lc_part2.style().polish(self.midweek_meeting_lc_part2))
    self.midweek_meeting_lc_part2.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_lc_part2.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_lc_part2.setObjectName(u"midweek_meeting_lc_part2")
    # sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_part2.sizePolicy().hasHeightForWidth())
    # self.midweek_meeting_lc_part2.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_part2_grid.addWidget(self.midweek_meeting_lc_part2, 1, 0, 1, 1)

    self.midweek_meeting_lc_area_vbox.addWidget(self.midweek_meeting_lc_part2_line)

    self.midweek_meeting_lc_part3_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_lc_part3_line.setObjectName('midweek_meeting_lc_part3_line')

    self.midweek_meeting_lc_part3_grid = QGridLayout(self.midweek_meeting_lc_part3_line)
    self.midweek_meeting_lc_part3_grid.setObjectName(u"midweek_meeting_lc_part3_line")
    self.midweek_meeting_lc_part3_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_lc_part3_title = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_part3_title.setObjectName(u"midweek_meeting_lc_part3_title")
    # sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_part3_title.sizePolicy().hasHeightForWidth())
    # self.midweek_meeting_lc_part3_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_part3_grid.addWidget(self.midweek_meeting_lc_part3_title, 0, 0, 1, 1)

    self.midweek_meeting_lc_part3 = QComboBoxNoWheel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_part3.setEditable(True)
    # self.midweek_meeting_lc_part3.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_lc_part3.editTextChanged.connect(self.midweek_meeting_lc_part3.style().polish(self.midweek_meeting_lc_part3))
    self.midweek_meeting_lc_part3.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_lc_part3.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_lc_part3.setObjectName(u"midweek_meeting_lc_part3")
    # sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_part3.sizePolicy().hasHeightForWidth())
    # self.midweek_meeting_lc_part3.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_part3_grid.addWidget(self.midweek_meeting_lc_part3, 1, 0, 1, 1)

    self.midweek_meeting_lc_area_vbox.addWidget(self.midweek_meeting_lc_part3_line)

    self.midweek_meeting_lc_cong_bible_study_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_lc_cong_bible_study_line.setObjectName('midweek_meeting_lc_cong_bible_study_line')

    self.midweek_meeting_lc_cong_bible_study_grid = QGridLayout(self.midweek_meeting_lc_cong_bible_study_line)
    self.midweek_meeting_lc_cong_bible_study_grid.setObjectName(u"midweek_meeting_lc_cong_bible_study_line")
    self.midweek_meeting_lc_cong_bible_study_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.midweek_meeting_lc_cong_bible_study_title = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_cong_bible_study_title.setObjectName(u"midweek_meeting_lc_cong_bible_study_title")
    # sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_cong_bible_study_title.sizePolicy().hasHeightForWidth())
    # self.midweek_meeting_lc_cong_bible_study_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_cong_bible_study_grid.addWidget(self.midweek_meeting_lc_cong_bible_study_title, 0, 0, 1, 1)

    self.midweek_meeting_lc_cong_bible_study_conductor_title = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_cong_bible_study_conductor_title.setObjectName(u"midweek_meeting_lc_cong_bible_study_conductor_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_cong_bible_study_conductor_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_cong_bible_study_conductor_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_cong_bible_study_grid.addWidget(self.midweek_meeting_lc_cong_bible_study_conductor_title, 1, 0, 1, 1)

    self.midweek_meeting_lc_cong_bible_study_conductor = QComboBoxNoWheel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_cong_bible_study_conductor.setEditable(True)
    # self.midweek_meeting_lc_cong_bible_study_conductor.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_lc_cong_bible_study_conductor.editTextChanged.connect(self.midweek_meeting_lc_cong_bible_study_conductor.style().polish(self.midweek_meeting_lc_cong_bible_study_conductor))
    self.midweek_meeting_lc_cong_bible_study_conductor.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_lc_cong_bible_study_conductor.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_lc_cong_bible_study_conductor.setObjectName(u"midweek_meeting_lc_cong_bible_study")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_cong_bible_study_conductor.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_cong_bible_study_conductor.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_cong_bible_study_grid.addWidget(self.midweek_meeting_lc_cong_bible_study_conductor, 2, 0, 1, 1)

    self.midweek_meeting_lc_cong_bible_study_reader_title = QLabel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_cong_bible_study_reader_title.setObjectName(u"midweek_meeting_lc_cong_bible_study_reader_title")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_cong_bible_study_reader_title.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_cong_bible_study_reader_title.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_cong_bible_study_grid.addWidget(self.midweek_meeting_lc_cong_bible_study_reader_title, 3, 0, 1, 1)

    self.midweek_meeting_lc_cong_bible_study_reader = QComboBoxNoWheel(self.midweek_meeting_lc_area)
    self.midweek_meeting_lc_cong_bible_study_reader.setEditable(True)
    # self.midweek_meeting_lc_cong_bible_study_reader.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_lc_cong_bible_study_reader.editTextChanged.connect(self.midweek_meeting_lc_cong_bible_study_reader.style().polish(self.midweek_meeting_lc_cong_bible_study_reader))
    self.midweek_meeting_lc_cong_bible_study_reader.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_lc_cong_bible_study_reader.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_lc_cong_bible_study_reader.setObjectName(u"midweek_meeting_lc_cong_bible_study")
    sizePolicy3.setHeightForWidth(self.midweek_meeting_lc_cong_bible_study_reader.sizePolicy().hasHeightForWidth())
    self.midweek_meeting_lc_cong_bible_study_reader.setSizePolicy(sizePolicy3)

    self.midweek_meeting_lc_cong_bible_study_grid.addWidget(self.midweek_meeting_lc_cong_bible_study_reader, 4, 0, 1, 1)

    self.midweek_meeting_lc_area_vbox.addWidget(self.midweek_meeting_lc_cong_bible_study_line)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_lc_area)

    self.midweek_meeting_closing_prayer_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_closing_prayer_line.setObjectName('midweek_meeting_closing_prayer_line')

    self.midweek_meeting_closing_prayer_grid = QGridLayout(self.midweek_meeting_closing_prayer_line)
    self.midweek_meeting_closing_prayer_grid.setObjectName(u"closing_prayer_line")

    self.midweek_meeting_closing_prayer_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_closing_prayer_label.setObjectName(u"midweek_meeting_closing_prayer_label")

    self.midweek_meeting_closing_prayer_grid.addWidget(self.midweek_meeting_closing_prayer_label, 0, 0, 1, 1)

    self.midweek_meeting_closing_prayer = QComboBoxNoWheel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_closing_prayer.setEditable(True)
    # self.midweek_meeting_closing_prayer.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.midweek_meeting_closing_prayer.editTextChanged.connect(self.midweek_meeting_closing_prayer.style().polish(self.midweek_meeting_closing_prayer))
    self.midweek_meeting_closing_prayer.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_closing_prayer.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_closing_prayer.setObjectName(u"closing_prayer")

    self.midweek_meeting_closing_prayer_grid.addWidget(self.midweek_meeting_closing_prayer, 1, 0, 1, 1)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_closing_prayer_line)
    self.midweek_meeting_scroll_widget_vbox.addSpacing(8)

    self.midweek_meeting_routine_tasks_separator = QFrame()
    self.midweek_meeting_routine_tasks_separator.setObjectName('midweek_meeting_routine_tasks_separator')
    self.midweek_meeting_routine_tasks_separator.setFrameShape(QFrame.HLine)
    self.midweek_meeting_routine_tasks_separator.setFrameShadow(QFrame.Sunken)

    self.midweek_meeting_scroll_widget_vbox.addSpacing(8)
    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_routine_tasks_separator)

    self.midweek_meeting_routine_tasks_line = QWidget(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_line.setObjectName('midweek_meeting_routine_tasks_line')
    self.midweek_meeting_routine_tasks_line.setLayout(QGridLayout())

    self.midweek_meeting_routine_tasks_attendants_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_attendants_label.setObjectName(u"midweek_meeting_routine_tasks_attendants_label")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_attendants_label, 0, 0, 1, 1)

    self.midweek_meeting_routine_tasks_attendants = AssignmentsList(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_attendants.item_changed.connect(lambda: update_assignments(self))
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_attendants, 1, 0, 1, 1)

    self.midweek_meeting_routine_tasks_microphone_operators_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_microphone_operators_label.setObjectName(u"midweek_meeting_routine_tasks_microphone_operators_label")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_microphone_operators_label, 2, 0, 1, 1)

    self.midweek_meeting_routine_tasks_microphone_operators = AssignmentsList(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_microphone_operators.item_changed.connect(lambda: update_assignments(self))
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_microphone_operators, 3, 0, 1, 1)

    self.midweek_meeting_routine_tasks_audio_operator_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_audio_operator_label.setObjectName(u"midweek_meeting_routine_tasks_audio_operator_label")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_audio_operator_label, 4, 0, 1, 1)

    self.midweek_meeting_routine_tasks_audio_operator = QComboBoxNoWheel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_audio_operator.setEditable(True)
    self.midweek_meeting_routine_tasks_audio_operator.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_routine_tasks_audio_operator.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_routine_tasks_audio_operator.setObjectName(u"midweek_meeting_routine_tasks_audio_operator")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_audio_operator, 5, 0, 1, 1)

    self.midweek_meeting_routine_tasks_platform_operator_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_platform_operator_label.setObjectName(u"midweek_meeting_routine_tasks_platform_operator_label")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_platform_operator_label, 6, 0, 1, 1)

    self.midweek_meeting_routine_tasks_platform_operator = QComboBoxNoWheel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_platform_operator.setEditable(True)
    self.midweek_meeting_routine_tasks_platform_operator.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_routine_tasks_platform_operator.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_routine_tasks_platform_operator.setObjectName(u"midweek_meeting_routine_tasks_platform_operator")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_platform_operator, 7, 0, 1, 1)

    self.midweek_meeting_routine_tasks_video_operator_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_video_operator_label.setObjectName(u"midweek_meeting_routine_tasks_video_operator_label")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_video_operator_label, 8, 0, 1, 1)

    self.midweek_meeting_routine_tasks_video_operator = QComboBoxNoWheel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_video_operator.setEditable(True)
    self.midweek_meeting_routine_tasks_video_operator.setFocusPolicy(Qt.StrongFocus)
    self.midweek_meeting_routine_tasks_video_operator.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.midweek_meeting_routine_tasks_video_operator.setObjectName(u"midweek_meeting_routine_tasks_video_operator")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_video_operator, 9, 0, 1, 1)

    self.midweek_meeting_routine_tasks_videoconference_attendants_label = QLabel(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_videoconference_attendants_label.setObjectName(u"midweek_meeting_routine_tasks_videoconference_attendants_label")
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_videoconference_attendants_label, 10, 0, 1, 1)

    self.midweek_meeting_routine_tasks_videoconference_attendants = AssignmentsList(self.midweek_meeting_scroll_widget)
    self.midweek_meeting_routine_tasks_videoconference_attendants.item_changed.connect(lambda: update_assignments(self))
    self.midweek_meeting_routine_tasks_line.layout().addWidget(self.midweek_meeting_routine_tasks_videoconference_attendants, 11, 0, 1, 1)

    self.midweek_meeting_scroll_widget_vbox.addWidget(self.midweek_meeting_routine_tasks_line)

    self.midweek_meeting_scroll.setWidget(self.midweek_meeting_scroll_widget)

    self.midweek_meeting_vbox.addWidget(self.midweek_meeting_scroll)

    self.stackedWidget.addWidget(self.midweek_meeting)


def modules_menu_button_midweek_meeting_clicked(self):
    self.selected_module = MODULE_INFO
    load_assignments(self)
    self.stackedWidget.setCurrentWidget(self.midweek_meeting)
    self.toggle_menu(True)
    self.resetStyle(self.modules_menu_button_midweek_meeting.objectName())
    self.modules_menu_button_midweek_meeting.setStyleSheet(self.selectMenu(self.modules_menu_button_midweek_meeting.styleSheet()))


def translate_widgets(self):
    self.modules_menu_button_midweek_meeting.setText(_(MODULE_INFO['title']))
    self.midweek_meeting_chairman_label.setText(_('Chairman'))
    self.midweek_meeting_opening_prayer_label.setText(_('Opening prayer'))
    self.midweek_meeting_closing_prayer_label.setText(_('Closing prayer'))
    self.midweek_meeting_tfgw_label.setText(_("Treasures from God's Word").upper())
    self.midweek_meeting_ayfm_part1_assistant_title.setText(_('Assistant'))
    self.midweek_meeting_ayfm_part2_assistant_title.setText(_('Assistant'))
    self.midweek_meeting_ayfm_part3_assistant_title.setText(_('Assistant'))
    self.midweek_meeting_ayfm_part4_assistant_title.setText(_('Assistant'))
    self.midweek_meeting_ayfm_label.setText(_('Apply Yourself to the Field Ministry').upper())
    self.midweek_meeting_lc_label.setText(_('Living as Christians').upper())
    self.midweek_meeting_lc_cong_bible_study_conductor_title.setText(_('Conductor'))
    self.midweek_meeting_lc_cong_bible_study_reader_title.setText(_('Reader'))
    self.midweek_meeting_routine_tasks_attendants_label.setText(_('Attendants'))
    self.midweek_meeting_routine_tasks_microphone_operators_label.setText(_('Microphone operators'))
    self.midweek_meeting_routine_tasks_audio_operator_label.setText(_('Audio operator'))
    self.midweek_meeting_routine_tasks_platform_operator_label.setText(_('Platform operator'))
    self.midweek_meeting_routine_tasks_video_operator_label.setText(_('Video operator'))
    self.midweek_meeting_routine_tasks_videoconference_attendants_label.setText(_('Videoconference attendants'))


def update_date(self):
    week_start = get_week_start(self.selected_date)

    tfgw_index = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('tfgw', {})

    self.midweek_meeting_tfgw_area.setVisible(bool(tfgw_index))
    self.midweek_meeting_tfgw_title.setText(tfgw_index.get('tfgw_title', ''))
    self.midweek_meeting_spiritual_gems_title.setText(tfgw_index.get('tfgw_spiritual_gems_title', ''))
    self.midweek_meeting_bible_reading_title.setText(tfgw_index.get('tfgw_bible_reading_title', ''))

    ayfm_index = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {})

    self.midweek_meeting_ayfm_area.setVisible(bool(ayfm_index))
    self.midweek_meeting_ayfm_part1_line.setVisible(bool(ayfm_index.get('ayfm_part1_title', '')))
    self.midweek_meeting_ayfm_part1_title.setText(ayfm_index.get('ayfm_part1_title', ''))
    self.midweek_meeting_ayfm_part1_assistant_title.setVisible(not (bool(ayfm_index.get('ayfm_part1_is_talk', '')) or bool(ayfm_index.get('ayfm_part1_has_video', ''))))
    self.midweek_meeting_ayfm_part1_assistant.setVisible(not (bool(ayfm_index.get('ayfm_part1_is_talk', '')) or bool(ayfm_index.get('ayfm_part1_has_video', ''))))
    self.midweek_meeting_ayfm_part2_line.setVisible(bool(ayfm_index.get('ayfm_part2_title', '')))
    self.midweek_meeting_ayfm_part2_title.setText(ayfm_index.get('ayfm_part2_title', ''))
    self.midweek_meeting_ayfm_part2_assistant_title.setVisible(not (bool(ayfm_index.get('ayfm_part2_is_talk', '')) or bool(ayfm_index.get('ayfm_part2_has_video', ''))))
    self.midweek_meeting_ayfm_part2_assistant.setVisible(not (bool(ayfm_index.get('ayfm_part2_is_talk', '')) or bool(ayfm_index.get('ayfm_part2_has_video', ''))))
    self.midweek_meeting_ayfm_part3_line.setVisible(bool(ayfm_index.get('ayfm_part3_title', '')))
    self.midweek_meeting_ayfm_part3_title.setText(ayfm_index.get('ayfm_part3_title', ''))
    self.midweek_meeting_ayfm_part3_assistant_title.setVisible(not (bool(ayfm_index.get('ayfm_part3_is_talk', '')) or bool(ayfm_index.get('ayfm_part3_has_video', ''))))
    self.midweek_meeting_ayfm_part3_assistant.setVisible(not (bool(ayfm_index.get('ayfm_part3_is_talk', '')) or bool(ayfm_index.get('ayfm_part3_has_video', ''))))
    self.midweek_meeting_ayfm_part4_line.setVisible(bool(ayfm_index.get('ayfm_part4_title', '')))
    self.midweek_meeting_ayfm_part4_title.setText(ayfm_index.get('ayfm_part4_title', ''))
    self.midweek_meeting_ayfm_part4_assistant_title.setVisible(not (bool(ayfm_index.get('ayfm_part4_is_talk', '')) or bool(ayfm_index.get('ayfm_part4_has_video', ''))))
    self.midweek_meeting_ayfm_part4_assistant.setVisible(not (bool(ayfm_index.get('ayfm_part4_is_talk', '')) or bool(ayfm_index.get('ayfm_part4_has_video', ''))))

    lc_index = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('lc', {})

    self.midweek_meeting_lc_area.setVisible(bool(lc_index))
    self.midweek_meeting_lc_part1_line.setVisible(bool(lc_index.get('lc_part1_title', '')))
    self.midweek_meeting_lc_part1_title.setText(lc_index.get('lc_part1_title', ''))
    self.midweek_meeting_lc_part2_line.setVisible(bool(lc_index.get('lc_part2_title', '')))
    self.midweek_meeting_lc_part2_title.setText(lc_index.get('lc_part2_title', ''))
    self.midweek_meeting_lc_part3_line.setVisible(bool(lc_index.get('lc_part3_title', '')))
    self.midweek_meeting_lc_part3_title.setText(lc_index.get('lc_part3_title', ''))
    self.midweek_meeting_lc_cong_bible_study_title.setText(lc_index.get('lc_bible_study_title', ''))

    update_assignment_widgets(self)


MODULE_INFO['date_function'] = update_date


def update_assignment_widgets(self):
    week_start = get_week_start(self.selected_date)
    clm_chairman_list = get_publishers_clm_chairman(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_chairman_name.clear()
    self.midweek_meeting_chairman_name.addItems(clm_chairman_list)

    opening_prayer_list = get_publishers_meetings_prayer(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_opening_prayer.clear()
    self.midweek_meeting_opening_prayer.addItems(opening_prayer_list)

    tfgw_list = get_publishers_leadership(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_tfgw.clear()
    self.midweek_meeting_tfgw.addItems(tfgw_list)

    self.midweek_meeting_spiritual_gems.clear()
    self.midweek_meeting_spiritual_gems.addItems(tfgw_list)

    tfgw_bible_reading_list = get_publishers_tfgw_bible_reading(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_bible_reading.clear()
    self.midweek_meeting_bible_reading.addItems(tfgw_bible_reading_list)

    # FIX THIS: set if it's for brother or sister
    ayfm_demonstration_list = get_publishers_ayfm_demonstrations(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))

    ayfm_short_talks_list = get_publishers_ayfm_talks(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))

    self.midweek_meeting_ayfm_part1.clear()
    if bool(self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {}).get('ayfm_part1_is_talk', '')):
        self.midweek_meeting_ayfm_part1.addItems(tfgw_list)
    else:
        self.midweek_meeting_ayfm_part1.addItems(ayfm_demonstration_list)

    self.midweek_meeting_ayfm_part2.clear()
    if bool(self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {}).get('ayfm_part2_is_talk', '')):
        self.midweek_meeting_ayfm_part2.addItems(ayfm_short_talks_list)
    else:
        self.midweek_meeting_ayfm_part2.addItems(ayfm_demonstration_list)

    self.midweek_meeting_ayfm_part3.clear()
    if bool(self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {}).get('ayfm_part3_is_talk', '')):
        self.midweek_meeting_ayfm_part3.addItems(ayfm_short_talks_list)
    else:
        self.midweek_meeting_ayfm_part3.addItems(ayfm_demonstration_list)

    self.midweek_meeting_ayfm_part4.clear()
    if bool(self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {}).get('ayfm_part4_is_talk', '')):
        self.midweek_meeting_ayfm_part4.addItems(ayfm_short_talks_list)
    else:
        self.midweek_meeting_ayfm_part4.addItems(ayfm_demonstration_list)

    self.midweek_meeting_ayfm_part1_assistant.clear()
    self.midweek_meeting_ayfm_part1_assistant.addItems(ayfm_demonstration_list)
    self.midweek_meeting_ayfm_part2_assistant.clear()
    self.midweek_meeting_ayfm_part2_assistant.addItems(ayfm_demonstration_list)
    self.midweek_meeting_ayfm_part3_assistant.clear()
    self.midweek_meeting_ayfm_part3_assistant.addItems(ayfm_demonstration_list)
    self.midweek_meeting_ayfm_part4_assistant.clear()
    self.midweek_meeting_ayfm_part4_assistant.addItems(ayfm_demonstration_list)

    lc_list = get_publishers_leadership(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_lc_part1.clear()
    self.midweek_meeting_lc_part1.addItems(lc_list)
    self.midweek_meeting_lc_part2.clear()
    self.midweek_meeting_lc_part2.addItems(lc_list)
    self.midweek_meeting_lc_part3.clear()
    self.midweek_meeting_lc_part3.addItems(lc_list)

    cond_list = get_publishers_bible_study_conductors(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_lc_cong_bible_study_conductor.clear()
    self.midweek_meeting_lc_cong_bible_study_conductor.addItems(cond_list)

    lc_bible_study_reader_list = get_publishers_bible_study_readers(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_lc_cong_bible_study_reader.clear()
    self.midweek_meeting_lc_cong_bible_study_reader.addItems(lc_bible_study_reader_list)

    closing_prayer_list = get_publishers_meetings_prayer(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_closing_prayer.clear()
    self.midweek_meeting_closing_prayer.addItems(closing_prayer_list)

    attendants = get_publishers_attendants(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_routine_tasks_attendants.clear()
    self.midweek_meeting_routine_tasks_attendants.setItems(attendants)

    roving_microphone = get_publishers_roving_microphones(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_routine_tasks_microphone_operators.clear()
    self.midweek_meeting_routine_tasks_microphone_operators.setItems(roving_microphone)

    audio_operators = get_publishers_audio_operators(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_routine_tasks_audio_operator.clear()
    self.midweek_meeting_routine_tasks_audio_operator.addItems(audio_operators)

    roving_microphone = get_publishers_roving_microphones(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_routine_tasks_platform_operator.clear()
    self.midweek_meeting_routine_tasks_platform_operator.addItems(roving_microphone)

    video_operators = get_publishers_media_operators(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.midweek_meeting_routine_tasks_video_operator.clear()
    self.midweek_meeting_routine_tasks_video_operator.addItems(video_operators)

    videoconference_attendants = get_publishers_media_operators(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    # self.midweek_meeting_routine_tasks_videoconference_attendants.clear()
    self.midweek_meeting_routine_tasks_videoconference_attendants.setItems(videoconference_attendants)

    week_start = get_week_start(self.selected_date)

    midweek_meeting_assignments = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {})

    self.midweek_meeting_chairman_name.setCurrentText(midweek_meeting_assignments.get('chairman', ''))
    self.midweek_meeting_opening_prayer.setCurrentText(midweek_meeting_assignments.get('opening_prayer', ''))
    self.midweek_meeting_tfgw.setCurrentText(midweek_meeting_assignments.get('tfgw', ''))
    self.midweek_meeting_spiritual_gems.setCurrentText(midweek_meeting_assignments.get('spiritual_gems', ''))
    self.midweek_meeting_bible_reading.setCurrentText(midweek_meeting_assignments.get('bible_reading', ''))

    update_assignment_ayfm_parts_widgets(self, midweek_meeting_assignments=midweek_meeting_assignments, week_start=week_start)

    self.midweek_meeting_ayfm_part4_assistant.setCurrentText(midweek_meeting_assignments.get('ayfm_part4_assistant', ''))
    self.midweek_meeting_lc_part1.setCurrentText(midweek_meeting_assignments.get('lc_part1', ''))
    self.midweek_meeting_lc_part2.setCurrentText(midweek_meeting_assignments.get('lc_part2', ''))
    self.midweek_meeting_lc_part3.setCurrentText(midweek_meeting_assignments.get('lc_part3', ''))
    self.midweek_meeting_lc_cong_bible_study_conductor.setCurrentText(midweek_meeting_assignments.get('lc_cong_bible_study_conductor', ''))
    self.midweek_meeting_lc_cong_bible_study_reader.setCurrentText(midweek_meeting_assignments.get('lc_cong_bible_study_reader', ''))
    self.midweek_meeting_closing_prayer.setCurrentText(midweek_meeting_assignments.get('closing_prayer', ''))

    self.midweek_meeting_routine_tasks_attendants.setList(midweek_meeting_assignments.get('attendants', []))
    self.midweek_meeting_routine_tasks_microphone_operators.setList(midweek_meeting_assignments.get('microphone_operators', []))
    self.midweek_meeting_routine_tasks_platform_operator.setCurrentText(midweek_meeting_assignments.get('platform_operator', ''))
    self.midweek_meeting_routine_tasks_audio_operator.setCurrentText(midweek_meeting_assignments.get('audio_operator', ''))
    self.midweek_meeting_routine_tasks_video_operator.setCurrentText(midweek_meeting_assignments.get('video_operator', ''))
    self.midweek_meeting_routine_tasks_videoconference_attendants.setList(midweek_meeting_assignments.get('videoconference_attendants', []))


def update_assignment_ayfm_parts_widgets(self, midweek_meeting_assignments=False, week_start=False):
    if not week_start:
        week_start = get_week_start(self.selected_date)

    if not midweek_meeting_assignments:
        midweek_meeting_assignments = self.assignments['midweek_meeting'].get(week_start.strftime('%Y%m%d'), {})

    ayfm_index = self.index_clm.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('ayfm', {})

    part1_name = midweek_meeting_assignments.get('chairman', '') if bool(ayfm_index.get('ayfm_part1_has_video', '') and not midweek_meeting_assignments.get('ayfm_part1', '')) else midweek_meeting_assignments.get('ayfm_part1', '')
    self.midweek_meeting_ayfm_part1.setCurrentText(part1_name)

    self.midweek_meeting_ayfm_part1_assistant.setCurrentText(midweek_meeting_assignments.get('ayfm_part1_assistant', ''))

    part2_name = midweek_meeting_assignments.get('chairman', '') if bool(ayfm_index.get('ayfm_part2_has_video', '') and not midweek_meeting_assignments.get('ayfm_part2', '')) else midweek_meeting_assignments.get('ayfm_part2', '')
    self.midweek_meeting_ayfm_part2.setCurrentText(part2_name)

    self.midweek_meeting_ayfm_part2_assistant.setCurrentText(midweek_meeting_assignments.get('ayfm_part2_assistant', ''))

    part3_name = midweek_meeting_assignments.get('chairman', '') if bool(ayfm_index.get('ayfm_part3_has_video', '') and not midweek_meeting_assignments.get('ayfm_part3', '')) else midweek_meeting_assignments.get('ayfm_part3', '')
    self.midweek_meeting_ayfm_part3.setCurrentText(part3_name)

    self.midweek_meeting_ayfm_part3_assistant.setCurrentText(midweek_meeting_assignments.get('ayfm_part3_assistant', ''))

    part4_name = midweek_meeting_assignments.get('chairman', '') if bool(ayfm_index.get('ayfm_part4_has_video', '') and not midweek_meeting_assignments.get('ayfm_part4', '')) else midweek_meeting_assignments.get('ayfm_part4', '')
    self.midweek_meeting_ayfm_part4.setCurrentText(part4_name)


def chairman_name_editingfinished(self):
    update_assignments(self)
    update_assignment_ayfm_parts_widgets(self)


def update_assignments(self):
    week_start = get_week_start(self.selected_date)
    if not week_start.strftime('%Y%m%d') in self.assignments['midweek_meeting']:
        self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')] = {}

    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['chairman'] = self.midweek_meeting_chairman_name.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['opening_prayer'] = self.midweek_meeting_opening_prayer.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['tfgw'] = self.midweek_meeting_tfgw.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['spiritual_gems'] = self.midweek_meeting_spiritual_gems.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['bible_reading'] = self.midweek_meeting_bible_reading.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part1'] = self.midweek_meeting_ayfm_part1.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part1_assistant'] = self.midweek_meeting_ayfm_part1_assistant.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part2'] = self.midweek_meeting_ayfm_part2.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part2_assistant'] = self.midweek_meeting_ayfm_part2_assistant.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part3'] = self.midweek_meeting_ayfm_part3.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part3_assistant'] = self.midweek_meeting_ayfm_part3_assistant.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part4'] = self.midweek_meeting_ayfm_part4.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['ayfm_part4_assistant'] = self.midweek_meeting_ayfm_part4_assistant.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['lc_part1'] = self.midweek_meeting_lc_part1.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['lc_part2'] = self.midweek_meeting_lc_part2.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['lc_part3'] = self.midweek_meeting_lc_part3.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['lc_cong_bible_study_conductor'] = self.midweek_meeting_lc_cong_bible_study_conductor.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['lc_cong_bible_study_reader'] = self.midweek_meeting_lc_cong_bible_study_reader.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['closing_prayer'] = self.midweek_meeting_closing_prayer.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['attendants'] = self.midweek_meeting_routine_tasks_attendants.getList()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['microphone_operators'] = self.midweek_meeting_routine_tasks_microphone_operators.getList()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['audio_operator'] = self.midweek_meeting_routine_tasks_audio_operator.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['platform_operator'] = self.midweek_meeting_routine_tasks_platform_operator.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['video_operator'] = self.midweek_meeting_routine_tasks_video_operator.currentText()
    self.assignments['midweek_meeting'][week_start.strftime('%Y%m%d')]['videoconference_attendants'] = self.midweek_meeting_routine_tasks_videoconference_attendants.getList()


def save_assignments(self, midweek_meeting_assignmets_file=PATH_PRAT_MIDWEEK_MEETING_ASSIGMENTS_FILE):
    if 'midweek_meeting' in self.assignments:
        open(midweek_meeting_assignmets_file, 'w').write(json.dumps(self.assignments['midweek_meeting'], indent=4, ensure_ascii=False))


MODULE_INFO['save_function'] = save_assignments


def load_assignments(self, midweek_meeting_assignmets_file=PATH_PRAT_MIDWEEK_MEETING_ASSIGMENTS_FILE):
    if not self.assignments.get('midweek_meeting', {}):
        midweek_meeting_assignments_json = json.loads('{}')
        if os.path.isfile(midweek_meeting_assignmets_file):
            midweek_meeting_assignments_json = json.load(open(midweek_meeting_assignmets_file))

        self.assignments['midweek_meeting'] = midweek_meeting_assignments_json
