#!/usr/bin/python3
"""All path definitions for Prat"""

import os
import sys
import tempfile
import subprocess
import prat

PATH_PRAT = os.path.dirname(prat.__file__)

PATH_HOME = os.path.expanduser("~")
PATH_REAL_HOME = PATH_HOME

tempdir = tempfile.TemporaryDirectory()
PATH_PRAT_TMP  = tempdir.name
PATH_PRAT_USER_CONFIG_FOLDER = os.path.join(PATH_HOME, '.config', 'prat')

if sys.platform == 'darwin':
    PATH_PRAT_USER_CONFIG_FOLDER = os.path.join(PATH_HOME, 'Library', 'Application Support', 'prat')
elif sys.platform == 'win32' or os.name == 'nt':
    PATH_PRAT = os.path.abspath(os.path.dirname(sys.argv[0]))
    PATH_PRAT_USER_CONFIG_FOLDER = os.path.join(os.getenv('LOCALAPPDATA'), 'prat')
else:
    try:
        PATH_REAL_HOME = subprocess.Popen(['getent', 'passwd', str(os.getuid())], stdout=subprocess.PIPE).stdout.read().decode().split(':')[5]
    except FileNotFoundError:
        pass
    if not os.path.isdir(os.path.join(PATH_HOME, '.config')):
        os.mkdir(os.path.join(PATH_HOME, '.config'))

if not os.path.isdir(PATH_PRAT_USER_CONFIG_FOLDER):
    os.mkdir(PATH_PRAT_USER_CONFIG_FOLDER)

PATH_PRAT_DOCUMENTS = os.path.join(PATH_PRAT, 'documents')
PATH_PRAT_GRAPHICS = os.path.join(PATH_PRAT, 'graphics')

PATH_PRAT_USER_CONFIG_BACKUP_FOLDER = os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'backup')

if not os.path.isdir(PATH_PRAT_USER_CONFIG_BACKUP_FOLDER):
    os.mkdir(PATH_PRAT_USER_CONFIG_BACKUP_FOLDER)

PATH_PRAT_MIDWEEK_MEETING_ASSIGMENTS_FILE = os.path.join(PATH_PRAT_USER_CONFIG_BACKUP_FOLDER, 'midweek_meeting.json')
PATH_PRAT_WEEKEND_MEETING_ASSIGMENTS_FILE = os.path.join(PATH_PRAT_USER_CONFIG_BACKUP_FOLDER, 'weekend_meeting.json')

PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE = os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'publishers.json')

PATH_PRAT_USER_CONFIG_TALKS_FILE = os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'public_talks.json')

PATH_PRAT_USER_CONFIG_FILE = os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'config.json')

PATH_PRAT_MWB_INDEX = os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'meeting_workbook.json')
PATH_PRAT_W_INDEX = os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'watchtower.json')

PATH_PRAT_DOCUMENTS_LIST = os.listdir(PATH_PRAT_DOCUMENTS)

def get_graphics_path(filename):
    final_filepath = os.path.join(PATH_PRAT_GRAPHICS, filename)
    if sys.platform == 'win32' or os.name == 'nt':
        final_filepath = final_filepath.replace('\\', '/')
    return final_filepath