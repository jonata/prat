#!/usr/bin/env python3

import os
import json

from PySide6.QtWidgets import QTableWidget, QTableWidgetItem, QWidget, QVBoxLayout, QPushButton, QSizePolicy, QScrollArea, QFrame, QHBoxLayout, QFileDialog
from PySide6.QtGui import QCursor
from PySide6.QtCore import Qt

from prat.modules.translation import _
from prat.modules.indices import get_number_from_string
from prat.modules.paths import PATH_REAL_HOME, get_graphics_path, PATH_PRAT_USER_CONFIG_TALKS_FILE


MODULE_INFO = {
    'title': _('Public Talks'),
    'name': 'public_talks',
    'date_enabled': False
}


def add_menu_widgets(self):
    self.modules_menu_button_public_talks = QPushButton(self.modules_menu)
    self.modules_menu_button_public_talks.setObjectName(u"modules_menu_button_public_talks")
    self.modules_menu_button_public_talks.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.modules_menu_button_public_talks.sizePolicy().hasHeightForWidth())
    self.modules_menu_button_public_talks.setSizePolicy(sizePolicy)
    self.modules_menu_button_public_talks.setMinimumHeight(45)
    self.modules_menu_button_public_talks.setCursor(QCursor(Qt.PointingHandCursor))
    self.modules_menu_button_public_talks.setLayoutDirection(Qt.LeftToRight)
    self.modules_menu_button_public_talks.setStyleSheet('background-image: url("' + get_graphics_path('public_talks_icon.svg') + '");')
    self.modules_menu_button_public_talks.clicked.connect(lambda: modules_menu_button_public_talks_clicked(self))

    self.modules_menu_vl.addWidget(self.modules_menu_button_public_talks)


def add_widgets(self):
    self.public_talks_page = QWidget()
    self.public_talks_page.setObjectName(u"public_talks")

    self.public_talks_vbox = QVBoxLayout(self.public_talks_page)
    self.public_talks_vbox.setObjectName(u"public_talks_vbox")

    self.public_talks_scroll = QScrollArea(self.public_talks_page)
    self.public_talks_scroll.setObjectName(u"public_talks_scroll")
    self.public_talks_scroll.setWidgetResizable(True)
    self.public_talks_scroll.setFrameShape(QFrame.NoFrame)
    self.public_talks_scroll.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

    self.public_talks_scroll_widget = QWidget()
    self.public_talks_scroll_widget.setObjectName(u"public_talks_scroll_widget")

    # sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
    # sizePolicy3.setHorizontalStretch(0)
    # sizePolicy3.setVerticalStretch(0)
    # sizePolicy3.setHeightForWidth(self.public_talks_scroll_widget.sizePolicy().hasHeightForWidth())
    # self.public_talks_scroll_widget.setSizePolicy(sizePolicy3)

    self.public_talks_scroll_widget_vbox = QVBoxLayout(self.public_talks_scroll_widget)
    self.public_talks_scroll_widget_vbox.setObjectName(u"public_talks_scroll_widget_vbox")

    self.public_talks_table = QTableWidget(self.public_talks_scroll_widget)
    self.public_talks_table.horizontalHeader().hide()
    self.public_talks_table.verticalHeader().hide()
    self.public_talks_table.setColumnCount(2)
    self.public_talks_table.setColumnWidth(0, 100)
    self.public_talks_table.setColumnWidth(1, 400)

    self.public_talks_scroll_widget_vbox.addWidget(self.public_talks_table)

    self.public_talks_final_buttons_frame = QWidget()
    self.public_talks_final_buttons_frame_hbox = QHBoxLayout(self.public_talks_final_buttons_frame)

    self.public_talks_import_button = QPushButton(self.public_talks_final_buttons_frame)
    self.public_talks_import_button.setProperty('class', 'default_button')
    self.public_talks_import_button.clicked.connect(lambda: public_talks_import_button_clicked(self))
    self.public_talks_final_buttons_frame_hbox.addWidget(self.public_talks_import_button)

    self.public_talks_final_buttons_frame_hbox.addStretch()

    self.public_talks_scroll_widget_vbox.addWidget(self.public_talks_final_buttons_frame)

    self.public_talks_scroll.setWidget(self.public_talks_scroll_widget)

    self.public_talks_vbox.addWidget(self.public_talks_scroll)

    self.stackedWidget.addWidget(self.public_talks_page)


def modules_menu_button_public_talks_clicked(self):
    self.selected_module = MODULE_INFO
    update_widgets(self)
    self.stackedWidget.setCurrentWidget(self.public_talks_page)
    self.toggle_menu(True)
    self.resetStyle(self.modules_menu_button_public_talks.objectName())
    self.modules_menu_button_public_talks.setStyleSheet(self.selectMenu(self.modules_menu_button_public_talks.styleSheet()))


def translate_widgets(self):
    self.modules_menu_button_public_talks.setText(_(MODULE_INFO['title']))
    self.public_talks_import_button.setText(_('Import themes'))
    # self.public_talks_midweek_meeting_main_label.setText(_('Midweek Meeting'))
    # self.public_talks_midweek_meeting_weekday_label.setText(_('Weekday'))
    # self.public_talks_midweek_meeting_weekday.clear()
    # list_of_weekdays = get_list_of_weekdays(self.selected_language)
    # self.public_talks_midweek_meeting_weekday.clear()
    # self.public_talks_midweek_meeting_weekday.addItems(list_of_weekdays)
    # self.public_talks_midweek_meeting_time_label.setText(_('Starting time'))
    # self.public_talks_weekend_meeting_weekday_label.setText(_('Weekday'))
    # self.public_talks_weekend_meeting_main_label.setText(_('Weekend Meeting'))
    # self.public_talks_weekend_meeting_weekday.clear()
    # self.public_talks_weekend_meeting_weekday.addItems(list_of_weekdays)
    # self.public_talks_weekend_meeting_time_label.setText(_('Starting time'))
    # self.public_talks_name_label.setText(_('Congregation name'))


def update_widgets(self):
    self.public_talks_table.setVisible(bool(self.public_talks_config.get('themes_list', {})))
    self.public_talks_table.clear()

    themes_list = self.public_talks_config.get('themes_list', {})
    self.public_talks_table.setRowCount(len(themes_list))

    row = 0
    for theme_number in themes_list:
        item = QTableWidgetItem(str(theme_number))
        self.public_talks_table.setItem(row, 0, item)
        item = QTableWidgetItem(str(themes_list[theme_number]))
        self.public_talks_table.setItem(row, 1, item)
        row += 1
    # self.public_talks_midweek_meeting_time.setText(self.settings.get('midweek_meeting_time', ''))
    # self.public_talks_midweek_meeting_weekday.setCurrentIndex(self.settings.get('midweek_meeting_weekday', 3))
    # self.public_talks_weekend_meeting_time.setText(self.settings.get('weekend_meeting_time', ''))
    # self.public_talks_weekend_meeting_weekday.setCurrentIndex(self.settings.get('weekend_meeting_weekday', 5))
    # self.public_talks_name.setText(self.settings.get('public_talks_name', ''))
    # self.public_talks_name.setReadOnly(bool(self.settings.get('public_talks_name', '')))
    # self.public_talks_name.setStyleSheet(self.public_talks_name.styleSheet())


def add_talk_themes(self, themes_list={}):
    if not 'themes_list' in self.public_talks_config:
        self.public_talks_config['themes_list'] = {}
    for talk in themes_list:
        self.public_talks_config['themes_list'][talk] = themes_list[talk]
    update_widgets(self)
    save_settings(self.public_talks_config)


def load_settings(settings_file=PATH_PRAT_USER_CONFIG_TALKS_FILE):
    talks_json = json.loads('{}')
    if os.path.isfile(settings_file):
        talks_json = json.load(open(settings_file))

    int_list = {}
    if 'themes_list' in talks_json:
        for talk in talks_json['themes_list']:
            int_list[int(talk)] = talks_json['themes_list'][talk]

    talks_json['themes_list'] = int_list

    return talks_json


def save_settings(settings_dict, settings_file=PATH_PRAT_USER_CONFIG_TALKS_FILE):
    open(settings_file, 'w').write(json.dumps(settings_dict, indent=4, ensure_ascii=False))


def public_talks_import_button_clicked(self):
    list_file = QFileDialog.getOpenFileName(parent=self, caption=_('Select a file with the list of public talks themes'), dir=PATH_REAL_HOME, filter='TXT (*.txt);;DOCX (*.docx)')
    themes_list = {}
    if os.path.isfile(list_file[0]):
        if list_file[0].rsplit('.', 1)[-1].lower() in ['txt']:
            with open(list_file[0], 'r') as themes_file:
                for line in themes_file:
                    number = get_number_from_string(line)
                    title = line.split(str(number), 1)[-1].strip().strip('-').strip(',').strip(')').strip('=').strip(':').strip()

                    if number and title:
                        themes_list[int(number)] = title
    if themes_list:
        add_talk_themes(self, themes_list=themes_list)