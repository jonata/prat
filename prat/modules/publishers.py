#!/usr/bin/env python3

import os
import datetime
import json

from PySide6.QtWidgets import QCheckBox, QLineEdit, QListWidget, QVBoxLayout, QFrame, QHBoxLayout, QPushButton, QSizePolicy, QWidget, QScrollArea, QLabel, QGridLayout
from PySide6.QtGui import QCursor, QIcon
from PySide6.QtCore import QSize, Qt

from prat.modules.paths import PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE, PATH_PRAT_USER_CONFIG_FOLDER, get_graphics_path
from prat.modules.translation import _
from prat.special_widgets import QLineEditDC, QLabelDC


MODULE_INFO = {
    'title': 'Publishers',
    'name': 'publishers',
    'date_enabled': False
}


def add_menu_widgets(self):
    self.modules_menu_button_publishers = QPushButton(self.modules_menu)
    self.modules_menu_button_publishers.setObjectName(u"modules_menu_button_publishers")
    self.modules_menu_button_publishers.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.modules_menu_button_publishers.sizePolicy().hasHeightForWidth())
    self.modules_menu_button_publishers.setSizePolicy(sizePolicy)
    self.modules_menu_button_publishers.setMinimumHeight(45)
    self.modules_menu_button_publishers.setCursor(QCursor(Qt.PointingHandCursor))
    self.modules_menu_button_publishers.setLayoutDirection(Qt.LeftToRight)
    self.modules_menu_button_publishers.setStyleSheet('background-image: url("' + get_graphics_path('publishers_icon.svg') + '");')
    self.modules_menu_button_publishers.clicked.connect(lambda: modules_menu_button_publishers_clicked(self))

    self.modules_menu_vl.addWidget(self.modules_menu_button_publishers)


def add_panel_widgets(self):
    self.rightButtons = QFrame(self.content_frame_topbar)
    self.rightButtons.setObjectName(u"rightButtons")
    self.rightButtons.setMinimumSize(QSize(0, 28))
    self.rightButtons.setFrameShape(QFrame.NoFrame)
    self.rightButtons.setFrameShadow(QFrame.Raised)

    self.horizontalLayout_2 = QHBoxLayout(self.rightButtons)
    self.horizontalLayout_2.setSpacing(5)
    self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
    self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)

    self.settingsTopBtn = QPushButton(self.rightButtons)
    self.settingsTopBtn.setObjectName(u"settingsTopBtn")
    self.settingsTopBtn.setMinimumSize(QSize(28, 28))
    self.settingsTopBtn.setMaximumSize(QSize(28, 28))
    self.settingsTopBtn.setCursor(QCursor(Qt.PointingHandCursor))

    icon1 = QIcon()
    icon1.addFile(get_graphics_path('publisher_icon.svg'), QSize(), QIcon.Normal, QIcon.Off)
    self.settingsTopBtn.setIcon(icon1)
    self.settingsTopBtn.setIconSize(QSize(20, 20))
    self.settingsTopBtn.clicked.connect(lambda: openCloseRightBox(self))

    self.horizontalLayout_2.addWidget(self.settingsTopBtn)

    self.content_frame_topbar_hbox.addWidget(self.rightButtons)

    self.extraRightBox = QFrame(self.content)
    self.extraRightBox.setObjectName(u"extraRightBox")
    self.extraRightBox.setMinimumSize(QSize(0, 0))
    self.extraRightBox.setMaximumSize(QSize(0, 16777215))
    self.extraRightBox.setFrameShape(QFrame.NoFrame)
    self.extraRightBox.setFrameShadow(QFrame.Raised)
    self.verticalLayout_7 = QVBoxLayout(self.extraRightBox)
    self.verticalLayout_7.setSpacing(0)
    self.verticalLayout_7.setObjectName(u"verticalLayout_7")
    self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
    self.themeSettingsTopDetail = QFrame(self.extraRightBox)
    self.themeSettingsTopDetail.setObjectName(u"themeSettingsTopDetail")
    self.themeSettingsTopDetail.setMaximumSize(QSize(16777215, 3))
    self.themeSettingsTopDetail.setFrameShape(QFrame.NoFrame)
    self.themeSettingsTopDetail.setFrameShadow(QFrame.Raised)

    self.verticalLayout_7.addWidget(self.themeSettingsTopDetail)

    self.contentSettings = QFrame(self.extraRightBox)
    self.contentSettings.setObjectName(u"contentSettings")
    self.contentSettings.setFrameShape(QFrame.NoFrame)
    self.contentSettings.setFrameShadow(QFrame.Raised)
    self.verticalLayout_13 = QVBoxLayout(self.contentSettings)
    self.verticalLayout_13.setSpacing(0)
    self.verticalLayout_13.setObjectName(u"verticalLayout_13")
    self.verticalLayout_13.setContentsMargins(0, 0, 0, 0)
    self.topeMenus = QFrame(self.contentSettings)
    self.topeMenus.setObjectName(u"topeMenus")
    self.topeMenus.setFrameShape(QFrame.NoFrame)
    self.topeMenus.setFrameShadow(QFrame.Raised)
    self.verticalLayout_14 = QVBoxLayout(self.topeMenus)
    self.verticalLayout_14.setSpacing(0)
    self.verticalLayout_14.setObjectName(u"verticalLayout_14")
    self.verticalLayout_14.setContentsMargins(0, 0, 0, 0)
    self.btn_message = QPushButton(self.topeMenus)
    self.btn_message.setObjectName(u"btn_message")
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.btn_message.sizePolicy().hasHeightForWidth())
    self.btn_message.setSizePolicy(sizePolicy)
    self.btn_message.setMinimumSize(QSize(0, 45))
    self.btn_message.setCursor(QCursor(Qt.PointingHandCursor))
    self.btn_message.setLayoutDirection(Qt.LeftToRight)
    # self.btn_message.setStyleSheet('background-image: url("images/icons/cil-envelope-open.png");')

    self.verticalLayout_14.addWidget(self.btn_message)

    self.btn_print = QPushButton(self.topeMenus)
    self.btn_print.setObjectName(u"btn_print")
    sizePolicy.setHeightForWidth(self.btn_print.sizePolicy().hasHeightForWidth())
    self.btn_print.setSizePolicy(sizePolicy)
    self.btn_print.setMinimumSize(QSize(0, 45))
    self.btn_print.setCursor(QCursor(Qt.PointingHandCursor))
    self.btn_print.setLayoutDirection(Qt.LeftToRight)
    # self.btn_print.setStyleSheet('background-image: url("images/icons/cil-print.png");')

    self.verticalLayout_14.addWidget(self.btn_print)

    self.btn_logout = QPushButton(self.topeMenus)
    self.btn_logout.setObjectName(u"btn_logout")
    sizePolicy.setHeightForWidth(self.btn_logout.sizePolicy().hasHeightForWidth())
    self.btn_logout.setSizePolicy(sizePolicy)
    self.btn_logout.setMinimumSize(QSize(0, 45))
    self.btn_logout.setCursor(QCursor(Qt.PointingHandCursor))
    self.btn_logout.setLayoutDirection(Qt.LeftToRight)
    # self.btn_logout.setStyleSheet('background-image: url("images/icons/cil-account-logout.png");')

    self.verticalLayout_14.addWidget(self.btn_logout)

    self.verticalLayout_13.addWidget(self.topeMenus, 0, Qt.AlignTop)

    self.verticalLayout_7.addWidget(self.contentSettings)

    self.horizontalLayout_4.addWidget(self.extraRightBox)


def add_widgets(self):
    self.publishers_page = QWidget()
    self.publishers_page.setObjectName(u"settings")

    self.publishers_hbox = QHBoxLayout(self.publishers_page)
    self.publishers_hbox.setObjectName(u"publishers_hbox")

    self.publishers_add_name_frame = QWidget(self.publishers_page)
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.publishers_add_name_frame.sizePolicy().hasHeightForWidth())
    self.publishers_add_name_frame.setSizePolicy(sizePolicy)

    self.publishers_add_name_frame_vbox = QVBoxLayout(self.publishers_add_name_frame)
    self.publishers_add_name_frame_vbox.setContentsMargins(0, 0, 0, 0)

    self.publishers_add_name_label = QLabel(self.publishers_add_name_frame)
    self.publishers_add_name_label.setProperty('class', 'small_label')

    self.publishers_add_name_frame_vbox.addWidget(self.publishers_add_name_label)

    self.publishers_add_name_line = QWidget(self.publishers_add_name_frame)
    # self.publishers_add_name_line.setObjectName('publishers_add_name_line')

    self.publishers_add_name_line_hbox = QHBoxLayout(self.publishers_add_name_line)
    self.publishers_add_name_line_hbox.setContentsMargins(0, 0, 0, 0)
    self.publishers_add_name_line_hbox.setSpacing(0)

    self.publishers_add_name_lineedit = QLineEdit(self.publishers_add_name_line)
    self.publishers_add_name_lineedit.setObjectName('publishers_add_name_lineedit')
    self.publishers_add_name_lineedit.textEdited.connect(lambda: publishers_add_name_lineedit_text_edited(self))
    self.publishers_add_name_lineedit.setMinimumWidth(300)

    self.publishers_add_name_line_hbox.addWidget(self.publishers_add_name_lineedit)

    self.publishers_add_name_confirm_button = QPushButton(self.publishers_add_name_line)
    self.publishers_add_name_confirm_button.setObjectName('publishers_add_name_confirm_button')
    self.publishers_add_name_confirm_button.setProperty('class', 'default_button')
    self.publishers_add_name_confirm_button.clicked.connect(lambda: publishers_add_name_confirm_button_clicked(self))
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(1)
    sizePolicy.setHeightForWidth(self.publishers_add_name_confirm_button.sizePolicy().hasHeightForWidth())
    self.publishers_add_name_confirm_button.setSizePolicy(sizePolicy)

    self.publishers_add_name_line_hbox.addWidget(self.publishers_add_name_confirm_button)

    self.publishers_add_name_cancel_button = QPushButton(self.publishers_add_name_line)
    self.publishers_add_name_cancel_button.setObjectName('publishers_add_name_cancel_button')
    self.publishers_add_name_cancel_button.setProperty('class', 'secondary_button')
    self.publishers_add_name_cancel_button.clicked.connect(lambda: publishers_add_name_cancel_button_clicked(self))
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(1)
    sizePolicy.setHeightForWidth(self.publishers_add_name_cancel_button.sizePolicy().hasHeightForWidth())
    self.publishers_add_name_cancel_button.setSizePolicy(sizePolicy)

    self.publishers_add_name_line_hbox.addWidget(self.publishers_add_name_cancel_button)

    self.publishers_add_name_frame_vbox.addWidget(self.publishers_add_name_line)

    self.publishers_add_name_frame.setVisible(False)

    self.publishers_hbox.addWidget(self.publishers_add_name_frame)

    self.publishers_list_frame = QWidget(self.publishers_page)
    self.publishers_list_frame.setObjectName('publishers_list_frame')
    self.publishers_list_frame.setMinimumWidth(300)
    self.publishers_list_frame.setMaximumWidth(300)

    self.publishers_list_vbox = QVBoxLayout(self.publishers_list_frame)
    self.publishers_list_vbox.setContentsMargins(0, 0, 0, 0)
    self.publishers_list_vbox.setSpacing(0)

    self.publishers_listview = QListWidget(self.publishers_list_frame)
    self.publishers_listview.setObjectName(u"publishers_listview")
    self.publishers_listview.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.publishers_listview.setSortingEnabled(True)
    self.publishers_listview.currentItemChanged.connect(lambda item: publishers_listview_itemactivated(self, item))
    # self.publishers_listview.itemClicked.connect(lambda item: publishers_listview_itemactivated(self, item))
    # self.publishers_listview.itemEntered.connect(lambda item: publishers_listview_itemactivated(self, item))

    self.publishers_list_vbox.addWidget(self.publishers_listview)

    self.publishers_list_addremove_frame = QWidget(self.publishers_list_frame)
    self.publishers_list_addremove_frame.setObjectName('publishers_list_addremove_frame')

    self.publishers_list_addremove_hbox = QHBoxLayout(self.publishers_list_addremove_frame)
    self.publishers_list_addremove_hbox.setContentsMargins(0, 0, 0, 0)
    self.publishers_list_addremove_hbox.setSpacing(0)

    self.publishers_list_add_publisher_button = QPushButton(self.publishers_list_addremove_frame)
    self.publishers_list_add_publisher_button.setObjectName('publishers_list_add_publisher_button')
    self.publishers_list_add_publisher_button.setProperty('class', 'secondary_button')
    self.publishers_list_add_publisher_button.clicked.connect(lambda: publishers_list_add_publisher_button_clicked(self))
    # icon = QIcon(get_graphics_path('publisher_icon.svg'))
    # self.publishers_list_add_publisher_button.setIcon(icon)

    self.publishers_list_addremove_hbox.addWidget(self.publishers_list_add_publisher_button)

    self.publishers_list_remove_publisher_button = QPushButton(self.publishers_list_addremove_frame)
    self.publishers_list_remove_publisher_button.setObjectName('publishers_list_remove_publisher_button')
    self.publishers_list_remove_publisher_button.setProperty('class', 'secondary_button')
    self.publishers_list_remove_publisher_button.clicked.connect(lambda: publishers_list_remove_publisher_button_clicked(self))

    self.publishers_list_addremove_hbox.addWidget(self.publishers_list_remove_publisher_button)

    self.publishers_list_vbox.addWidget(self.publishers_list_addremove_frame, 0, Qt.AlignBottom)

    self.publishers_list_frame.setVisible(False)

    self.publishers_hbox.addWidget(self.publishers_list_frame)

    self.publishers_scroll = QScrollArea(self.publishers_page)
    self.publishers_scroll.setObjectName(u"publishers_scroll")
    self.publishers_scroll.setWidgetResizable(True)
    self.publishers_scroll.setFrameShape(QFrame.NoFrame)
    self.publishers_scroll.setAlignment(Qt.AlignLeading | Qt.AlignLeft | Qt.AlignTop)

    self.publishers_scroll_widget = QWidget()
    self.publishers_scroll_widget.setObjectName(u"publishers_scroll_widget")
    sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.publishers_scroll_widget.sizePolicy().hasHeightForWidth())
    self.publishers_scroll_widget.setSizePolicy(sizePolicy)

    self.publishers_scroll_widget_vbox = QVBoxLayout(self.publishers_scroll_widget)
    self.publishers_scroll_widget_vbox.setObjectName(u"publishers_scroll_widget_vbox")

    self.publishers_scroll_widget_name = QLineEditDC(self.publishers_scroll_widget)
    self.publishers_scroll_widget_name.setObjectName('publishers_scroll_widget_name')
    self.publishers_scroll_widget_name.setReadOnly(True)
    self.publishers_scroll_widget_name.editingFinished.connect(lambda: publishers_scroll_widget_name_editingfinished(self))

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_name)

    self.publishers_scroll_widget_badges_line = QWidget(self.publishers_scroll_widget)

    self.publishers_scroll_widget_badges_line_hbox = QHBoxLayout(self.publishers_scroll_widget_badges_line)

    self.publishers_scroll_widget_badges_brothersister = QLabelDC(self.publishers_scroll_widget_badges_line)
    self.publishers_scroll_widget_badges_brothersister.setObjectName('publishers_scroll_widget_badges_brothersister')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.publishers_scroll_widget_badges_brothersister.sizePolicy().hasWidthForHeight())
    self.publishers_scroll_widget_badges_brothersister.setSizePolicy(sizePolicy)
    self.publishers_scroll_widget_badges_brothersister.doubleClicked.connect(lambda: publishers_scroll_widget_badges_brothersister_doubleclicked(self))

    self.publishers_scroll_widget_badges_line_hbox.addWidget(self.publishers_scroll_widget_badges_brothersister, 0, Qt.AlignLeft)

    self.publishers_scroll_widget_badges_baptized = QLabelDC(self.publishers_scroll_widget_badges_line)
    self.publishers_scroll_widget_badges_baptized.setObjectName('publishers_scroll_widget_badges_baptized')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.publishers_scroll_widget_badges_baptized.sizePolicy().hasWidthForHeight())
    self.publishers_scroll_widget_badges_baptized.setSizePolicy(sizePolicy)
    self.publishers_scroll_widget_badges_baptized.doubleClicked.connect(lambda: publishers_scroll_widget_badges_baptized_doubleclicked(self))

    self.publishers_scroll_widget_badges_line_hbox.addWidget(self.publishers_scroll_widget_badges_baptized, 0, Qt.AlignLeft)

    self.publishers_scroll_widget_badges_line_hbox.addStretch()

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_badges_line)

    self.publishers_scroll_widget_info_frame = QWidget(self.publishers_scroll_widget)
    self.publishers_scroll_widget_info_frame.setObjectName('publishers_scroll_widget_info_frame')

    self.publishers_scroll_widget_info_frame_vbox = QVBoxLayout(self.publishers_scroll_widget_info_frame)

    self.publishers_scroll_widget_info_frame_label = QLabel(self.congregation_midweek_meeting_frame)
    self.publishers_scroll_widget_info_frame_label.setObjectName(u"publishers_scroll_widget_info_frame_label")
    self.publishers_scroll_widget_info_frame_label.setProperty('class', 'big_label')

    self.publishers_scroll_widget_info_frame_vbox.addWidget(self.publishers_scroll_widget_info_frame_label)

    self.publishers_scroll_widget_info_dates_frame = QWidget(self.publishers_scroll_widget_info_frame)

    self.publishers_scroll_widget_info_dates_grid = QGridLayout(self.publishers_scroll_widget_info_dates_frame)

    self.publishers_scroll_widget_info_birthdate_label = QLabel(self.publishers_scroll_widget_info_dates_frame)
    self.publishers_scroll_widget_info_birthdate_label.setObjectName(u"publishers_scroll_widget_info_birthdate_label")
    self.publishers_scroll_widget_info_birthdate_label.setProperty('class', 'small_label')

    self.publishers_scroll_widget_info_dates_grid.addWidget(self.publishers_scroll_widget_info_birthdate_label, 0, 0, 1, 1)

    self.publishers_scroll_widget_info_birthdate = QLineEditDC(self.publishers_scroll_widget_info_dates_frame)
    self.publishers_scroll_widget_info_birthdate.setObjectName('publishers_scroll_widget_info_birthdate')
    self.publishers_scroll_widget_info_birthdate.setReadOnly(True)
    self.publishers_scroll_widget_info_birthdate.setMinimumWidth(100)
    self.publishers_scroll_widget_info_birthdate.editingFinished.connect(lambda: publishers_scroll_widget_info_birthdate_editingfinished(self))

    self.publishers_scroll_widget_info_dates_grid.addWidget(self.publishers_scroll_widget_info_birthdate, 1, 0, 1, 1)

    self.publishers_scroll_widget_info_baptism_label = QLabel(self.publishers_scroll_widget_info_dates_frame)
    self.publishers_scroll_widget_info_baptism_label.setObjectName(u"publishers_scroll_widget_info_baptism_label")
    self.publishers_scroll_widget_info_baptism_label.setProperty('class', 'small_label')

    self.publishers_scroll_widget_info_dates_grid.addWidget(self.publishers_scroll_widget_info_baptism_label, 0, 1, 1, 1)

    self.publishers_scroll_widget_info_baptism = QLineEditDC(self.publishers_scroll_widget_info_dates_frame)
    self.publishers_scroll_widget_info_baptism.setObjectName('publishers_scroll_widget_info_baptism')
    self.publishers_scroll_widget_info_baptism.setReadOnly(True)
    self.publishers_scroll_widget_info_baptism.setMinimumWidth(100)
    self.publishers_scroll_widget_info_baptism.editingFinished.connect(lambda: publishers_scroll_widget_info_baptism_editingfinished(self))

    self.publishers_scroll_widget_info_dates_grid.addWidget(self.publishers_scroll_widget_info_baptism, 1, 1, 1, 1)

    self.publishers_scroll_widget_info_dates_grid.setColumnStretch(2, 1)

    self.publishers_scroll_widget_info_frame_vbox.addWidget(self.publishers_scroll_widget_info_dates_frame)

    self.publishers_scroll_widget_info_contact_frame = QWidget(self.publishers_scroll_widget_info_frame)

    self.publishers_scroll_widget_info_contact_grid = QGridLayout(self.publishers_scroll_widget_info_contact_frame)

    self.publishers_scroll_widget_info_phone_label = QLabel(self.publishers_scroll_widget_info_contact_frame)
    self.publishers_scroll_widget_info_phone_label.setObjectName(u"publishers_scroll_widget_info_phone_label")
    self.publishers_scroll_widget_info_phone_label.setProperty('class', 'small_label')

    self.publishers_scroll_widget_info_contact_grid.addWidget(self.publishers_scroll_widget_info_phone_label, 0, 0, 1, 1)

    self.publishers_scroll_widget_info_phone = QLineEditDC(self.publishers_scroll_widget_info_contact_frame)
    self.publishers_scroll_widget_info_phone.setObjectName('publishers_scroll_widget_info_phone')
    self.publishers_scroll_widget_info_phone.setReadOnly(True)
    self.publishers_scroll_widget_info_phone.setMinimumWidth(180)
    self.publishers_scroll_widget_info_phone.editingFinished.connect(lambda: publishers_scroll_widget_info_phone_editingfinished(self))

    self.publishers_scroll_widget_info_contact_grid.addWidget(self.publishers_scroll_widget_info_phone, 1, 0, 1, 1)

    self.publishers_scroll_widget_info_email_label = QLabel(self.publishers_scroll_widget_info_contact_frame)
    self.publishers_scroll_widget_info_email_label.setObjectName(u"publishers_scroll_widget_info_email_label")
    self.publishers_scroll_widget_info_email_label.setProperty('class', 'small_label')

    self.publishers_scroll_widget_info_contact_grid.addWidget(self.publishers_scroll_widget_info_email_label, 0, 1, 1, 1)

    self.publishers_scroll_widget_info_email = QLineEditDC(self.publishers_scroll_widget_info_contact_frame)
    self.publishers_scroll_widget_info_email.setObjectName('publishers_scroll_widget_info_email')
    self.publishers_scroll_widget_info_email.setReadOnly(True)
    self.publishers_scroll_widget_info_email.setMinimumWidth(250)
    self.publishers_scroll_widget_info_email.editingFinished.connect(lambda: publishers_scroll_widget_info_email_editingfinished(self))

    self.publishers_scroll_widget_info_contact_grid.addWidget(self.publishers_scroll_widget_info_email, 1, 1, 1, 1)

    self.publishers_scroll_widget_info_contact_grid.setColumnStretch(2, 1)

    self.publishers_scroll_widget_info_frame_vbox.addWidget(self.publishers_scroll_widget_info_contact_frame)

    self.publishers_scroll_widget_info_address_frame = QWidget(self.publishers_scroll_widget_info_frame)

    self.publishers_scroll_widget_info_address_grid = QGridLayout(self.publishers_scroll_widget_info_address_frame)

    self.publishers_scroll_widget_info_address_label = QLabel(self.publishers_scroll_widget_info_address_frame)
    self.publishers_scroll_widget_info_address_label.setObjectName(u"publishers_scroll_widget_info_address_label")
    self.publishers_scroll_widget_info_address_label.setProperty('class', 'small_label')

    self.publishers_scroll_widget_info_address_grid.addWidget(self.publishers_scroll_widget_info_address_label, 0, 0, 1, 1)

    self.publishers_scroll_widget_info_address = QLineEditDC(self.publishers_scroll_widget_info_address_frame)
    self.publishers_scroll_widget_info_address.setObjectName('publishers_scroll_widget_info_address')
    self.publishers_scroll_widget_info_address.setReadOnly(True)
    self.publishers_scroll_widget_info_address.setMinimumWidth(400)
    self.publishers_scroll_widget_info_address.editingFinished.connect(lambda: publishers_scroll_widget_info_address_editingfinished(self))

    self.publishers_scroll_widget_info_address_grid.addWidget(self.publishers_scroll_widget_info_address, 1, 0, 1, 1)

    self.publishers_scroll_widget_info_address_grid.setColumnStretch(1, 1)

    self.publishers_scroll_widget_info_frame_vbox.addWidget(self.publishers_scroll_widget_info_address_frame)

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_info_frame)

    self.publishers_scroll_widget_leadership_frame = QWidget(self.publishers_scroll_widget)
    self.publishers_scroll_widget_leadership_frame.setObjectName('publishers_scroll_widget_leadership_frame')

    self.publishers_scroll_widget_leadership_frame_vbox = QVBoxLayout(self.publishers_scroll_widget_leadership_frame)

    self.publishers_scroll_widget_leadership_frame_label = QLabel(self.publishers_scroll_widget_leadership_frame)
    self.publishers_scroll_widget_leadership_frame_label.setObjectName(u"publishers_scroll_widget_leadership_frame_label")
    self.publishers_scroll_widget_leadership_frame_label.setProperty('class', 'big_label')

    self.publishers_scroll_widget_leadership_frame_vbox.addWidget(self.publishers_scroll_widget_leadership_frame_label)

    self.publishers_scroll_widget_leadership_frame_vbox.addSpacing(10)

    self.publishers_scroll_widget_leadership_frame_hframe = QWidget(self.publishers_scroll_widget)
    self.publishers_scroll_widget_leadership_frame_hframe.setObjectName('publishers_scroll_widget_leadership_frame_hframe')

    self.publishers_scroll_widget_leadership_frame_hbox = QHBoxLayout(self.publishers_scroll_widget_leadership_frame_hframe)

    self.publishers_scroll_widget_leadership_ministerial_servant = QCheckBox(self.publishers_scroll_widget_leadership_frame)
    self.publishers_scroll_widget_leadership_ministerial_servant.stateChanged.connect(lambda: publishers_scroll_widget_leadership_ministerial_servant_clicked(self))

    self.publishers_scroll_widget_leadership_frame_hbox.addWidget(self.publishers_scroll_widget_leadership_ministerial_servant, 0, Qt.AlignLeft)

    self.publishers_scroll_widget_leadership_elder = QCheckBox(self.publishers_scroll_widget_leadership_frame)
    self.publishers_scroll_widget_leadership_elder.stateChanged.connect(lambda: publishers_scroll_widget_leadership_elder_clicked(self))

    self.publishers_scroll_widget_leadership_frame_hbox.addWidget(self.publishers_scroll_widget_leadership_elder, 0, Qt.AlignLeft)

    self.publishers_scroll_widget_leadership_frame_hbox.addStretch()

    self.publishers_scroll_widget_leadership_frame_vbox.addWidget(self.publishers_scroll_widget_leadership_frame_hframe)

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_leadership_frame)

    self.publishers_scroll_widget_privileges_frame = QWidget(self.publishers_scroll_widget)
    self.publishers_scroll_widget_privileges_frame.setObjectName('publishers_scroll_widget_privileges_frame')
    self.publishers_scroll_widget_privileges_frame.setLayout(QVBoxLayout(self.publishers_scroll_widget_privileges_frame))

    self.publishers_scroll_widget_privileges_frame_label = QLabel(self.publishers_scroll_widget_privileges_frame)
    self.publishers_scroll_widget_privileges_frame_label.setObjectName(u"publishers_scroll_widget_privileges_frame_label")
    self.publishers_scroll_widget_privileges_frame_label.setProperty('class', 'big_label')

    self.publishers_scroll_widget_privileges_frame.layout().addWidget(self.publishers_scroll_widget_privileges_frame_label)

    self.publishers_scroll_widget_privileges_frame.layout().addSpacing(10)

    self.publishers_scroll_widget_privileges_meetings_prayer = QCheckBox()
    self.publishers_scroll_widget_privileges_meetings_prayer.stateChanged.connect(lambda: publishers_scroll_widget_privileges_meetings_prayer_clicked(self))
    self.publishers_scroll_widget_privileges_frame.layout().addWidget(self.publishers_scroll_widget_privileges_meetings_prayer)

    self.publishers_scroll_widget_privileges_attendant = QCheckBox()
    self.publishers_scroll_widget_privileges_attendant.stateChanged.connect(lambda: publishers_scroll_widget_privileges_attendant_clicked(self))
    self.publishers_scroll_widget_privileges_frame.layout().addWidget(self.publishers_scroll_widget_privileges_attendant)

    self.publishers_scroll_widget_privileges_media_operator = QCheckBox()
    self.publishers_scroll_widget_privileges_media_operator.stateChanged.connect(lambda: publishers_scroll_widget_privileges_media_operator_clicked(self))
    self.publishers_scroll_widget_privileges_frame.layout().addWidget(self.publishers_scroll_widget_privileges_media_operator)

    self.publishers_scroll_widget_privileges_audio_operator = QCheckBox()
    self.publishers_scroll_widget_privileges_audio_operator.stateChanged.connect(lambda: publishers_scroll_widget_privileges_audio_operator_clicked(self))
    self.publishers_scroll_widget_privileges_frame.layout().addWidget(self.publishers_scroll_widget_privileges_audio_operator)

    self.publishers_scroll_widget_privileges_roving_microphone = QCheckBox()
    self.publishers_scroll_widget_privileges_roving_microphone.stateChanged.connect(lambda: publishers_scroll_widget_privileges_roving_microphone_clicked(self))
    self.publishers_scroll_widget_privileges_frame.layout().addWidget(self.publishers_scroll_widget_privileges_roving_microphone)

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_privileges_frame)

    self.publishers_scroll_widget_midweekmeeting_frame = QWidget(self.publishers_scroll_widget)
    self.publishers_scroll_widget_midweekmeeting_frame.setObjectName('publishers_scroll_widget_midweekmeeting_frame')
    self.publishers_scroll_widget_midweekmeeting_frame.setLayout(QVBoxLayout(self.publishers_scroll_widget_midweekmeeting_frame))

    self.publishers_scroll_widget_midweekmeeting_frame_label = QLabel(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_midweekmeeting_frame_label.setObjectName(u"publishers_scroll_widget_midweekmeeting_frame_label")
    self.publishers_scroll_widget_midweekmeeting_frame_label.setProperty('class', 'big_label')

    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_midweekmeeting_frame_label)

    self.publishers_scroll_widget_midweekmeeting_frame.layout().addSpacing(10)

    self.publishers_scroll_widget_privileges_bible_reading = QCheckBox(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_privileges_bible_reading.stateChanged.connect(lambda: publishers_scroll_widget_privileges_bible_reading_clicked(self))

    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_bible_reading)

    self.publishers_scroll_widget_privileges_demonstrations = QCheckBox(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_privileges_demonstrations.stateChanged.connect(lambda: publishers_scroll_widget_privileges_demonstrations_clicked(self))

    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_demonstrations)

    self.publishers_scroll_widget_privileges_shorttalk = QCheckBox(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_privileges_shorttalk.stateChanged.connect(lambda: publishers_scroll_widget_privileges_shorttalk_clicked(self))
    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_shorttalk)

    self.publishers_scroll_widget_privileges_biblestudy_reader = QCheckBox(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_privileges_biblestudy_reader.stateChanged.connect(lambda: publishers_scroll_widget_privileges_biblestudy_reader_clicked(self))
    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_biblestudy_reader)

    self.publishers_scroll_widget_privileges_biblestudy_conductor = QCheckBox(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_privileges_biblestudy_conductor.stateChanged.connect(lambda: publishers_scroll_widget_privileges_biblestudy_conductor_clicked(self))
    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_biblestudy_conductor)

    self.publishers_scroll_widget_privileges_midweek_chairman = QCheckBox(self.publishers_scroll_widget_midweekmeeting_frame)
    self.publishers_scroll_widget_privileges_midweek_chairman.stateChanged.connect(lambda: publishers_scroll_widget_privileges_midweek_chairman_clicked(self))
    self.publishers_scroll_widget_midweekmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_midweek_chairman)

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_midweekmeeting_frame)

    self.publishers_scroll_widget_weekendmeeting_frame = QWidget(self.publishers_scroll_widget)
    self.publishers_scroll_widget_weekendmeeting_frame.setObjectName('publishers_scroll_widget_weekendmeeting_frame')
    self.publishers_scroll_widget_weekendmeeting_frame.setLayout(QVBoxLayout(self.publishers_scroll_widget_weekendmeeting_frame))

    self.publishers_scroll_widget_weekendmeeting_frame_label = QLabel(self.publishers_scroll_widget_weekendmeeting_frame)
    self.publishers_scroll_widget_weekendmeeting_frame_label.setObjectName(u"publishers_scroll_widget_weekendmeeting_frame_label")
    self.publishers_scroll_widget_weekendmeeting_frame_label.setProperty('class', 'big_label')

    self.publishers_scroll_widget_weekendmeeting_frame.layout().addWidget(self.publishers_scroll_widget_weekendmeeting_frame_label)

    self.publishers_scroll_widget_weekendmeeting_frame.layout().addSpacing(10)

    self.publishers_scroll_widget_privileges_watchtowerreader = QCheckBox()
    self.publishers_scroll_widget_privileges_watchtowerreader.stateChanged.connect(lambda: publishers_scroll_widget_privileges_watchtowerreader_clicked(self))
    self.publishers_scroll_widget_weekendmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_watchtowerreader)

    self.publishers_scroll_widget_privileges_weekend_chairman = QCheckBox()
    self.publishers_scroll_widget_privileges_weekend_chairman.stateChanged.connect(lambda: publishers_scroll_widget_privileges_weekend_chairman_clicked(self))
    self.publishers_scroll_widget_weekendmeeting_frame.layout().addWidget(self.publishers_scroll_widget_privileges_weekend_chairman)

    self.publishers_scroll_widget_vbox.addWidget(self.publishers_scroll_widget_weekendmeeting_frame)

    self.publishers_scroll.setWidget(self.publishers_scroll_widget)

    self.publishers_scroll.setVisible(False)

    self.publishers_hbox.addWidget(self.publishers_scroll)

    self.publishers_selectone_label = QLabel(self.publishers_page)
    self.publishers_selectone_label.setObjectName('publishers_selectone_label')

    self.publishers_selectone_label.setVisible(False)

    self.publishers_hbox.addWidget(self.publishers_selectone_label, 0, Qt.AlignCenter)

    self.stackedWidget.addWidget(self.publishers_page)


def openCloseRightBox(self):
    self.toggleRightBox(True)


def translate_widgets(self):
    self.modules_menu_button_publishers.setText(_(MODULE_INFO['title']))
    self.publishers_list_add_publisher_button.setText(_('Add'))
    self.publishers_list_remove_publisher_button.setText(_('Remove'))
    self.publishers_add_name_label.setText(_('Publisher name'))
    self.publishers_add_name_confirm_button.setText(_('Add'))
    self.publishers_add_name_cancel_button.setText(_('Cancel'))
    self.publishers_selectone_label.setText(_('Select a name to show information here'))
    self.publishers_scroll_widget_info_frame_label.setText(_('Information'))
    self.publishers_scroll_widget_info_birthdate_label.setText(_('Birth date'))
    self.publishers_scroll_widget_info_baptism_label.setText(_('Baptism date'))
    self.publishers_scroll_widget_info_phone_label.setText(_('Phone number'))
    self.publishers_scroll_widget_info_email_label.setText(_('email'))
    self.publishers_scroll_widget_info_address_label.setText(_('Address'))
    self.publishers_scroll_widget_privileges_frame_label.setText(_('Privileges'))
    self.publishers_scroll_widget_midweekmeeting_frame_label.setText(_('Midweek Meeting'))
    self.publishers_scroll_widget_weekendmeeting_frame_label.setText(_('Weekend Meeting'))
    self.publishers_scroll_widget_privileges_bible_reading.setText(_('Bible reading'))
    self.publishers_scroll_widget_privileges_demonstrations.setText(_('Demonstrations'))
    self.publishers_scroll_widget_privileges_shorttalk.setText(_('Short talks'))
    self.publishers_scroll_widget_privileges_watchtowerreader.setText(_('Watchtower Study reader'))
    self.publishers_scroll_widget_privileges_biblestudy_reader.setText(_('Bible Study reader'))
    self.publishers_scroll_widget_privileges_biblestudy_conductor.setText(_('Bible Study conductor'))
    self.publishers_scroll_widget_privileges_meetings_prayer.setText(_('Meetings prayer'))
    self.publishers_scroll_widget_privileges_midweek_chairman.setText(_('Midweek chairman'))
    self.publishers_scroll_widget_privileges_weekend_chairman.setText(_('Weekend chairman'))
    self.publishers_scroll_widget_privileges_attendant.setText(_('Attendant'))
    self.publishers_scroll_widget_privileges_audio_operator.setText(_('Audio operator'))
    self.publishers_scroll_widget_privileges_media_operator.setText(_('Media operator'))
    self.publishers_scroll_widget_privileges_roving_microphone.setText(_('Roving microphone'))
    self.publishers_scroll_widget_leadership_frame_label.setText(_('Leadership'))
    self.publishers_scroll_widget_leadership_ministerial_servant.setText(_('Ministerial servant'))
    self.publishers_scroll_widget_leadership_elder.setText(_('Elder'))


def modules_menu_button_publishers_clicked(self):
    self.selected_module = MODULE_INFO
    publishers_add_name_cancel_button_clicked(self)
    update_widgets(self)
    self.stackedWidget.setCurrentWidget(self.publishers_page)
    self.toggle_menu(True)
    self.resetStyle(self.modules_menu_button_publishers.objectName())
    self.modules_menu_button_publishers.setStyleSheet(self.selectMenu(self.modules_menu_button_publishers.styleSheet()))
    if not self.publishers_list:
        publishers_list_add_publisher_button_clicked(self)


def publishers_list_add_publisher_button_clicked(self):
    self.publishers_add_name_frame.setVisible(True)
    self.publishers_list_frame.setVisible(False)
    self.publishers_scroll.setVisible(False)
    self.publishers_selectone_label.setVisible(False)
    self.publishers_add_name_lineedit.setText('')
    self.publishers_add_name_lineedit.setFocus()
    publishers_add_name_lineedit_text_edited(self)


def publishers_list_remove_publisher_button_clicked(self):
    if self.publishers_listview.currentItem():
        del self.publishers_list[self.publishers_listview.currentItem().text()]
    update_widgets(self)


def publishers_add_name_lineedit_text_edited(self):
    self.publishers_add_name_confirm_button.setVisible(bool(self.publishers_add_name_lineedit.text()) and not self.publishers_add_name_lineedit.text() in self.publishers_list)


def publishers_add_name_confirm_button_clicked(self):
    self.publishers_list[self.publishers_add_name_lineedit.text()] = {}
    publishers_add_name_cancel_button_clicked(self)
    self.publishers_listview.setCurrentItem(self.publishers_listview.findItems(self.publishers_add_name_lineedit.text(), Qt.MatchExactly)[0])
    update_publisher_widgets(self)


def publishers_add_name_cancel_button_clicked(self):
    self.publishers_add_name_frame.setVisible(False)
    self.publishers_list_frame.setVisible(True)
    self.publishers_scroll.setVisible(True)
    update_widgets(self)


def publishers_listview_itemactivated(self, item):
    if item:
        self.publishers_list_remove_publisher_button.setEnabled(True)
    update_publisher_widgets(self)


def publishers_scroll_widget_name_editingfinished(self):
    update_publisher_widgets(self)
    self.publishers_scroll_widget_name.setStyleSheet(self.publishers_scroll_widget_name.styleSheet())
    # CHANGE NAME


def publishers_scroll_widget_badges_brothersister_doubleclicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['brother'] = not self.publishers_list[publisher_name].get('brother', True)
    update_publisher_widgets(self)


def publishers_scroll_widget_badges_baptized_doubleclicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['baptized'] = not self.publishers_list[publisher_name].get('baptized', False)
    update_publisher_widgets(self)


def publishers_scroll_widget_info_birthdate_editingfinished(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['birth_date'] = self.publishers_scroll_widget_info_birthdate.text()
    update_publisher_widgets(self)
    self.publishers_scroll_widget_info_birthdate.setStyleSheet(self.publishers_scroll_widget_info_birthdate.styleSheet())


def publishers_scroll_widget_info_baptism_editingfinished(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['baptism_date'] = self.publishers_scroll_widget_info_baptism.text()
    update_publisher_widgets(self)
    self.publishers_scroll_widget_info_baptism.setStyleSheet(self.publishers_scroll_widget_info_baptism.styleSheet())


def publishers_scroll_widget_info_phone_editingfinished(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['phone'] = self.publishers_scroll_widget_info_phone.text()
    update_publisher_widgets(self)
    self.publishers_scroll_widget_info_phone.setStyleSheet(self.publishers_scroll_widget_info_phone.styleSheet())


def publishers_scroll_widget_info_email_editingfinished(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['email'] = self.publishers_scroll_widget_info_email.text()
    update_publisher_widgets(self)
    self.publishers_scroll_widget_info_email.setStyleSheet(self.publishers_scroll_widget_info_email.styleSheet())


def publishers_scroll_widget_info_address_editingfinished(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['address'] = self.publishers_scroll_widget_info_address.text()
    update_publisher_widgets(self)
    self.publishers_scroll_widget_info_address.setStyleSheet(self.publishers_scroll_widget_info_address.styleSheet())


def publishers_scroll_widget_privileges_bible_reading_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['bible_reading'] = self.publishers_scroll_widget_privileges_bible_reading.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_demonstrations_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['demonstrations'] = self.publishers_scroll_widget_privileges_demonstrations.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_shorttalk_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['short_talk'] = self.publishers_scroll_widget_privileges_shorttalk.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_watchtowerreader_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['watchtower_reader'] = self.publishers_scroll_widget_privileges_watchtowerreader.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_biblestudy_reader_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['bible_study_reader'] = self.publishers_scroll_widget_privileges_biblestudy_reader.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_biblestudy_conductor_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['bible_study_conductor'] = self.publishers_scroll_widget_privileges_biblestudy_conductor.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_meetings_prayer_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['meetings_prayer'] = self.publishers_scroll_widget_privileges_meetings_prayer.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_midweek_chairman_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['midweek_chairman'] = self.publishers_scroll_widget_privileges_midweek_chairman.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_weekend_chairman_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['weekend_chairman'] = self.publishers_scroll_widget_privileges_weekend_chairman.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_attendant_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['attendant'] = self.publishers_scroll_widget_privileges_attendant.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_media_operator_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['media_operator'] = self.publishers_scroll_widget_privileges_media_operator.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_audio_operator_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['audio_operator'] = self.publishers_scroll_widget_privileges_audio_operator.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_privileges_roving_microphone_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['roving_microphone'] = self.publishers_scroll_widget_privileges_roving_microphone.isChecked()
    update_publisher_widgets(self)


def publishers_scroll_widget_leadership_ministerial_servant_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['ministerial_servant'] = self.publishers_scroll_widget_leadership_ministerial_servant.isChecked()
        if self.publishers_list[publisher_name].get('elder', False):
            self.publishers_list[publisher_name]['elder'] = False
    update_publisher_widgets(self)


def publishers_scroll_widget_leadership_elder_clicked(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_list[publisher_name]['elder'] = self.publishers_scroll_widget_leadership_elder.isChecked()
        if self.publishers_list[publisher_name].get('ministerial_servant', False):
            self.publishers_list[publisher_name]['ministerial_servant'] = False
    update_publisher_widgets(self)


def update_publisher_widgets(self):
    if self.publishers_listview.currentItem():
        publisher_name = self.publishers_listview.currentItem().text()
        self.publishers_scroll_widget_name.setText(publisher_name)
        self.publishers_scroll_widget_name.setReadOnly(True)

        self.publishers_scroll_widget_info_birthdate.setText(self.publishers_list[publisher_name].get('birth_date', ''))
        self.publishers_scroll_widget_info_birthdate.setReadOnly(True)

        self.publishers_scroll_widget_info_baptism.setVisible(self.publishers_list[publisher_name].get('baptized', False))
        self.publishers_scroll_widget_info_baptism_label.setVisible(self.publishers_list[publisher_name].get('baptized', False))
        self.publishers_scroll_widget_info_baptism.setText(self.publishers_list[publisher_name].get('baptism_date', ''))
        self.publishers_scroll_widget_info_baptism.setReadOnly(True)

        self.publishers_scroll_widget_info_phone.setText(self.publishers_list[publisher_name].get('phone', ''))
        self.publishers_scroll_widget_info_phone.setReadOnly(True)

        self.publishers_scroll_widget_info_email.setText(self.publishers_list[publisher_name].get('email', ''))
        self.publishers_scroll_widget_info_email.setReadOnly(True)

        self.publishers_scroll_widget_info_address.setText(self.publishers_list[publisher_name].get('address', ''))
        self.publishers_scroll_widget_info_address.setReadOnly(True)

        self.publishers_scroll_widget_leadership_frame.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_leadership_elder.setChecked(self.publishers_list[publisher_name].get('elder', False))
        self.publishers_scroll_widget_leadership_ministerial_servant.setChecked(self.publishers_list[publisher_name].get('ministerial_servant', False))

        self.publishers_scroll_widget_privileges_bible_reading.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_bible_reading.setChecked(self.publishers_list[publisher_name].get('bible_reading', False))

        self.publishers_scroll_widget_privileges_demonstrations.setChecked(self.publishers_list[publisher_name].get('demonstrations', False))

        self.publishers_scroll_widget_privileges_shorttalk.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_shorttalk.setChecked(self.publishers_list[publisher_name].get('short_talk', False))

        # self.publishers_scroll_widget_privileges_watchtowerreader.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_watchtowerreader.setChecked(self.publishers_list[publisher_name].get('watchtower_reader', False))

        self.publishers_scroll_widget_privileges_biblestudy_reader.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_biblestudy_reader.setChecked(self.publishers_list[publisher_name].get('bible_study_reader', False))

        self.publishers_scroll_widget_privileges_biblestudy_conductor.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_biblestudy_conductor.setChecked(self.publishers_list[publisher_name].get('bible_study_conductor', False))

        self.publishers_scroll_widget_privileges_frame.setVisible(self.publishers_list[publisher_name].get('brother', True))
        # self.publishers_scroll_widget_privileges_meetings_prayer.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_meetings_prayer.setChecked(self.publishers_list[publisher_name].get('meetings_prayer', False))

        self.publishers_scroll_widget_privileges_midweek_chairman.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_midweek_chairman.setChecked(self.publishers_list[publisher_name].get('midweek_chairman', False))

        self.publishers_scroll_widget_weekendmeeting_frame.setVisible(self.publishers_list[publisher_name].get('brother', True))
        # self.publishers_scroll_widget_privileges_weekend_chairman.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_weekend_chairman.setChecked(self.publishers_list[publisher_name].get('weekend_chairman', False))

        # self.publishers_scroll_widget_privileges_attendant.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_attendant.setChecked(self.publishers_list[publisher_name].get('attendant', False))

        # self.publishers_scroll_widget_privileges_media_operator.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_media_operator.setChecked(self.publishers_list[publisher_name].get('media_operator', False))

        # self.publishers_scroll_widget_privileges_audio_operator.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_audio_operator.setChecked(self.publishers_list[publisher_name].get('audio_operator', False))

        # self.publishers_scroll_widget_privileges_roving_microphone.setVisible(self.publishers_list[publisher_name].get('brother', True))
        self.publishers_scroll_widget_privileges_roving_microphone.setChecked(self.publishers_list[publisher_name].get('roving_microphone', False))

        # self.publishers_scroll_widget_privileges_roving_microphone.setVisible(self.publishers_list[publisher_name].get('brother', True))
        # self.publishers_scroll_widget_privileges_roving_microphone.setChecked(self.publishers_list[publisher_name].get('roving_microphone', False))

        self.publishers_scroll_widget_badges_brothersister.setProperty('class', 'label_brother' if self.publishers_list[publisher_name].get('brother', True) else 'label_sister')
        self.publishers_scroll_widget_badges_brothersister.setText(_('Brother') if self.publishers_list[publisher_name].get('brother', True) else _('Sister'))
        self.publishers_scroll_widget_badges_brothersister.setStyleSheet(self.publishers_scroll_widget_badges_brothersister.styleSheet())

        self.publishers_scroll_widget_badges_baptized.setProperty('class', 'label_baptized' if self.publishers_list[publisher_name].get('baptized', False) else 'label_unbaptized')
        self.publishers_scroll_widget_badges_baptized.setText(_('Baptized') if self.publishers_list[publisher_name].get('baptized', False) else _('Unbaptized'))
        self.publishers_scroll_widget_badges_baptized.setStyleSheet(self.publishers_scroll_widget_badges_baptized.styleSheet())

    self.publishers_scroll.setVisible(bool(self.publishers_listview.currentItem()))
    self.publishers_selectone_label.setVisible(not bool(self.publishers_listview.currentItem()))


def update_widgets(self):
    self.publishers_listview.clear()
    self.publishers_listview.addItems(list(self.publishers_list))
    self.publishers_list_remove_publisher_button.setEnabled(bool(self.publishers_listview.currentItem()))

    update_publisher_widgets(self)


def save_list(publishers_list, publishers_file=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
    open(publishers_file, 'w').write(json.dumps(publishers_list, indent=4, ensure_ascii=False))


def load_list(publishers_file_path=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
    publishers_json = json.loads('{}')
    if os.path.isfile(PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
        publishers_json = json.load(open(PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE))

    # LEGACY
    publishers_file = ''
    if os.path.isfile(os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'publishers.xml')):  # os.path.join(self.path_prat_data, 'publishers.xml')
        publishers_file += open(os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'publishers.xml'), mode='r', encoding='utf-8').read()  # os.path.join(self.path_prat_data, 'publishers.xml')

        # final_list = {}

        for publisher in publishers_file.split('<publisher '):
            name = ''
            is_brother = True
            is_pioneer = False
            is_elder = False
            is_ministerial_servant = False
            is_ms_student = False
            is_ms_student_reading = True
            is_ms_student_firstvisit = True
            is_ms_student_firstvisitassistant = True
            is_ms_student_returnvisit = True
            is_ms_student_returnvisitassistant = True
            is_ms_student_biblestudy = True
            is_ms_student_biblestudyassistant = True

            baptized = False
            clm_chairman = False
            attendant = False
            is_biblestudy_reader = False
            watchtower_reader = False
            media_operator = False
            audio_operator = False
            meetings_prayer = False
            weekend_chairman = False
            group = False
            activities = {
                datetime.datetime.now().year - 6: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year - 5: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year - 4: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year - 3: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year - 2: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year - 1: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year: ['', None, None, None, None, None, None, None, None, None, None, None, None],
                datetime.datetime.now().year + 1: ['', None, None, None, None, None, None, None, None, None, None, None, None]
            }
            ministry_school_studies = {
                1: ['', False, ''],
                2: ['', False, ''],
                3: ['', False, ''],
                4: ['', False, ''],
                5: ['', False, ''],
                6: ['', False, ''],
                7: ['', False, ''],
                8: ['', False, ''],
                9: ['', False, ''],
                10: ['', False, ''],
                11: ['', False, ''],
                12: ['', False, ''],
                13: ['', False, ''],
                14: ['', False, ''],
                15: ['', False, ''],
                16: ['', False, ''],
                17: ['', False, ''],
                18: ['', False, ''],
                19: ['', False, ''],
                20: ['', False, ''],
                21: ['', False, ''],
                22: ['', False, ''],
                23: ['', False, ''],
                24: ['', False, ''],
                25: ['', False, ''],
                26: ['', False, ''],
                27: ['', False, ''],
                28: ['', False, ''],
                29: ['', False, ''],
                30: ['', False, ''],
                31: ['', False, ''],
                32: ['', False, ''],
                33: ['', False, ''],
                34: ['', False, ''],
                35: ['', False, ''],
                36: ['', False, ''],
                37: ['', False, ''],
                38: ['', False, ''],
                39: ['', False, ''],
                40: ['', False, ''],
                41: ['', False, ''],
                42: ['', False, ''],
                43: ['', False, ''],
                44: ['', False, ''],
                45: ['', False, ''],
                46: ['', False, ''],
                47: ['', False, ''],
                48: ['', False, ''],
                49: ['', False, ''],
                50: ['', False, ''],
                51: ['', False, ''],
                52: ['', False, ''],
                53: ['', False, '']
            }
            outlines = []
            publisher_list = {}

            if 'name="' in publisher.split('>')[0]:
                name = publisher.split('name="')[1].split('"')[0]

            if 'is_brother="' in publisher.split('>')[0]:
                if publisher.split('is_brother="')[1].split('"')[0] == 'False':
                    is_brother = False
            publisher_list['brother'] = is_brother

            if 'is_elder="' in publisher.split('>')[0]:
                if publisher.split('is_elder="')[1].split('"')[0] == 'True':
                    is_elder = True
            publisher_list['elder'] = is_elder

            if 'is_ministerial_servant="' in publisher.split('>')[0]:
                if publisher.split('is_ministerial_servant="')[1].split('"')[0] == 'True':
                    is_ministerial_servant = True
            publisher_list['ministerial_servant'] = is_ministerial_servant

            if 'is_ms_student="' in publisher.split('>')[0]:
                if publisher.split('is_ms_student="')[1].split('"')[0] == 'True':
                    is_ms_student = True
            publisher_list['is_ms_student'] = is_ms_student

            if 'is_ms_student_reading' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_reading')[1].split('"')[0] == 'False':
                    is_ms_student_reading = False
            publisher_list['is_ms_student_reading'] = is_ms_student_reading

            if 'is_ms_student_firstvisit' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_firstvisit')[1].split('"')[0] == 'False':
                    is_ms_student_firstvisit = False
            publisher_list['is_ms_student_firstvisit'] = is_ms_student_firstvisit

            if 'is_ms_student_firstvisitassistant' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_firstvisitassistant')[1].split('"')[0] == 'False':
                    is_ms_student_firstvisitassistant = False
            publisher_list['is_ms_student_firstvisitassistant'] = is_ms_student_firstvisitassistant

            if 'is_ms_student_returnvisit' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_returnvisit')[1].split('"')[0] == 'False':
                    is_ms_student_returnvisit = False
            publisher_list['is_ms_student_returnvisit'] = is_ms_student_returnvisit

            if 'is_ms_student_returnvisitassistant' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_returnvisitassistant')[1].split('"')[0] == 'False':
                    is_ms_student_returnvisitassistant = False
            publisher_list['is_ms_student_returnvisitassistant'] = is_ms_student_returnvisitassistant

            if 'is_ms_student_biblestudy' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_biblestudy')[1].split('"')[0] == 'False':
                    is_ms_student_biblestudy = False
            publisher_list['is_ms_student_biblestudy'] = is_ms_student_biblestudy

            if 'is_ms_student_biblestudyassistant' in publisher.split('>')[0]:
                if publisher.split('is_ms_student_biblestudyassistant')[1].split('"')[0] == 'False':
                    is_ms_student_biblestudyassistant = False
            publisher_list['is_ms_student_biblestudyassistant'] = is_ms_student_biblestudyassistant

            if 'baptized="' in publisher.split('>')[0]:
                if publisher.split('baptized="')[1].split('"')[0] == 'True':
                    baptized = True
            publisher_list['baptized'] = baptized

            if 'is_pioneer="' in publisher.split('>')[0]:
                if publisher.split('is_pioneer="')[1].split('"')[0] == 'True':
                    is_pioneer = True
            publisher_list['is_pioneer'] = is_pioneer

            if 'clm_chairman="' in publisher.split('>')[0]:
                if publisher.split('clm_chairman="')[1].split('"')[0] == 'True':
                    clm_chairman = True
            publisher_list['clm_chairman'] = clm_chairman

            if 'is_biblestudy_reader="' in publisher.split('>')[0]:
                if publisher.split('is_biblestudy_reader="')[1].split('"')[0] == 'True':
                    is_biblestudy_reader = True
            publisher_list['is_biblestudy_reader'] = is_biblestudy_reader

            if 'watchtower_reader="' in publisher.split('>')[0]:
                if publisher.split('watchtower_reader="')[1].split('"')[0] == 'True':
                    watchtower_reader = True
            publisher_list['watchtower_reader'] = watchtower_reader

            if 'attendant="' in publisher.split('>')[0]:
                if publisher.split('attendant="')[1].split('"')[0] == 'True':
                    attendant = True
            publisher_list['attendant'] = attendant

            if 'media_operator="' in publisher.split('>')[0]:
                if publisher.split('media_operator="')[1].split('"')[0] == 'True':
                    media_operator = True
            publisher_list['media_operator'] = media_operator

            if 'audio_operator="' in publisher.split('>')[0]:
                if publisher.split('audio_operator="')[1].split('"')[0] == 'True':
                    audio_operator = True
            publisher_list['audio_operator'] = audio_operator

            if 'meetings_prayer="' in publisher.split('>')[0]:
                if publisher.split('meetings_prayer="')[1].split('"')[0] == 'True':
                    meetings_prayer = True
            publisher_list['meetings_prayer'] = meetings_prayer

            if 'weekend_chairman="' in publisher.split('>')[0]:
                if publisher.split('weekend_chairman="')[1].split('"')[0] == 'True':
                    weekend_chairman = True
            publisher_list['weekend_chairman'] = weekend_chairman

            if 'group="' in publisher.split('>')[0]:
                if not publisher.split('group="')[1].split('"')[0] == 'False':
                    group = publisher.split('group="')[1].split('"')[0]
            publisher_list['group'] = group

            if 'address="' in publisher.split('>')[0]:
                publisher_list['address'] = publisher.split('address="')[1].split('"')[0]

            if 'email="' in publisher.split('>')[0]:
                publisher_list['email'] = publisher.split('email="')[1].split('"')[0]

            if '<activities' in publisher:
                for report in publisher.split('<activities')[1].split('<report '):
                    if '</report>' in report:
                        year = 0
                        if report.split('year="')[1].split('"')[0].isnumeric():
                            year = int(report.split('year="')[1].split('"')[0])
                        month = 0
                        if report.split('month="')[1].split('"')[0].isnumeric():
                            month = int(report.split('month="')[1].split('"')[0])
                        placements = 0
                        if report.split('placements="')[1].split('"')[0].isnumeric():
                            placements = int(report.split('placements="')[1].split('"')[0])
                        videos = 0
                        if report.split('videos="')[1].split('"')[0].isnumeric():
                            videos = int(report.split('videos="')[1].split('"')[0])
                        hours = 0
                        if report.split('hours="')[1].split('"')[0].isnumeric():
                            hours = int(report.split('hours="')[1].split('"')[0])
                        return_visits = 0
                        if report.split('return_visits="')[1].split('"')[0].isnumeric():
                            return_visits = int(report.split('return_visits="')[1].split('"')[0])
                        bible_studies = 0
                        if report.split('bible_studies="')[1].split('"')[0].isnumeric():
                            bible_studies = int(report.split('bible_studies="')[1].split('"')[0])
                        comments = report.split('comments="')[1].split('"')[0]
                        activities[year][month] = {
                            'placements': placements,
                            'videos': videos,
                            'hours': hours,
                            'return_visits': return_visits,
                            'bible_studies': bible_studies,
                            'comments': comments
                        }
            publisher_list['activities'] = activities

            if '<ministry_school_studies' in publisher:
                for study in publisher.split('<ministry_school_studies')[1].split('<study '):
                    if '</study>' in study:
                        number = int(study.split('number="')[1].split('"')[0])
                        ministry_school_studies[number][0] = study.split('assigned="')[1].split('"')[0]

                        if study.split('exercises="')[1].split('"')[0] == 'True':
                            ministry_school_studies[number][1] = True

                        ministry_school_studies[number][2] = study.split('completed="')[1].split('"')[0]

            publisher_list['ministry_school_studies'] = ministry_school_studies

            if '<outlines' in publisher:
                for outline in publisher.split('<outlines')[1].split('<outline '):
                    if '</outline>' in outline:
                        outlines.append(int(outline.split('number="')[1].split('"')[0]))

            publisher_list['outlines'] = outlines

            if not name == '':
                publishers_json[name] = publisher_list

        os.remove(os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'publishers.xml'))

    return publishers_json


def new_publisher(publishers_list, name, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
    publishers_list[name] = {'brother': True}
    update_publishers_file(publishers_list, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE)


def change_publisher_name(publishers_list, old_name, new_name, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
    old_name_dict = publishers_list[old_name]
    publishers_list[new_name] = old_name_dict
    remove_publisher(publishers_list, old_name, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE)
    update_publishers_file(publishers_list, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE)


def remove_publisher(publishers_list, publisher_name, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
    del publishers_list[publisher_name]
    update_publishers_file(publishers_list, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE)


def update_publishers_file(publishers_list, filepath=PATH_PRAT_USER_CONFIG_PUBLISHERS_FILE):
    if os.path.isfile(filepath):
        os.remove(filepath)
    publishers_file = '<?xml version="1.0" encoding="UTF-8"?>'
    for publisher in publishers_list.keys():
        publishers_file += '<publisher '
        publishers_file += 'name="' + publisher + '" '
        publishers_activities = '<activities>'
        publishers_ministry_school_studies = '<ministry_school_studies>'
        outlines = '<outlines>'

        for registry in publishers_list[publisher].keys():
            if registry == 'activities':
                for year in publishers_list[publisher]['activities'].keys():
                    for month in range(13):
                        if not month == 0 and not publishers_list[publisher]['activities'][year][month] is None:
                            placements = ''
                            if 'placements' in publishers_list[publisher]['activities'][year][month].keys():
                                placements = publishers_list[publisher]['activities'][year][month]['placements']

                            videos = ''
                            if 'videos' in publishers_list[publisher]['activities'][year][month].keys():
                                videos = publishers_list[publisher]['activities'][year][month]['videos']

                            hours = ''
                            if 'hours' in publishers_list[publisher]['activities'][year][month].keys():
                                hours = publishers_list[publisher]['activities'][year][month]['hours']

                            return_visits = ''
                            if 'return_visits' in publishers_list[publisher]['activities'][year][month].keys():
                                return_visits = publishers_list[publisher]['activities'][year][month]['return_visits']

                            bible_studies = ''
                            if 'bible_studies' in publishers_list[publisher]['activities'][year][month].keys():
                                bible_studies = publishers_list[publisher]['activities'][year][month]['bible_studies']

                            comments = ''
                            if 'comments' in publishers_list[publisher]['activities'][year][month].keys():
                                comments = publishers_list[publisher]['activities'][year][month]['comments']

                            publishers_activities += '<report year="' + str(year) + '" month="' + str(month) + '"'
                            publishers_activities += ' placements="' + str(placements) + '"'
                            publishers_activities += ' videos="' + str(videos) + '"'
                            publishers_activities += ' hours="' + str(hours) + '"'
                            publishers_activities += ' return_visits="' + str(return_visits) + '"'
                            publishers_activities += ' bible_studies="' + str(bible_studies) + '"'
                            publishers_activities += ' comments="' + comments + '">'
                            publishers_activities += '</report>'
            elif registry == 'ministry_school_studies':
                for number in publishers_list[publisher]['ministry_school_studies'].keys():
                    publishers_ministry_school_studies += '<study number="' + str(number) + '"'
                    publishers_ministry_school_studies += ' assigned="' + publishers_list[publisher]['ministry_school_studies'][number][0] + '"'
                    publishers_ministry_school_studies += ' exercises="' + str(publishers_list[publisher]['ministry_school_studies'][number][1]) + '"'
                    publishers_ministry_school_studies += ' completed="' + publishers_list[publisher]['ministry_school_studies'][number][2] + '">'
                    publishers_ministry_school_studies += '</study>'
            elif registry == 'outlines':
                for outline in publishers_list[publisher]['outlines']:
                    outlines += '<outline number="' + str(outline) + '">'
                    outlines += '</outline>'
            else:
                publishers_file += registry + '="' + str(publishers_list[publisher][registry]) + '" '

        publishers_file += '>'
        if not publishers_activities == '<activities>':
            publishers_file += publishers_activities + '</activities>'
        if not publishers_ministry_school_studies == '<ministry_school_studies>':
            publishers_file += publishers_ministry_school_studies + '</ministry_school_studies>'
        if not outlines == '<outlines>':
            publishers_file += outlines + '</outlines>'
        publishers_file += '</publisher>'

    publishers_file = open(filepath, mode='w', encoding='utf-8').write(publishers_file)


def get_publishers_names(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_clm_chairman(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and ((publishers_list[publisher].get('elder', False)) or (publishers_list[publisher].get('ministerial_servant', False))) and publishers_list[publisher].get('midweek_chairman', False):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_biblestudy_readers(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and 'is_biblestudy_reader' in publishers_list[publisher].keys() and publishers_list[publisher]['is_biblestudy_reader']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_watchtowerstudy_readers(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and 'watchtower_reader' in publishers_list[publisher].keys() and publishers_list[publisher]['watchtower_reader']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_attendants(publishers_list, suggestions=False, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and publishers_list[publisher].get('attendant', False):
            final_list.append(publisher)

    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_media_operators(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'media_operator' in publishers_list[publisher].keys() and publishers_list[publisher]['media_operator']:
            final_list.append(publisher)

    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_audio_operators(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'audio_operator' in publishers_list[publisher].keys() and publishers_list[publisher]['audio_operator']:
            final_list.append(publisher)

    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_roving_microphones(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'roving_microphone' in publishers_list[publisher].keys() and publishers_list[publisher]['roving_microphone']:
            final_list.append(publisher)

    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_elders(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and publishers_list[publisher].get('elder', False):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministerial_servants(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and publishers_list[publisher].get('ministerial_servant', False):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_leadership(publishers_list, sort=False):
    final_list = get_publishers_elders(publishers_list) + get_publishers_ministerial_servants(publishers_list)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_bible_study_conductors(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and 'bible_study_conductor' in publishers_list[publisher].keys() and publishers_list[publisher]['bible_study_conductor']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_bible_study_readers(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and 'bible_study_reader' in publishers_list[publisher].keys() and publishers_list[publisher]['bible_study_reader']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_pioneers(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('is_pioneer', False):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_meetings_prayer(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and 'meetings_prayer' in publishers_list[publisher].keys() and publishers_list[publisher]['meetings_prayer']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_weekend_meeting_chairmans(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and 'weekend_chairman' in publishers_list[publisher].keys() and publishers_list[publisher]['weekend_chairman']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_baptized_brothers(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized'] and publishers_list[publisher].get('brother', True):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_reading(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'is_ms_student_reading' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_reading']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_tfgw_bible_reading(publishers_list, sort=False):
    '''PLACEHOLDER'''
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True) and 'bible_reading' in publishers_list[publisher].keys() and publishers_list[publisher]['bible_reading']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ayfm_demonstrations(publishers_list, sort=False):
    '''PLACEHOLDER'''
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('demonstrations', False):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ayfm_talks(publishers_list, sort=False):
    '''PLACEHOLDER'''
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('short_talk', False) and publishers_list[publisher].get('brother', True):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_firstvisit(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'is_ms_student_firstvisit' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_firstvisit']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_firstvisitassistant(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'is_ms_student_firstvisitassistant' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_firstvisitassistant']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_returnvisit(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'is_ms_student_returnvisit' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_returnvisit']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_returnvisitassistant(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'is_ms_student_returnvisitassistant' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_returnvisitassistant']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_biblestudy(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'is_ms_student_biblestudy' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_biblestudy']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_ministry_school_biblestudyassistant(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'is_ms_student_biblestudyassistant' in publishers_list[publisher].keys() and publishers_list[publisher]['is_ms_student_biblestudyassistant']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_brothers(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if publishers_list[publisher].get('brother', True):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_baptized(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'baptized' in publishers_list[publisher].keys() and publishers_list[publisher]['baptized']:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_by_group(publishers_list, group, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'group' in publishers_list[publisher].keys() and publishers_list[publisher]['group'] and publishers_list[publisher]['group'] == group:
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list


def get_publishers_without_group(publishers_list, sort=False):
    final_list = []
    for publisher in publishers_list.keys():
        if 'group' not in publishers_list[publisher].keys() or ('group' in publishers_list[publisher].keys() and not publishers_list[publisher]['group']):
            final_list.append(publisher)
    if sort:
        final_list = sorted(final_list)
    return final_list
