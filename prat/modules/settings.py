#!/usr/bin/env python3

import os
import json

from PySide6.QtWidgets import QCheckBox, QWidget, QVBoxLayout, QPushButton, QSizePolicy, QScrollArea, QFrame, QHBoxLayout, QLabel, QFileDialog
from PySide6.QtGui import QCursor
from PySide6.QtCore import Qt

from prat.modules.translation import _, get_available_language_names, set_language
from prat.modules import welcome_screen
from prat.modules.paths import PATH_PRAT_USER_CONFIG_FILE, PATH_PRAT_USER_CONFIG_FOLDER, PATH_REAL_HOME, get_graphics_path
from prat.special_widgets import QComboBoxNoWheel

MODULE_INFO = {
    'title': _('Settings'),
    'name': 'settings',
    'date_enabled': False
}


def add_menu_widgets(self):
    self.modules_menu_button_settings = QPushButton(self.modules_menu)
    self.modules_menu_button_settings.setObjectName(u"modules_menu_button_settings")
    self.modules_menu_button_settings.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.modules_menu_button_settings.sizePolicy().hasHeightForWidth())
    self.modules_menu_button_settings.setSizePolicy(sizePolicy)
    self.modules_menu_button_settings.setMinimumHeight(45)
    self.modules_menu_button_settings.setCursor(QCursor(Qt.PointingHandCursor))
    self.modules_menu_button_settings.setLayoutDirection(Qt.LeftToRight)
    self.modules_menu_button_settings.setStyleSheet('background-image: url("' + get_graphics_path('settings_icon.svg') + '");')
    self.modules_menu_button_settings.clicked.connect(lambda: modules_menu_button_settings_clicked(self))

    self.modules_menu_vl.addWidget(self.modules_menu_button_settings)


def add_widgets(self):
    self.settings_page = QWidget()
    self.settings_page.setObjectName(u"settings")

    self.settings_vbox = QVBoxLayout(self.settings_page)
    self.settings_vbox.setObjectName(u"settings_vbox")

    self.settings_scroll = QScrollArea(self.settings_page)
    self.settings_scroll.setObjectName(u"settings_scroll")
    self.settings_scroll.setWidgetResizable(True)
    self.settings_scroll.setFrameShape(QFrame.NoFrame)
    self.settings_scroll.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

    self.settings_scroll_widget = QWidget()
    self.settings_scroll_widget.setObjectName(u"settings_scroll_widget")

    sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
    sizePolicy3.setHorizontalStretch(0)
    sizePolicy3.setVerticalStretch(0)
    sizePolicy3.setHeightForWidth(self.settings_scroll_widget.sizePolicy().hasHeightForWidth())
    self.settings_scroll_widget.setSizePolicy(sizePolicy3)

    self.settings_scroll_widget_vbox = QVBoxLayout(self.settings_scroll_widget)
    self.settings_scroll_widget_vbox.setObjectName(u"settings_scroll_widget_vbox")

    self.settings_use_friendly_date = QCheckBox(self.settings_scroll_widget)
    self.settings_use_friendly_date.stateChanged.connect(lambda: settings_use_friendly_date_clicked(self))

    self.settings_scroll_widget_vbox.addWidget(self.settings_use_friendly_date)

    self.settings_use_weekof = QCheckBox(self.settings_scroll_widget)
    self.settings_use_weekof.stateChanged.connect(lambda: settings_use_weekof_clicked(self))

    self.settings_scroll_widget_vbox.addWidget(self.settings_use_weekof)

    self.settings_delivered_documents_folder_line = QFrame(self.settings_scroll_widget)
    self.settings_delivered_documents_folder_line.setObjectName('settings_delivered_documents_folder_line')

    self.settings_delivered_documents_folder_vbox = QVBoxLayout(self.settings_delivered_documents_folder_line)
    self.settings_delivered_documents_folder_vbox.setContentsMargins(0, 10, 0, 10)

    self.settings_delivered_documents_folder_label = QLabel(self.settings_delivered_documents_folder_line)
    self.settings_delivered_documents_folder_label.setObjectName(u"settings_delivered_documents_folder_label")
    self.settings_delivered_documents_folder_label.setProperty('class', 'small_label')

    self.settings_delivered_documents_folder_vbox.addWidget(self.settings_delivered_documents_folder_label)

    self.settings_delivered_documents_folder_path_line = QFrame(self.settings_scroll_widget)
    self.settings_delivered_documents_folder_path_line.setObjectName('settings_delivered_documents_folder_path_line')

    self.settings_delivered_documents_folder_path_hbox = QHBoxLayout(self.settings_delivered_documents_folder_path_line)
    self.settings_delivered_documents_folder_path_hbox.setContentsMargins(0, 0, 0, 0)
    self.settings_delivered_documents_folder_path_hbox.setSpacing(0)

    self.settings_delivered_documents_folder_path_label = QLabel(self.settings_delivered_documents_folder_path_line)
    self.settings_delivered_documents_folder_path_label.setObjectName('settings_delivered_documents_folder_path_label')
    # self.settings_delivered_documents_folder_path_label.setProperty('class', 'small_label')
    # sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    # sizePolicy.setHorizontalStretch(0)
    # sizePolicy.setVerticalStretch(0)
    # sizePolicy.setHeightForWidth(self.settings_delivered_documents_folder_path_label.sizePolicy().hasHeightForWidth())
    # self.settings_delivered_documents_folder_path_label.setSizePolicy(sizePolicy)

    self.settings_delivered_documents_folder_path_hbox.addWidget(self.settings_delivered_documents_folder_path_label, 0, Qt.AlignLeft)

    self.settings_delivered_documents_folder_change_button = QPushButton(self.settings_delivered_documents_folder_path_line)
    self.settings_delivered_documents_folder_change_button.setObjectName('settings_delivered_documents_folder_change_button')
    self.settings_delivered_documents_folder_change_button.setProperty('class', 'secondary_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.settings_delivered_documents_folder_change_button.sizePolicy().hasHeightForWidth())
    self.settings_delivered_documents_folder_change_button.setSizePolicy(sizePolicy)
    self.settings_delivered_documents_folder_change_button.clicked.connect(lambda: settings_delivered_documents_folder_change_button_clicked(self))

    self.settings_delivered_documents_folder_path_hbox.addWidget(self.settings_delivered_documents_folder_change_button, 0, Qt.AlignLeft)

    self.settings_delivered_documents_folder_path_hbox.addStretch()

    self.settings_delivered_documents_folder_vbox.addWidget(self.settings_delivered_documents_folder_path_line)

    # self.settings_delivered_documents_folder_name = QComboBoxNoWheel(self.settings_delivered_documents_folder_line)
    # self.settings_delivered_documents_folder_name.setEditable(True)
    # # self.settings_delivered_documents_folder_name.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # # self.settings_delivered_documents_folder_name.editTextChanged.connect(self.settings_delivered_documents_folder_name.style().polish(self.settings_delivered_documents_folder_name))
    # self.settings_delivered_documents_folder_name.setFocusPolicy(Qt.StrongFocus)
    # self.settings_delivered_documents_folder_name.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    # self.settings_delivered_documents_folder_name.setObjectName(u"chairman_name")

    # self.settings_delivered_documents_folder_grid.addWidget(self.settings_delivered_documents_folder_name, 1, 0, 1, 1)

    self.settings_scroll_widget_vbox.addWidget(self.settings_delivered_documents_folder_line)

    self.settings_language_label = QLabel(self.settings_delivered_documents_folder_line)
    self.settings_language_label.setObjectName(u"settings_language_label")
    self.settings_language_label.setProperty('class', 'small_label')

    self.settings_scroll_widget_vbox.addWidget(self.settings_language_label)

    self.settings_language_combobox = QComboBoxNoWheel(self.welcome_screen)
    self.settings_language_combobox.setFocusPolicy(Qt.StrongFocus)
    self.settings_language_combobox.activated.connect(lambda: apply_language(self))
    self.settings_language_combobox.setObjectName(u"settings_language_combobox")
    self.settings_language_combobox.setMinimumWidth(300)

    self.settings_scroll_widget_vbox.addWidget(self.settings_language_combobox, 0, Qt.AlignLeft)





    # self.settings_update_button = QPushButton(self.settings_scroll_widget)
    # self.settings_update_button.setObjectName('settings_update_button')
    # self.settings_update_button.setProperty('class', 'default_button')
    # self.settings_update_button.clicked.connect(lambda: settings_update_button_clicked(self))

    # self.settings_scroll_widget_vbox.addWidget(self.settings_update_button)

    self.settings_scroll.setWidget(self.settings_scroll_widget)

    self.settings_vbox.addWidget(self.settings_scroll)

    self.stackedWidget.addWidget(self.settings_page)


def modules_menu_button_settings_clicked(self):
    self.selected_module = MODULE_INFO
    update_widgets(self)
    self.stackedWidget.setCurrentWidget(self.settings_page)
    self.toggle_menu(True)
    self.resetStyle(self.modules_menu_button_settings.objectName())
    self.modules_menu_button_settings.setStyleSheet(self.selectMenu(self.modules_menu_button_settings.styleSheet()))


def translate_widgets(self):
    self.modules_menu_button_settings.setText(_(MODULE_INFO['title']))
    self.settings_delivered_documents_folder_label.setText(_('Delivered documents folder').upper())
    self.settings_use_friendly_date.setText(_('Use friendly date'))
    self.settings_use_weekof.setText(_('Use "Week of"'))
    self.settings_delivered_documents_folder_change_button.setText(_('Change'))
    self.settings_language_label.setText(_('Language').upper())

# def settings_update_button_clicked(self):
#     indices.update_from_epub(os.path.join(PATH_REAL_HOME, 'Download'))
#     self.index_w = indices.get_w_index()
#     self.index_clm = indices.get_clm_index()


def load_settings(settings_file=PATH_PRAT_USER_CONFIG_FILE):
    settings_json = json.loads('{}')
    if os.path.isfile(settings_file):
        settings_json = json.load(open(settings_file))


    # LEGACY - remove on future versions

    # settings = {}
    # settings_json['congregation_name'] = ''

    if os.path.isfile(os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'config')):
        config_file = open(os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'config'), mode='r', encoding='utf-8').read()
        for line in config_file.split('\n'):
            if '=' in line:
                if not line.split('=', 1)[-1] == '':
                    if line.split('=', 1)[-1] == 'True':
                        settings_json[line.split('=', 1)[0]] = True
                    elif line.split('=', 1)[-1] == 'False':
                        settings_json[line.split('=', 1)[0]] = False
                    elif line.split('=', 1)[-1].isnumeric():
                        settings_json[line.split('=', 1)[0]] = int(line.split('=', 1)[-1])
                    else:
                        settings_json[line.split('=', 1)[0]] = line.split('=', 1)[-1]
        os.remove(os.path.join(PATH_PRAT_USER_CONFIG_FOLDER, 'config'))
    # if not 'path_for_documents' in [*settings_json] or ('path_for_documents' in [*settings_json] and not os.access(settings_json['path_for_documents'], os.W_OK)):
    #     home = PATH_REAL_HOME
    #     settings_json['path_for_documents'] = home
    # if ('path_for_updates' in settings_json.keys() and not os.path.isdir(settings_json['path_for_updates'])) or (not 'path_for_updates' in settings_json.keys()):
        # settings_json['path_for_updates'] = self.real_path_home

    return settings_json


def save_settings(settings_dict, settings_file=PATH_PRAT_USER_CONFIG_FILE):
    open(settings_file, 'w').write(json.dumps(settings_dict, indent=4, ensure_ascii=False))


def update_widgets(self):
    final_language_list = []
    lang_dict = get_available_language_names()
    for lang in lang_dict:
        final_language_list.append(lang_dict[lang])
    self.settings_language_combobox.clear()
    self.settings_language_combobox.addItems(final_language_list)
    self.settings_language_combobox.setCurrentText(lang_dict[self.selected_language])

    self.settings_use_friendly_date.setChecked(self.settings.get('use_pretty_date', True))
    self.settings_use_weekof.setChecked(self.settings.get('use_week_of', False))
    self.settings_delivered_documents_folder_path_label.setText(os.path.join(self.settings.get('path_for_documents', PATH_REAL_HOME)))


def settings_use_friendly_date_clicked(self):
    self.settings['use_pretty_date'] = self.settings_use_friendly_date.isChecked()


def settings_use_weekof_clicked(self):
    self.settings['use_week_of'] = self.settings_use_weekof.isChecked()


def settings_delivered_documents_folder_change_button_clicked(self):
    selected_directory = QFileDialog.getExistingDirectory()
    if os.path.isdir(selected_directory):
        self.settings['path_for_documents'] = selected_directory
    update_widgets(self)


def apply_language(self):
    lang_dict = get_available_language_names()
    for lang in lang_dict:
        if lang_dict[lang] == self.settings_language_combobox.currentText():
            self.selected_language = lang
            break
    self.settings['language'] = self.selected_language
    set_language(self.selected_language)
    self.translate_widgets()
