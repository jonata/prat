#!/usr/bin/env python3

from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QFrame, QPushButton, QSizePolicy, QLabel
from PySide6.QtGui import QCursor, QIcon
from PySide6.QtCore import Qt, QSize

import os
from datetime import datetime, timedelta

from prat.modules.date import get_week_start, pretty_date
from prat.modules.translation import _
from prat.modules.paths import get_graphics_path, PATH_PRAT_USER_CONFIG_FILE
from prat.modules.publishers import get_publishers_elders, get_publishers_ministerial_servants, get_publishers_names, get_publishers_pioneers
from prat.modules.indices import get_last_update_date, update_widgets
from prat.modules.midweek_meeting import load_assignments as midweek_load_assignments
from prat.modules.weekend_meeting import load_assignments as weekend_load_assignments
from prat.modules.welcome_screen import update_welcome_screen


MODULE_INFO = {
                'title': 'Prat',
                'name':'start_screen',
                'date_enabled': False
              }


def add_menu_widgets(self):
    self.main_menu_toggle_frame = QFrame(self.main_menu_frame)
    self.main_menu_toggle_frame.setObjectName(u"main_menu_toggle_frame")
    self.main_menu_toggle_frame.setMaximumHeight(45)
    self.main_menu_toggle_frame.setFrameShape(QFrame.NoFrame)
    self.main_menu_toggle_frame.setFrameShadow(QFrame.Raised)

    self.main_menu_toggle_frame_vl = QVBoxLayout(self.main_menu_toggle_frame)
    self.main_menu_toggle_frame_vl.setSpacing(0)
    self.main_menu_toggle_frame_vl.setObjectName(u"main_menu_toggle_frame_vl")
    self.main_menu_toggle_frame_vl.setContentsMargins(0, 0, 0, 0)

    self.main_menu_toggle_button = QPushButton(self.main_menu_toggle_frame)
    self.main_menu_toggle_button.setObjectName(u"main_menu_toggle_button")
    self.main_menu_toggle_button.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.main_menu_toggle_button.sizePolicy().hasHeightForWidth())
    self.main_menu_toggle_button.setSizePolicy(sizePolicy)
    self.main_menu_toggle_button.setMinimumHeight(45)
    self.main_menu_toggle_button.setCursor(QCursor(Qt.PointingHandCursor))
    self.main_menu_toggle_button.setLayoutDirection(Qt.LeftToRight)
    self.main_menu_toggle_button.setStyleSheet('background-image: url("' + get_graphics_path('main_menu_icon.svg') + '");')
    self.main_menu_toggle_button.clicked.connect(lambda: main_menu_toggle_button_clicked(self))

    self.main_menu_toggle_frame_vl.addWidget(self.main_menu_toggle_button)

    self.main_menu_vlayout.addWidget(self.main_menu_toggle_frame)


def add_widgets(self):
    self.start_screen = QWidget()
    self.start_screen.setObjectName(u"start_screen")

    self.start_screen_vbox = QVBoxLayout(self.start_screen)
    self.start_screen_vbox.setObjectName(u"start_screen_vbox")

    self.start_screen_top_frame = QWidget(self.start_screen)
    self.start_screen_top_frame.setObjectName('start_screen_top_frame')

    self.start_screen_top_frame_hbox = QHBoxLayout(self.start_screen_top_frame)
    self.start_screen_top_frame_hbox.setObjectName(u"start_screen_top_frame_hbox")

    self.start_screen_top_frame_congregation_frame = QFrame(self.start_screen_top_frame)

    self.start_screen_top_frame_congregation_frame_vbox = QVBoxLayout(self.start_screen_top_frame_congregation_frame)
    self.start_screen_top_frame_congregation_frame_vbox.setObjectName(u"start_screen_top_frame_congregation_frame_vbox")

    self.start_screen_top_frame_congregation_name_label = QLabel(self.start_screen_top_frame_congregation_frame)
    self.start_screen_top_frame_congregation_name_label.setObjectName('start_screen_top_frame_congregation_name_label')

    self.start_screen_top_frame_congregation_frame_vbox.addWidget(self.start_screen_top_frame_congregation_name_label)

    self.start_screen_top_frame_congregation_name = QLabel(self.start_screen_top_frame_congregation_frame)
    self.start_screen_top_frame_congregation_name.setObjectName('start_screen_top_frame_congregation_name')

    self.start_screen_top_frame_congregation_frame_vbox.addWidget(self.start_screen_top_frame_congregation_name)

    self.start_screen_top_frame_congregation_name_circuit = QLabel(self.start_screen_top_frame_congregation_frame)
    self.start_screen_top_frame_congregation_name_circuit.setObjectName('start_screen_top_frame_congregation_name_circuit')

    self.start_screen_top_frame_congregation_frame_vbox.addWidget(self.start_screen_top_frame_congregation_name_circuit)

    self.start_screen_top_frame_hbox.addWidget(self.start_screen_top_frame_congregation_frame)

    self.start_screen_top_frame_hbox.addStretch()


    self.start_screen_top_frame_publishers_frame = QFrame(self.start_screen_top_frame)

    self.start_screen_top_frame_publishers_frame_vbox = QVBoxLayout(self.start_screen_top_frame_publishers_frame)
    self.start_screen_top_frame_publishers_frame_vbox.setObjectName(u"start_screen_top_frame_publishers_frame_vbox")

    self.start_screen_top_frame_publishers_label = QLabel(self.start_screen_top_frame_publishers_frame)
    self.start_screen_top_frame_publishers_label.setObjectName('start_screen_top_frame_publishers_label')
    self.start_screen_top_frame_publishers_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_publishers_frame_vbox.addWidget(self.start_screen_top_frame_publishers_label)

    self.start_screen_top_frame_publishers = QLabel(self.start_screen_top_frame_publishers_frame)
    self.start_screen_top_frame_publishers.setObjectName('start_screen_top_frame_publishers')
    self.start_screen_top_frame_publishers.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_publishers_frame_vbox.addWidget(self.start_screen_top_frame_publishers)


    self.start_screen_top_frame_hbox.addWidget(self.start_screen_top_frame_publishers_frame)


    self.start_screen_top_frame_pioneers_frame = QFrame(self.start_screen_top_frame)

    self.start_screen_top_frame_pioneers_frame_vbox = QVBoxLayout(self.start_screen_top_frame_pioneers_frame)
    self.start_screen_top_frame_pioneers_frame_vbox.setObjectName(u"start_screen_top_frame_pioneers_frame_vbox")

    self.start_screen_top_frame_pioneers_label = QLabel(self.start_screen_top_frame_pioneers_frame)
    self.start_screen_top_frame_pioneers_label.setObjectName('start_screen_top_frame_pioneers_label')
    self.start_screen_top_frame_pioneers_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_pioneers_frame_vbox.addWidget(self.start_screen_top_frame_pioneers_label)

    self.start_screen_top_frame_pioneers = QLabel(self.start_screen_top_frame_pioneers_frame)
    self.start_screen_top_frame_pioneers.setObjectName('start_screen_top_frame_pioneers')
    self.start_screen_top_frame_pioneers.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_pioneers_frame_vbox.addWidget(self.start_screen_top_frame_pioneers)


    self.start_screen_top_frame_hbox.addWidget(self.start_screen_top_frame_pioneers_frame)

    self.start_screen_top_frame_elders_frame = QFrame(self.start_screen_top_frame)

    self.start_screen_top_frame_elders_frame_vbox = QVBoxLayout(self.start_screen_top_frame_elders_frame)
    self.start_screen_top_frame_elders_frame_vbox.setObjectName(u"start_screen_top_frame_elders_frame_vbox")

    self.start_screen_top_frame_elders_label = QLabel(self.start_screen_top_frame_elders_frame)
    self.start_screen_top_frame_elders_label.setObjectName('start_screen_top_frame_elders_label')
    self.start_screen_top_frame_elders_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_elders_frame_vbox.addWidget(self.start_screen_top_frame_elders_label)

    self.start_screen_top_frame_elders = QLabel(self.start_screen_top_frame_elders_frame)
    self.start_screen_top_frame_elders.setObjectName('start_screen_top_frame_elders')
    self.start_screen_top_frame_elders.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_elders_frame_vbox.addWidget(self.start_screen_top_frame_elders)


    self.start_screen_top_frame_hbox.addWidget(self.start_screen_top_frame_elders_frame)

    self.start_screen_top_frame_ministerialservants_frame = QFrame(self.start_screen_top_frame)

    self.start_screen_top_frame_ministerialservants_frame_vbox = QVBoxLayout(self.start_screen_top_frame_ministerialservants_frame)
    self.start_screen_top_frame_ministerialservants_frame_vbox.setObjectName(u"start_screen_top_frame_ministerialservants_frame_vbox")

    self.start_screen_top_frame_ministerialservants_label = QLabel(self.start_screen_top_frame_ministerialservants_frame)
    self.start_screen_top_frame_ministerialservants_label.setObjectName('start_screen_top_frame_ministerialservants_label')
    self.start_screen_top_frame_ministerialservants_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_ministerialservants_frame_vbox.addWidget(self.start_screen_top_frame_ministerialservants_label)

    self.start_screen_top_frame_ministerialservants = QLabel(self.start_screen_top_frame_ministerialservants_frame)
    self.start_screen_top_frame_ministerialservants.setObjectName('start_screen_top_frame_ministerialservants')
    self.start_screen_top_frame_ministerialservants.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_top_frame_ministerialservants_frame_vbox.addWidget(self.start_screen_top_frame_ministerialservants)

    self.start_screen_top_frame_hbox.addWidget(self.start_screen_top_frame_ministerialservants_frame)

    self.start_screen_vbox.addWidget(self.start_screen_top_frame)

    self.start_screen_middle_frame = QWidget(self.start_screen)
    self.start_screen_middle_frame.setObjectName('start_screen_middle_frame')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Expanding)
    # sizePolicy.setHorizontalStretch(0)
    # sizePolicy.setVerticalStretch(0)
    # sizePolicy.setHeightForWidth(self.start_screen_middle_frame.sizePolicy().hasHeightForWidth())
    self.start_screen_middle_frame.setSizePolicy(sizePolicy)

    self.start_screen_middle_frame_hbox = QHBoxLayout(self.start_screen_middle_frame)
    self.start_screen_middle_frame_hbox.setObjectName(u"start_screen_middle_frame_hbox")



    self.start_screen_middle_frame_next_meeting_frame = QWidget(self.start_screen_top_frame)
    self.start_screen_middle_frame_next_meeting_frame.setObjectName('start_screen_middle_frame_next_meeting_frame')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    # sizePolicy.setHorizontalStretch(0)
    # sizePolicy.setVerticalStretch(0)
    # sizePolicy.setHeightForWidth(self.start_screen_middle_frame.sizePolicy().hasHeightForWidth())
    self.start_screen_middle_frame_next_meeting_frame.setSizePolicy(sizePolicy)
    # self.start_screen_middle_frame_next_meeting_frame.setStyleSheet('background-color: gray; border-radius: 8px; padding: 20px;')

    self.start_screen_middle_frame_next_meeting_frame_vbox = QVBoxLayout(self.start_screen_middle_frame_next_meeting_frame)
    self.start_screen_middle_frame_next_meeting_frame_vbox.setObjectName(u"start_screen_middle_frame_next_meeting_frame_vbox")
    self.start_screen_middle_frame_next_meeting_frame_vbox.setSpacing(2)
    self.start_screen_middle_frame_next_meeting_frame_vbox.setContentsMargins(12,12,12,12)

    self.start_screen_middle_frame_next_meeting_label = QLabel(self.start_screen_middle_frame_next_meeting_frame)
    self.start_screen_middle_frame_next_meeting_label.setObjectName('start_screen_middle_frame_next_meeting_label')
    # self.start_screen_middle_frame_next_meeting_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_middle_frame_next_meeting_frame_vbox.addWidget(self.start_screen_middle_frame_next_meeting_label)

    self.start_screen_middle_frame_next_meeting_remaining_time = QLabel('Amanhã', self.start_screen_middle_frame_next_meeting_frame)
    self.start_screen_middle_frame_next_meeting_remaining_time.setObjectName('start_screen_middle_frame_next_meeting_remaining_time')
    # self.start_screen_middle_frame_next_meeting_remaining_time.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_middle_frame_next_meeting_frame_vbox.addWidget(self.start_screen_middle_frame_next_meeting_remaining_time)

    self.start_screen_middle_frame_next_meeting_description = QLabel(self.start_screen_middle_frame_next_meeting_frame)
    self.start_screen_middle_frame_next_meeting_description.setObjectName('start_screen_middle_frame_next_meeting_description')
    # self.start_screen_middle_frame_next_meeting_description.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_middle_frame_next_meeting_frame_vbox.addWidget(self.start_screen_middle_frame_next_meeting_description)

    self.start_screen_middle_frame_next_meeting_frame_vbox.addSpacing(6)

    self.start_screen_middle_frame_next_meeting_assignments = QLabel(self.start_screen_middle_frame_next_meeting_frame)
    self.start_screen_middle_frame_next_meeting_assignments.setObjectName('start_screen_middle_frame_next_meeting_assignments')
    # self.start_screen_middle_frame_next_meeting_assignments.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_middle_frame_next_meeting_frame_vbox.addWidget(self.start_screen_middle_frame_next_meeting_assignments)

    self.start_screen_middle_frame_hbox.addWidget(self.start_screen_middle_frame_next_meeting_frame, 0, Qt.AlignTop)

    self.start_screen_vbox.addWidget(self.start_screen_middle_frame)

    # self.start_screen_vbox.addStretch()

    self.start_screen_bottom_frame = QWidget(self.start_screen)
    self.start_screen_bottom_frame.setObjectName('start_screen_bottom_frame')

    self.start_screen_bottom_frame_hbox = QHBoxLayout(self.start_screen_bottom_frame)
    self.start_screen_bottom_frame_hbox.setObjectName(u"start_screen_bottom_frame_hbox")

    self.start_screen_bottom_frame_hbox.addStretch()

    self.start_screen_bottom_frame_update_frame = QFrame(self.start_screen_bottom_frame)

    self.start_screen_bottom_frame_update_frame_hbox = QHBoxLayout(self.start_screen_bottom_frame_update_frame)
    self.start_screen_bottom_frame_update_frame_hbox.setObjectName(u"start_screen_bottom_frame_update_frame_vbox")

    self.start_screen_bottom_frame_update_frame_vbox = QVBoxLayout()
    self.start_screen_bottom_frame_update_frame_vbox.setObjectName(u"start_screen_bottom_frame_update_frame_vbox")
    self.start_screen_bottom_frame_update_frame_vbox.setSpacing(0)

    self.start_screen_bottom_frame_update_label = QLabel(self.start_screen_bottom_frame_update_frame)
    self.start_screen_bottom_frame_update_label.setObjectName('start_screen_bottom_frame_update_label')
    self.start_screen_bottom_frame_update_label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_bottom_frame_update_frame_vbox.addWidget(self.start_screen_bottom_frame_update_label)

    self.start_screen_bottom_frame_update = QLabel(self.start_screen_bottom_frame_update_frame)
    self.start_screen_bottom_frame_update.setObjectName('start_screen_bottom_frame_update')
    self.start_screen_bottom_frame_update.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

    self.start_screen_bottom_frame_update_frame_vbox.addWidget(self.start_screen_bottom_frame_update)

    self.start_screen_bottom_frame_update_button = QPushButton(self.start_screen_bottom_frame_update_frame)
    self.start_screen_bottom_frame_update_button.setObjectName('start_screen_bottom_frame_update_button')
    self.start_screen_bottom_frame_update_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.start_screen_bottom_frame_update_button.sizePolicy().hasHeightForWidth())
    self.start_screen_bottom_frame_update_button.setSizePolicy(sizePolicy)
    self.start_screen_bottom_frame_update_button.clicked.connect(lambda: start_screen_bottom_frame_update_button_clicked(self))

    self.start_screen_bottom_frame_update_frame_vbox.addWidget(self.start_screen_bottom_frame_update_button, 0, Qt.AlignRight)

    self.start_screen_bottom_frame_update_frame_hbox.addLayout(self.start_screen_bottom_frame_update_frame_vbox)


    self.start_screen_bottom_frame_update_smbutton = QPushButton(self.start_screen_bottom_frame_update_frame)
    self.start_screen_bottom_frame_update_smbutton.setObjectName('start_screen_bottom_frame_update_smbutton')
    self.start_screen_bottom_frame_update_smbutton.setProperty('class', 'secondary_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.start_screen_bottom_frame_update_smbutton.sizePolicy().hasWidthForHeight())
    self.start_screen_bottom_frame_update_smbutton.setSizePolicy(sizePolicy)
    icon = QIcon()
    icon.addFile(get_graphics_path('update_icon.svg'), QSize(), QIcon.Normal, QIcon.Off)
    self.start_screen_bottom_frame_update_smbutton.setIcon(icon)
    self.start_screen_bottom_frame_update_smbutton.clicked.connect(lambda: start_screen_bottom_frame_update_button_clicked(self))

    self.start_screen_bottom_frame_update_frame_hbox.addWidget(self.start_screen_bottom_frame_update_smbutton, 0, Qt.AlignRight)

    self.start_screen_bottom_frame_hbox.addWidget(self.start_screen_bottom_frame_update_frame)

    self.start_screen_vbox.addWidget(self.start_screen_bottom_frame)

    self.stackedWidget.addWidget(self.start_screen)


def main_menu_toggle_button_clicked(self):
    self.selected_module = MODULE_INFO
    update_dashboard(self)
    if (not self.settings.get('congregation_name', '') or not self.settings.get('language', '') or not self.settings or not self.publishers_list): # Transfer to a funciton on welcome_screen module
        self.stackedWidget.setCurrentWidget(self.welcome_screen)
        update_welcome_screen(self)
    else:
        self.stackedWidget.setCurrentWidget(self.start_screen)

    # self.stackedWidget.setCurrentWidget(self.welcome_screen) #REMOVE ME
    # update_welcome_screen(self) #REMOVE ME

    self.toggle_menu(True)
    self.resetStyle(self.main_menu_toggle_button.objectName())
    self.main_menu_toggle_button.setStyleSheet(self.selectMenu(self.main_menu_toggle_button.styleSheet()))


def translate_widgets(self):
    self.main_menu_toggle_button.setText(MODULE_INFO['title'])
    self.start_screen_top_frame_congregation_name_label.setText(_('Congregation') + ':')
    self.start_screen_top_frame_publishers_label.setText(_('Publishers'))
    self.start_screen_top_frame_pioneers_label.setText(_('Pioneers'))
    self.start_screen_top_frame_elders_label.setText(_('Elders'))
    self.start_screen_top_frame_ministerialservants_label.setText(_('Ministerial Servants'))
    self.start_screen_bottom_frame_update_label.setText(_('Last index update') + ':')
    self.start_screen_bottom_frame_update_button.setText(_('Update indices'))
    self.start_screen_middle_frame_next_meeting_label.setText(_('Next meeting') + ':')


def update_dashboard(self):
    self.start_screen_top_frame_congregation_name.setText(self.settings.get('congregation_name', ''))
    self.start_screen_top_frame_congregation_name_circuit.setText(_('Circuit') + ': ' + self.settings.get('circuit', ''))
    self.start_screen_top_frame_publishers.setText(str(len(get_publishers_names(self.publishers_list))))
    self.start_screen_top_frame_pioneers.setText(str(len(get_publishers_pioneers(self.publishers_list))))
    self.start_screen_top_frame_elders.setText(str(len(get_publishers_elders(self.publishers_list))))
    self.start_screen_top_frame_ministerialservants.setText(str(len(get_publishers_ministerial_servants(self.publishers_list))))

    last_update_date = get_last_update_date()
    self.start_screen_bottom_frame_update_label.setVisible(bool(last_update_date))
    self.start_screen_bottom_frame_update.setVisible(bool(last_update_date))
    self.start_screen_bottom_frame_update_smbutton.setVisible(bool(last_update_date))
    self.start_screen_bottom_frame_update_button.setVisible(not bool(last_update_date))
    if last_update_date:
        self.start_screen_bottom_frame_update.setText(pretty_date(last_update_date) if self.settings.get('use_pretty_date', True) else last_update_date.strftime('%d/%m/%Y'))

    next_meeting_text = _('No meeting')
    next_meeting_date = False
    list_of_assignments = []
    week_start = get_week_start(datetime.today())

    if int(datetime.today().weekday()) <= int(self.settings.get('midweek_meeting_day', 3)) or int(datetime.today().weekday()) > int(self.settings.get('weekend_meeting_day', 6)):
        next_meeting_text = _('Our Christian Live and Ministry')
        if int(datetime.today().weekday()) > int(self.settings.get('weekend_meeting_day', 6)):
            week_start += timedelta(days=7)
        next_meeting_date = week_start + timedelta(days=self.settings.get('midweek_meeting_day', 3))

        midweek_load_assignments(self)
        assignments = self.assignments.get('midweek_meeting', {}).get(week_start.strftime('%Y%m%d'), {})
        for assignment in assignments:
            if assignments[assignment] and not assignments[assignment] in list_of_assignments:
                list_of_assignments.append(assignments[assignment])

    elif int(datetime.today().weekday()) <= int(self.settings.get('weekend_meeting_day', 6)):

        next_meeting_text = _('Weekend meeting')
        next_meeting_date = week_start + timedelta(days=self.settings.get('weekend_meeting_day', 6))

        weekend_load_assignments(self)
        assignments = self.assignments.get('weekend_meeting', {}).get(week_start.strftime('%Y%m%d'), {})
        for assignment in assignments:
            if assignments[assignment] and not assignments[assignment] in list_of_assignments:
                list_of_assignments.append(assignments[assignment])

    self.start_screen_middle_frame_next_meeting_description.setText(next_meeting_text)
    if next_meeting_date:
        self.start_screen_middle_frame_next_meeting_remaining_time.setText(pretty_date(next_meeting_date) if self.settings.get('use_pretty_date', True) else next_meeting_date.strftime('%d/%m/%Y'))
    # self.start_screen_middle_frame_next_meeting_assignments.setText('\n'.join(list_of_assignments))



def start_screen_bottom_frame_update_button_clicked(self):
    self.stackedWidget.setCurrentWidget(self.update_page)
    update_widgets(self)
    # settings_update_button_clicked(self)
    # update_dashboard(self)