#!/usr/bin/env python3

import os
import json

from PySide6.QtWidgets import QWidget, QVBoxLayout, QFrame, QPushButton, QSizePolicy, QGridLayout, QLabel, QScrollArea, QLayout
from PySide6.QtGui import QCursor
from PySide6.QtCore import QSize, Qt

from prat.modules.translation import _
from prat.modules.date import get_week_start
from prat.modules.paths import PATH_PRAT_WEEKEND_MEETING_ASSIGMENTS_FILE, get_graphics_path
from prat.modules.publishers import get_publishers_elders, get_publishers_leadership, get_publishers_watchtowerstudy_readers, get_publishers_weekend_meeting_chairmans, get_publishers_attendants, get_publishers_audio_operators, get_publishers_media_operators, get_publishers_names, get_publishers_meetings_prayer, get_publishers_roving_microphones
from prat.special_widgets import QComboBoxNoWheel, AssignmentsList


MODULE_INFO = {
    'title': _('Weekend Meeting'),
    'name': 'weekend_meeting',
    'date_enabled': True
}


def add_menu_widgets(self):
    self.modules_menu_button_weekend_meeting = QPushButton(self.modules_menu)
    self.modules_menu_button_weekend_meeting.setObjectName(u"modules_menu_button_weekend_meeting")
    self.modules_menu_button_weekend_meeting.setProperty('class', 'modules_menu')
    sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
    sizePolicy.setHeightForWidth(self.modules_menu_button_weekend_meeting.sizePolicy().hasHeightForWidth())
    self.modules_menu_button_weekend_meeting.setSizePolicy(sizePolicy)
    self.modules_menu_button_weekend_meeting.setMinimumHeight(45)
    self.modules_menu_button_weekend_meeting.setCursor(QCursor(Qt.PointingHandCursor))
    self.modules_menu_button_weekend_meeting.setLayoutDirection(Qt.LeftToRight)
    self.modules_menu_button_weekend_meeting.setStyleSheet('background-image: url("' + get_graphics_path('weekend_meeting_icon.svg') + '");')
    self.modules_menu_button_weekend_meeting.clicked.connect(lambda: modules_menu_button_weekend_meeting_clicked(self))

    self.modules_menu_vl.addWidget(self.modules_menu_button_weekend_meeting)


def add_widgets(self):
    self.weekend_meeting = QWidget()
    self.weekend_meeting.setObjectName(u"weekend_meeting")

    self.weekend_meeting_vbox = QVBoxLayout(self.weekend_meeting)
    self.weekend_meeting_vbox.setObjectName(u"weekend_meeting_vbox")

    self.weekend_meeting_scroll = QScrollArea(self.weekend_meeting)
    self.weekend_meeting_scroll.setObjectName(u"weekend_meeting_scroll")
    self.weekend_meeting_scroll.setWidgetResizable(True)
    self.weekend_meeting_scroll.setFrameShape(QFrame.NoFrame)
    self.weekend_meeting_scroll.setAlignment(Qt.AlignLeading | Qt.AlignLeft | Qt.AlignTop)

    self.weekend_meeting_scroll_widget = QWidget()
    self.weekend_meeting_scroll_widget.setObjectName(u"weekend_meeting_scroll_widget")

    sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
    sizePolicy3.setHorizontalStretch(0)
    sizePolicy3.setVerticalStretch(0)
    sizePolicy3.setHeightForWidth(self.weekend_meeting_scroll_widget.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_scroll_widget.setSizePolicy(sizePolicy3)

    self.weekend_meeting_scroll_widget_vbox = QVBoxLayout(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_scroll_widget_vbox.setObjectName(u"weekend_meeting_scroll_widget_vbox")

    self.weekend_meeting_chairman_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_chairman_line.setObjectName('weekend_meeting_chairman_line')

    self.weekend_meeting_chairman_grid = QGridLayout(self.weekend_meeting_chairman_line)
    self.weekend_meeting_chairman_grid.setObjectName(u"chairman_line")

    self.weekend_meeting_chairman_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_chairman_label.setObjectName(u"chairman_label")

    self.weekend_meeting_chairman_grid.addWidget(self.weekend_meeting_chairman_label, 0, 0, 1, 1)

    self.weekend_meeting_chairman_name = QComboBoxNoWheel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_chairman_name.setEditable(True)
    # self.weekend_meeting_chairman_name.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_chairman_name.editTextChanged.connect(self.weekend_meeting_chairman_name.style().polish(self.weekend_meeting_chairman_name))
    self.weekend_meeting_chairman_name.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_chairman_name.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_chairman_name.setObjectName(u"chairman_name")

    self.weekend_meeting_chairman_grid.addWidget(self.weekend_meeting_chairman_name, 1, 0, 1, 1)

    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_chairman_line)

    self.weekend_meeting_opening_prayer_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_opening_prayer_line.setObjectName('weekend_meeting_opening_prayer_line')

    self.weekend_meeting_opening_prayer_grid = QGridLayout(self.weekend_meeting_opening_prayer_line)
    self.weekend_meeting_opening_prayer_grid.setObjectName(u"opening_prayer_line")

    self.weekend_meeting_opening_prayer_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_opening_prayer_label.setObjectName(u"weekend_meeting_opening_prayer_label")

    self.weekend_meeting_opening_prayer_grid.addWidget(self.weekend_meeting_opening_prayer_label, 0, 0, 1, 1)

    self.weekend_meeting_opening_prayer = QComboBoxNoWheel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_opening_prayer.setEditable(True)
    # self.weekend_meeting_opening_prayer.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_opening_prayer.editTextChanged.connect(self.weekend_meeting_opening_prayer.style().polish(self.weekend_meeting_opening_prayer))
    self.weekend_meeting_opening_prayer.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_opening_prayer.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_opening_prayer.setObjectName(u"opening_prayer")

    self.weekend_meeting_opening_prayer_grid.addWidget(self.weekend_meeting_opening_prayer, 1, 0, 1, 1)

    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_opening_prayer_line)

    self.weekend_meeting_public_talk_area = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_public_talk_area.setObjectName(u"weekend_meeting_public_talk_area")

    self.weekend_meeting_public_talk_area_vbox = QVBoxLayout(self.weekend_meeting_public_talk_area)
    self.weekend_meeting_public_talk_area_vbox.setObjectName(u"weekend_meeting_public_talk_area_vbox")

    self.weekend_meeting_public_talk_label = QLabel(self.weekend_meeting_public_talk_area)
    self.weekend_meeting_public_talk_label.setObjectName(u"weekend_meeting_public_talk_label")
    sizePolicy4 = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy4.setHorizontalStretch(0)
    sizePolicy4.setVerticalStretch(0)
    sizePolicy4.setHeightForWidth(self.weekend_meeting_public_talk_label.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_public_talk_label.setSizePolicy(sizePolicy4)
    self.weekend_meeting_public_talk_label.setMinimumSize(QSize(0, 0))
    self.weekend_meeting_public_talk_label.setStyleSheet(u"QLabel { background-color: #373433; padding:8px; font-weight: bold; color:white; }")

    self.weekend_meeting_public_talk_area_vbox.addWidget(self.weekend_meeting_public_talk_label)

    self.weekend_meeting_public_talk_line = QWidget(self.weekend_meeting_public_talk_area)
    self.weekend_meeting_public_talk_line.setObjectName('weekend_meeting_public_talk_line')

    self.weekend_meeting_public_talk_grid = QGridLayout(self.weekend_meeting_public_talk_line)
    self.weekend_meeting_public_talk_grid.setObjectName(u"weekend_meeting_public_talk_line")
    self.weekend_meeting_public_talk_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.weekend_meeting_public_talk_title = QLabel(self.weekend_meeting_public_talk_line)
    self.weekend_meeting_public_talk_title.setObjectName(u"weekend_meeting_public_talk_title")
    # sizePolicy3.setHeightForWidth(self.weekend_meeting_public_talk_title.sizePolicy().hasHeightForWidth())
    # self.weekend_meeting_public_talk_title.setSizePolicy(sizePolicy3)

    self.weekend_meeting_public_talk_grid.addWidget(self.weekend_meeting_public_talk_title, 0, 0, 1, 1)

    self.weekend_meeting_public_talk = QComboBoxNoWheel(self.weekend_meeting_public_talk_line)
    self.weekend_meeting_public_talk.setEditable(True)
    # self.weekend_meeting_public_talk.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_public_talk.editTextChanged.connect(self.weekend_meeting_public_talk.style().polish(self.weekend_meeting_public_talk))
    self.weekend_meeting_public_talk.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_public_talk.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_public_talk.setObjectName(u"weekend_meeting_public_talk")
    # sizePolicy3.setHeightForWidth(self.weekend_meeting_public_talk.sizePolicy().hasHeightForWidth())
    # self.weekend_meeting_public_talk.setSizePolicy(sizePolicy3)

    self.weekend_meeting_public_talk_grid.addWidget(self.weekend_meeting_public_talk, 1, 0, 1, 1)

    self.weekend_meeting_public_talk_speaker_label = QLabel(self.weekend_meeting_public_talk_line)
    self.weekend_meeting_public_talk_speaker_label.setObjectName(u"weekend_meeting_public_talk_speaker_label")
    # sizePolicy3.setHeightForWidth(self.weekend_meeting_public_talk_speaker_label.sizePolicy().hasHeightForWidth())
    # self.weekend_meeting_public_talk_speaker_label.setSizePolicy(sizePolicy3)

    self.weekend_meeting_public_talk_grid.addWidget(self.weekend_meeting_public_talk_speaker_label, 2, 0, 1, 1)

    self.weekend_meeting_public_talk_speaker = QComboBoxNoWheel(self.weekend_meeting_public_talk_line)
    self.weekend_meeting_public_talk_speaker.setEditable(True)
    # self.weekend_meeting_public_talk_speaker.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_public_talk_speaker.editTextChanged.connect(self.weekend_meeting_public_talk_speaker.style().polish(self.weekend_meeting_public_talk))
    self.weekend_meeting_public_talk_speaker.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_public_talk_speaker.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_public_talk_speaker.setObjectName(u"weekend_meeting_public_talk")
    # sizePolicy3.setHeightForWidth(self.weekend_meeting_public_talk_speaker.sizePolicy().hasHeightForWidth())
    # self.weekend_meeting_public_talk_speaker.setSizePolicy(sizePolicy3)

    self.weekend_meeting_public_talk_grid.addWidget(self.weekend_meeting_public_talk_speaker, 3, 0, 1, 1)

    self.weekend_meeting_public_talk_area_vbox.addWidget(self.weekend_meeting_public_talk_line)

    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_public_talk_area)

    self.weekend_meeting_watchtower_study_area = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_watchtower_study_area.setObjectName(u"weekend_meeting_watchtower_study_area")

    self.weekend_meeting_watchtower_study_area_vbox = QVBoxLayout(self.weekend_meeting_watchtower_study_area)
    self.weekend_meeting_watchtower_study_area_vbox.setObjectName(u"weekend_meeting_watchtower_study_area_vbox")

    self.weekend_meeting_watchtower_study_label = QLabel(self.weekend_meeting_watchtower_study_area)
    self.weekend_meeting_watchtower_study_label.setObjectName(u"weekend_meeting_watchtower_study_label")
    self.weekend_meeting_watchtower_study_label.setSizePolicy(sizePolicy4)
    self.weekend_meeting_watchtower_study_label.setMinimumSize(QSize(0, 0))
    self.weekend_meeting_watchtower_study_label.setStyleSheet(u"QLabel { background-color: #373433; padding:8px; font-weight: bold; color:white;}")

    self.weekend_meeting_watchtower_study_area_vbox.addWidget(self.weekend_meeting_watchtower_study_label)

    self.weekend_meeting_watchtower_study_source_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_watchtower_study_source_line.setObjectName('weekend_meeting_watchtower_study_source_line')

    self.weekend_meeting_watchtower_study_source_grid = QGridLayout(self.weekend_meeting_watchtower_study_source_line)
    self.weekend_meeting_watchtower_study_source_grid.setObjectName(u"weekend_meeting_watchtower_study_source_line")
    self.weekend_meeting_watchtower_study_source_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.weekend_meeting_watchtower_study_source_label = QLabel(self.weekend_meeting_watchtower_study_source_line)
    self.weekend_meeting_watchtower_study_source_label.setObjectName(u"weekend_meeting_watchtower_study_source_label")
    sizePolicy3.setHeightForWidth(self.weekend_meeting_watchtower_study_source_label.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_watchtower_study_source_label.setSizePolicy(sizePolicy3)

    self.weekend_meeting_watchtower_study_source_grid.addWidget(self.weekend_meeting_watchtower_study_source_label, 0, 0, 1, 1)

    self.weekend_meeting_watchtower_study_source = QLabel(self.weekend_meeting_watchtower_study_area)
    self.weekend_meeting_watchtower_study_source.setObjectName(u"weekend_meeting_watchtower_study_source")
    sizePolicy3.setHeightForWidth(self.weekend_meeting_watchtower_study_source.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_watchtower_study_source.setSizePolicy(sizePolicy3)
    self.weekend_meeting_watchtower_study_source.setStyleSheet(u"QLabel { font-size: 18px; }")

    self.weekend_meeting_watchtower_study_source_grid.addWidget(self.weekend_meeting_watchtower_study_source, 1, 0, 1, 1)

    self.weekend_meeting_watchtower_study_area_vbox.addWidget(self.weekend_meeting_watchtower_study_source_line)

    self.weekend_meeting_watchtower_study_conductor_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_watchtower_study_conductor_line.setObjectName('weekend_meeting_watchtower_study_conductor_line')

    self.weekend_meeting_watchtower_study_conductor_grid = QGridLayout(self.weekend_meeting_watchtower_study_conductor_line)
    self.weekend_meeting_watchtower_study_conductor_grid.setObjectName(u"weekend_meeting_watchtower_study_conductor_line")
    self.weekend_meeting_watchtower_study_conductor_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.weekend_meeting_watchtower_study_conductor_label = QLabel(self.weekend_meeting_watchtower_study_conductor_line)
    self.weekend_meeting_watchtower_study_conductor_label.setObjectName(u"weekend_meeting_watchtower_study_conductor_label")
    sizePolicy3.setHeightForWidth(self.weekend_meeting_watchtower_study_conductor_label.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_watchtower_study_conductor_label.setSizePolicy(sizePolicy3)

    self.weekend_meeting_watchtower_study_conductor_grid.addWidget(self.weekend_meeting_watchtower_study_conductor_label, 0, 0, 1, 1)

    self.weekend_meeting_watchtower_study_conductor = QComboBoxNoWheel(self.weekend_meeting_watchtower_study_conductor_line)
    self.weekend_meeting_watchtower_study_conductor.setEditable(True)
    # self.weekend_meeting_watchtower_study.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_watchtower_study.editTextChanged.connect(self.weekend_meeting_watchtower_study.style().polish(self.weekend_meeting_watchtower_study))
    self.weekend_meeting_watchtower_study_conductor.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_watchtower_study_conductor.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_watchtower_study_conductor.setObjectName(u"weekend_meeting_watchtower_study_conductor")
    sizePolicy3.setHeightForWidth(self.weekend_meeting_watchtower_study_conductor.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_watchtower_study_conductor.setSizePolicy(sizePolicy3)

    self.weekend_meeting_watchtower_study_conductor_grid.addWidget(self.weekend_meeting_watchtower_study_conductor, 1, 0, 1, 1)

    self.weekend_meeting_watchtower_study_area_vbox.addWidget(self.weekend_meeting_watchtower_study_conductor_line)

    self.weekend_meeting_watchtower_study_reader_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_watchtower_study_reader_line.setObjectName('weekend_meeting_watchtower_study_reader_line')

    self.weekend_meeting_watchtower_study_reader_grid = QGridLayout(self.weekend_meeting_watchtower_study_reader_line)
    self.weekend_meeting_watchtower_study_reader_grid.setObjectName(u"weekend_meeting_watchtower_study_reader_line")
    self.weekend_meeting_watchtower_study_reader_grid.setSizeConstraint(QLayout.SetDefaultConstraint)

    self.weekend_meeting_watchtower_study_reader_label = QLabel(self.weekend_meeting_watchtower_study_reader_line)
    self.weekend_meeting_watchtower_study_reader_label.setObjectName(u"weekend_meeting_watchtower_study_reader_label")
    sizePolicy3.setHeightForWidth(self.weekend_meeting_watchtower_study_reader_label.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_watchtower_study_reader_label.setSizePolicy(sizePolicy3)

    self.weekend_meeting_watchtower_study_reader_grid.addWidget(self.weekend_meeting_watchtower_study_reader_label, 0, 0, 1, 1)

    self.weekend_meeting_watchtower_study_reader = QComboBoxNoWheel(self.weekend_meeting_watchtower_study_reader_line)
    self.weekend_meeting_watchtower_study_reader.setEditable(True)
    # self.weekend_meeting_watchtower_study.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_watchtower_study.editTextChanged.connect(self.weekend_meeting_watchtower_study.style().polish(self.weekend_meeting_watchtower_study))
    self.weekend_meeting_watchtower_study_reader.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_watchtower_study_reader.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_watchtower_study_reader.setObjectName(u"weekend_meeting_watchtower_study_reader")
    sizePolicy3.setHeightForWidth(self.weekend_meeting_watchtower_study_reader.sizePolicy().hasHeightForWidth())
    self.weekend_meeting_watchtower_study_reader.setSizePolicy(sizePolicy3)

    self.weekend_meeting_watchtower_study_reader_grid.addWidget(self.weekend_meeting_watchtower_study_reader, 1, 0, 1, 1)

    self.weekend_meeting_watchtower_study_area_vbox.addWidget(self.weekend_meeting_watchtower_study_reader_line)

    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_watchtower_study_area)

    self.weekend_meeting_closing_prayer_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_closing_prayer_line.setObjectName('weekend_meeting_closing_prayer_line')

    self.weekend_meeting_closing_prayer_grid = QGridLayout(self.weekend_meeting_closing_prayer_line)
    self.weekend_meeting_closing_prayer_grid.setObjectName(u"closing_prayer_line")

    self.weekend_meeting_closing_prayer_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_closing_prayer_label.setObjectName(u"weekend_meeting_closing_prayer_label")

    self.weekend_meeting_closing_prayer_grid.addWidget(self.weekend_meeting_closing_prayer_label, 0, 0, 1, 1)

    self.weekend_meeting_closing_prayer = QComboBoxNoWheel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_closing_prayer.setEditable(True)
    # self.weekend_meeting_closing_prayer.lineEdit().setPlaceholderText(_('No one assigned yet'))
    # self.weekend_meeting_closing_prayer.editTextChanged.connect(self.weekend_meeting_closing_prayer.style().polish(self.weekend_meeting_closing_prayer))
    self.weekend_meeting_closing_prayer.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_closing_prayer.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_closing_prayer.setObjectName(u"closing_prayer")

    self.weekend_meeting_closing_prayer_grid.addWidget(self.weekend_meeting_closing_prayer, 1, 0, 1, 1)

    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_closing_prayer_line)

    self.weekend_meeting_routine_tasks_separator = QFrame()
    self.weekend_meeting_routine_tasks_separator.setObjectName('weekend_meeting_routine_tasks_separator')
    self.weekend_meeting_routine_tasks_separator.setFrameShape(QFrame.HLine)
    self.weekend_meeting_routine_tasks_separator.setFrameShadow(QFrame.Sunken)

    self.weekend_meeting_scroll_widget_vbox.addSpacing(8)
    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_routine_tasks_separator)

    self.weekend_meeting_routine_tasks_line = QWidget(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_line.setObjectName('weekend_meeting_routine_tasks_line')
    self.weekend_meeting_routine_tasks_line.setLayout(QGridLayout())

    self.weekend_meeting_routine_tasks_attendants_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_attendants_label.setObjectName(u"weekend_meeting_routine_tasks_attendants_label")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_attendants_label, 0, 0, 1, 1)

    self.weekend_meeting_routine_tasks_attendants = AssignmentsList(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_attendants.item_changed.connect(lambda: update_assignments(self))
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_attendants, 1, 0, 1, 1)

    self.weekend_meeting_routine_tasks_microphone_operators_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_microphone_operators_label.setObjectName(u"weekend_meeting_routine_tasks_microphone_operators_label")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_microphone_operators_label, 2, 0, 1, 1)

    self.weekend_meeting_routine_tasks_microphone_operators = AssignmentsList(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_microphone_operators.item_changed.connect(lambda: update_assignments(self))
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_microphone_operators, 3, 0, 1, 1)

    self.weekend_meeting_routine_tasks_audio_operator_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_audio_operator_label.setObjectName(u"weekend_meeting_routine_tasks_audio_operator_label")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_audio_operator_label, 4, 0, 1, 1)

    self.weekend_meeting_routine_tasks_audio_operator = QComboBoxNoWheel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_audio_operator.setEditable(True)
    self.weekend_meeting_routine_tasks_audio_operator.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_routine_tasks_audio_operator.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_routine_tasks_audio_operator.setObjectName(u"weekend_meeting_routine_tasks_audio_operator")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_audio_operator, 5, 0, 1, 1)

    self.weekend_meeting_routine_tasks_platform_operator_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_platform_operator_label.setObjectName(u"weekend_meeting_routine_tasks_platform_operator_label")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_platform_operator_label, 6, 0, 1, 1)

    self.weekend_meeting_routine_tasks_platform_operator = QComboBoxNoWheel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_platform_operator.setEditable(True)
    self.weekend_meeting_routine_tasks_platform_operator.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_routine_tasks_platform_operator.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_routine_tasks_platform_operator.setObjectName(u"weekend_meeting_routine_tasks_platform_operator")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_platform_operator, 7, 0, 1, 1)

    self.weekend_meeting_routine_tasks_video_operator_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_video_operator_label.setObjectName(u"weekend_meeting_routine_tasks_video_operator_label")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_video_operator_label, 8, 0, 1, 1)

    self.weekend_meeting_routine_tasks_video_operator = QComboBoxNoWheel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_video_operator.setEditable(True)
    self.weekend_meeting_routine_tasks_video_operator.setFocusPolicy(Qt.StrongFocus)
    self.weekend_meeting_routine_tasks_video_operator.lineEdit().editingFinished.connect(lambda: update_assignments(self))
    self.weekend_meeting_routine_tasks_video_operator.setObjectName(u"weekend_meeting_routine_tasks_video_operator")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_video_operator, 9, 0, 1, 1)

    self.weekend_meeting_routine_tasks_videoconference_attendants_label = QLabel(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_videoconference_attendants_label.setObjectName(u"weekend_meeting_routine_tasks_videoconference_attendants_label")
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_videoconference_attendants_label, 10, 0, 1, 1)

    self.weekend_meeting_routine_tasks_videoconference_attendants = AssignmentsList(self.weekend_meeting_scroll_widget)
    self.weekend_meeting_routine_tasks_videoconference_attendants.item_changed.connect(lambda: update_assignments(self))
    self.weekend_meeting_routine_tasks_line.layout().addWidget(self.weekend_meeting_routine_tasks_videoconference_attendants, 11, 0, 1, 1)

    self.weekend_meeting_scroll_widget_vbox.addWidget(self.weekend_meeting_routine_tasks_line)

    self.weekend_meeting_scroll.setWidget(self.weekend_meeting_scroll_widget)

    self.weekend_meeting_vbox.addWidget(self.weekend_meeting_scroll)

    self.stackedWidget.addWidget(self.weekend_meeting)


def modules_menu_button_weekend_meeting_clicked(self):
    self.selected_module = MODULE_INFO
    load_assignments(self)
    self.stackedWidget.setCurrentWidget(self.weekend_meeting)
    self.toggle_menu(True)
    self.resetStyle(self.modules_menu_button_weekend_meeting.objectName())
    self.modules_menu_button_weekend_meeting.setStyleSheet(self.selectMenu(self.modules_menu_button_weekend_meeting.styleSheet()))


def translate_widgets(self):
    self.modules_menu_button_weekend_meeting.setText(_(MODULE_INFO['title']))
    self.weekend_meeting_chairman_label.setText(_('Chairman'))
    self.weekend_meeting_opening_prayer_label.setText(_('Opening prayer'))
    self.weekend_meeting_closing_prayer_label.setText(_('Closing prayer'))
    self.weekend_meeting_public_talk_label.setText(_('Public Talk').upper())
    self.weekend_meeting_public_talk_title.setText(_('Talk'))
    self.weekend_meeting_public_talk_speaker_label.setText(_('Speaker'))
    self.weekend_meeting_watchtower_study_label.setText(_('Watchtower Study').upper())
    self.weekend_meeting_watchtower_study_conductor_label.setText(_('Conductor'))
    self.weekend_meeting_watchtower_study_reader_label.setText(_('Reader'))
    self.weekend_meeting_routine_tasks_attendants_label.setText(_('Attendants'))
    self.weekend_meeting_routine_tasks_microphone_operators_label.setText(_('Microphone operators'))
    self.weekend_meeting_routine_tasks_audio_operator_label.setText(_('Audio operator'))
    self.weekend_meeting_routine_tasks_platform_operator_label.setText(_('Platform operator'))
    self.weekend_meeting_routine_tasks_video_operator_label.setText(_('Video operator'))
    self.weekend_meeting_routine_tasks_videoconference_attendants_label.setText(_('Videoconference attendants'))


def update_date(self):
    week_start = get_week_start(self.selected_date)

    self.weekend_meeting_watchtower_study_area.setVisible(bool(self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {})))

    self.weekend_meeting_watchtower_study_source_label.setText(self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('source', ''))
    self.weekend_meeting_watchtower_study_source.setText(self.index_w.get(self.selected_language, {}).get(week_start.strftime('%Y%m%d'), {}).get('title', ''))
    update_assignment_widgets(self)


MODULE_INFO['date_function'] = update_date


def update_assignment_widgets(self):
    week_start = get_week_start(self.selected_date)
    w_chairman_list = get_publishers_weekend_meeting_chairmans(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_chairman_name.clear()
    self.weekend_meeting_chairman_name.addItems(w_chairman_list)

    opening_prayer_list = get_publishers_meetings_prayer(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_opening_prayer.clear()
    self.weekend_meeting_opening_prayer.addItems(opening_prayer_list)

    public_talks = self.public_talks_config.get('themes_list', {})
    public_talks_list = []
    for talk in sorted(list(public_talks.keys())):
        public_talks_list.append(public_talks[talk] + ' ({})'.format(talk))
    self.weekend_meeting_public_talk.clear()
    self.weekend_meeting_public_talk.addItems(public_talks_list)

    speakers_list = get_publishers_leadership(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_public_talk_speaker.clear()
    self.weekend_meeting_public_talk_speaker.addItems(speakers_list)

    watchtower_conductors = get_publishers_elders(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_watchtower_study_conductor.clear()
    self.weekend_meeting_watchtower_study_conductor.addItems(watchtower_conductors)

    watchtower_readers = get_publishers_watchtowerstudy_readers(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_watchtower_study_reader.clear()
    self.weekend_meeting_watchtower_study_reader.addItems(watchtower_readers)

    closing_prayer_list = get_publishers_meetings_prayer(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_closing_prayer.clear()
    self.weekend_meeting_closing_prayer.addItems(closing_prayer_list)

    attendants = get_publishers_attendants(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_routine_tasks_attendants.clear()
    self.weekend_meeting_routine_tasks_attendants.setItems(attendants)

    roving_microphone = get_publishers_roving_microphones(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_routine_tasks_microphone_operators.clear()
    self.weekend_meeting_routine_tasks_microphone_operators.setItems(roving_microphone)

    audio_operators = get_publishers_audio_operators(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_routine_tasks_audio_operator.clear()
    self.weekend_meeting_routine_tasks_audio_operator.addItems(audio_operators)

    roving_microphone = get_publishers_roving_microphones(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_routine_tasks_platform_operator.clear()
    self.weekend_meeting_routine_tasks_platform_operator.addItems(roving_microphone)

    video_operators = get_publishers_media_operators(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    self.weekend_meeting_routine_tasks_video_operator.clear()
    self.weekend_meeting_routine_tasks_video_operator.addItems(video_operators)

    videoconference_attendants = get_publishers_media_operators(self.publishers_list, sort=self.settings.get('sorted_names', False)) if self.settings.get('suggest_names', False) else get_publishers_names(self.publishers_list, sort=self.settings.get('sorted_names', False))
    # self.weekend_meeting_routine_tasks_videoconference_attendants.clear()
    self.weekend_meeting_routine_tasks_videoconference_attendants.setItems(videoconference_attendants)

    week_start = get_week_start(self.selected_date)
    self.weekend_meeting_chairman_name.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('chairman', ''))
    self.weekend_meeting_opening_prayer.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('opening_prayer', ''))
    self.weekend_meeting_public_talk.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('public_talk', ''))
    self.weekend_meeting_public_talk_speaker.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('speaker', ''))
    self.weekend_meeting_watchtower_study_conductor.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('watchtower_conductor', ''))
    self.weekend_meeting_watchtower_study_reader.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('watchtower_reader', ''))
    self.weekend_meeting_closing_prayer.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('closing_prayer', ''))
    self.weekend_meeting_routine_tasks_attendants.setList(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('attendants', []))
    self.weekend_meeting_routine_tasks_microphone_operators.setList(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('microphone_operators', []))
    self.weekend_meeting_routine_tasks_platform_operator.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('platform_operator', ''))
    self.weekend_meeting_routine_tasks_audio_operator.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('audio_operator', ''))
    self.weekend_meeting_routine_tasks_video_operator.setCurrentText(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('video_operator', ''))
    self.weekend_meeting_routine_tasks_videoconference_attendants.setList(self.assignments['weekend_meeting'].get(week_start.strftime('%Y%m%d'), {}).get('videoconference_attendants', []))

def update_assignments(self):
    week_start = get_week_start(self.selected_date)
    if not week_start.strftime('%Y%m%d') in self.assignments['weekend_meeting']:
        self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')] = {}
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['chairman'] = self.weekend_meeting_chairman_name.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['opening_prayer'] = self.weekend_meeting_opening_prayer.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['public_talk'] = self.weekend_meeting_public_talk.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['speaker'] = self.weekend_meeting_public_talk_speaker.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['watchtower_conductor'] = self.weekend_meeting_watchtower_study_conductor.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['watchtower_reader'] = self.weekend_meeting_watchtower_study_reader.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['closing_prayer'] = self.weekend_meeting_closing_prayer.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['attendants'] = self.weekend_meeting_routine_tasks_attendants.getList()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['microphone_operators'] = self.weekend_meeting_routine_tasks_microphone_operators.getList()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['audio_operator'] = self.weekend_meeting_routine_tasks_audio_operator.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['platform_operator'] = self.weekend_meeting_routine_tasks_platform_operator.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['video_operator'] = self.weekend_meeting_routine_tasks_video_operator.currentText()
    self.assignments['weekend_meeting'][week_start.strftime('%Y%m%d')]['videoconference_attendants'] = self.weekend_meeting_routine_tasks_videoconference_attendants.getList()


def save_assignments(self, weekend_meeting_assignmets_file=PATH_PRAT_WEEKEND_MEETING_ASSIGMENTS_FILE):
    if 'weekend_meeting' in self.assignments:
        open(weekend_meeting_assignmets_file, 'w').write(json.dumps(self.assignments['weekend_meeting'], indent=4, ensure_ascii=False))


MODULE_INFO['save_function'] = save_assignments


def load_assignments(self, weekend_meeting_assignmets_file=PATH_PRAT_WEEKEND_MEETING_ASSIGMENTS_FILE):
    if not self.assignments.get('weekend_meeting', {}):
        weekend_meeting_assignments_json = json.loads('{}')
        if os.path.isfile(weekend_meeting_assignmets_file):
            weekend_meeting_assignments_json = json.load(open(weekend_meeting_assignmets_file))

        self.assignments['weekend_meeting'] = weekend_meeting_assignments_json
