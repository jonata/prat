#!/usr/bin/env python3

from PySide6.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QFrame, QPushButton, QSizePolicy, QLabel
from PySide6.QtGui import QPixmap
from PySide6.QtCore import Qt

from prat.modules.translation import _, get_available_language_names, set_language
from prat.modules.paths import get_graphics_path
from prat.modules.settings import modules_menu_button_settings_clicked
from prat.modules.publishers import modules_menu_button_publishers_clicked
from prat.special_widgets import QComboBoxNoWheel


MODULE_INFO = {
    'title': _('Welcome'),
    'name': 'welcome_screen',
    'date_enabled': False
}


def add_widgets(self):
    self.welcome_screen = QWidget()
    self.welcome_screen.setObjectName(u"welcome_screen")

    self.welcome_screen_vbox = QVBoxLayout(self.welcome_screen)
    self.welcome_screen_vbox.setObjectName(u"welcome_screen_vbox")

    self.welcome_screen_vbox.addStretch()

    self.welcome_screen_logo_frame = QFrame(self.welcome_screen)
    self.welcome_screen_logo_frame.setObjectName(u"welcome_screen_logo_frame")

    self.welcome_screen_logo_hbox = QHBoxLayout(self.welcome_screen_logo_frame)
    self.welcome_screen_logo_hbox.setObjectName(u"welcome_screen_logo_hbox")

    # self.welcome_screen_logo_hbox.addStretch()

    # self.welcome_screen_icon = QLabel(self.welcome_screen)
    # self.welcome_screen_icon.setObjectName('welcome_screen_icon')
    # self.welcome_screen_icon.setPixmap(QPixmap(get_graphics_path('prat.png')).scaled(64,64))

    # self.welcome_screen_logo_hbox.addWidget(self.welcome_screen_icon, 0, Qt.AlignRight)

    self.welcome_screen_logo_hbox.addSpacing(4)

    self.welcome_screen_icon_label = QLabel(self.welcome_screen)
    self.welcome_screen_icon_label.setObjectName('welcome_screen_icon_label')
    # self.welcome_screen_icon_label.setAlignment(Qt.AlignCenter)

    self.welcome_screen_logo_hbox.addWidget(self.welcome_screen_icon_label, 0, Qt.AlignCenter)

    # self.welcome_screen_logo_hbox.addStretch()

    self.welcome_screen_vbox.addWidget(self.welcome_screen_logo_frame)

    self.welcome_screen_vbox.addSpacing(20)

    self.welcome_screen_language_combobox = QComboBoxNoWheel(self.welcome_screen)
    self.welcome_screen_language_combobox.setFocusPolicy(Qt.StrongFocus)
    self.welcome_screen_language_combobox.activated.connect(lambda: apply_language(self))
    self.welcome_screen_language_combobox.setObjectName(u"welcome_screen_language_combobox")
    self.welcome_screen_language_combobox.setMinimumWidth(300)

    self.welcome_screen_vbox.addWidget(self.welcome_screen_language_combobox, 0, Qt.AlignCenter)

    self.welcome_screen_language_checked_line = QFrame(self.welcome_screen)
    self.welcome_screen_language_checked_line_hbox = QHBoxLayout(self.welcome_screen_language_checked_line)

    self.welcome_screen_language_checked_icon = QLabel(self.welcome_screen_language_checked_line)
    self.welcome_screen_language_checked_icon.setObjectName(u"welcome_screen_language_checked_icon")
    self.welcome_screen_language_checked_icon.setPixmap(QPixmap(get_graphics_path('check_icon.svg')))

    self.welcome_screen_language_checked_line_hbox.addWidget(self.welcome_screen_language_checked_icon, 0, Qt.AlignRight)

    self.welcome_screen_language_checked_label = QLabel(self.welcome_screen_language_checked_line)
    self.welcome_screen_language_checked_label.setObjectName(u"welcome_screen_language_checked_label")

    self.welcome_screen_language_checked_line_hbox.addWidget(self.welcome_screen_language_checked_label, 0, Qt.AlignLeft)

    self.welcome_screen_vbox.addWidget(self.welcome_screen_language_checked_line, 0, Qt.AlignCenter)

    self.welcome_screen_update_congregation_info_button = QPushButton(self.welcome_screen)
    self.welcome_screen_update_congregation_info_button.setObjectName('welcome_screen_update_congregation_info_button')
    self.welcome_screen_update_congregation_info_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.welcome_screen_update_congregation_info_button.sizePolicy().hasHeightForWidth())
    self.welcome_screen_update_congregation_info_button.setSizePolicy(sizePolicy)
    self.welcome_screen_update_congregation_info_button.clicked.connect(lambda: welcome_screen_update_congregation_info_button_clicked(self))

    self.welcome_screen_vbox.addWidget(self.welcome_screen_update_congregation_info_button, 0, Qt.AlignCenter)

    self.welcome_screen_update_congregation_info_checked_line = QFrame(self.welcome_screen)
    self.welcome_screen_update_congregation_info_checked_line_hbox = QHBoxLayout(self.welcome_screen_update_congregation_info_checked_line)

    self.welcome_screen_update_congregation_info_checked_icon = QLabel(self.welcome_screen_update_congregation_info_checked_line)
    self.welcome_screen_update_congregation_info_checked_icon.setObjectName(u"welcome_screen_update_congregation_info_checked_icon")
    self.welcome_screen_update_congregation_info_checked_icon.setPixmap(QPixmap(get_graphics_path('check_icon.svg')))

    self.welcome_screen_update_congregation_info_checked_line_hbox.addWidget(self.welcome_screen_update_congregation_info_checked_icon, 0, Qt.AlignRight)

    self.welcome_screen_update_congregation_info_checked_label = QLabel(self.welcome_screen_update_congregation_info_checked_line)
    self.welcome_screen_update_congregation_info_checked_label.setObjectName(u"welcome_screen_update_congregation_info_checked_label")

    self.welcome_screen_update_congregation_info_checked_line_hbox.addWidget(self.welcome_screen_update_congregation_info_checked_label, 0, Qt.AlignLeft)

    self.welcome_screen_vbox.addWidget(self.welcome_screen_update_congregation_info_checked_line, 0, Qt.AlignCenter)

    self.welcome_screen_update_settings_button = QPushButton(self.welcome_screen)
    self.welcome_screen_update_settings_button.setObjectName('welcome_screen_update_settings_button')
    self.welcome_screen_update_settings_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.welcome_screen_update_settings_button.sizePolicy().hasHeightForWidth())
    self.welcome_screen_update_settings_button.setSizePolicy(sizePolicy)
    self.welcome_screen_update_settings_button.clicked.connect(lambda: welcome_screen_update_settings_button_clicked(self))

    self.welcome_screen_vbox.addWidget(self.welcome_screen_update_settings_button, 0, Qt.AlignCenter)

    self.welcome_screen_update_settings_checked_line = QFrame(self.welcome_screen)
    self.welcome_screen_update_settings_checked_line_hbox = QHBoxLayout(self.welcome_screen_update_settings_checked_line)

    self.welcome_screen_update_settings_checked_icon = QLabel(self.welcome_screen_update_settings_checked_line)
    self.welcome_screen_update_settings_checked_icon.setObjectName(u"welcome_screen_update_settings_checked_icon")
    self.welcome_screen_update_settings_checked_icon.setPixmap(QPixmap(get_graphics_path('check_icon.svg')))

    self.welcome_screen_update_settings_checked_line_hbox.addWidget(self.welcome_screen_update_settings_checked_icon, 0, Qt.AlignRight)

    self.welcome_screen_update_settings_checked_label = QLabel(self.welcome_screen_update_settings_checked_line)
    self.welcome_screen_update_settings_checked_label.setObjectName(u"welcome_screen_update_settings_checked_label")

    self.welcome_screen_update_settings_checked_line_hbox.addWidget(self.welcome_screen_update_settings_checked_label, 0, Qt.AlignLeft)

    self.welcome_screen_vbox.addWidget(self.welcome_screen_update_settings_checked_line, 0, Qt.AlignCenter)

    self.welcome_screen_update_publishers_button = QPushButton(self.welcome_screen)
    self.welcome_screen_update_publishers_button.setObjectName('welcome_screen_update_publishers_button')
    self.welcome_screen_update_publishers_button.setProperty('class', 'default_button')
    sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
    sizePolicy.setHorizontalStretch(0)
    sizePolicy.setVerticalStretch(0)
    sizePolicy.setHeightForWidth(self.welcome_screen_update_publishers_button.sizePolicy().hasHeightForWidth())
    self.welcome_screen_update_publishers_button.setSizePolicy(sizePolicy)
    self.welcome_screen_update_publishers_button.clicked.connect(lambda: welcome_screen_update_publishers_button_clicked(self))

    self.welcome_screen_vbox.addWidget(self.welcome_screen_update_publishers_button, 0, Qt.AlignCenter)

    self.welcome_screen_update_publishers_checked_line = QFrame(self.welcome_screen)
    self.welcome_screen_update_publishers_checked_line_hbox = QHBoxLayout(self.welcome_screen_update_publishers_checked_line)

    self.welcome_screen_update_publishers_checked_icon = QLabel(self.welcome_screen_update_publishers_checked_line)
    self.welcome_screen_update_publishers_checked_icon.setObjectName(u"welcome_screen_update_publishers_checked_icon")
    self.welcome_screen_update_publishers_checked_icon.setPixmap(QPixmap(get_graphics_path('check_icon.svg')))

    self.welcome_screen_update_publishers_checked_line_hbox.addWidget(self.welcome_screen_update_publishers_checked_icon, 0, Qt.AlignRight)

    self.welcome_screen_update_publishers_checked_label = QLabel(self.welcome_screen_update_publishers_checked_line)
    self.welcome_screen_update_publishers_checked_label.setObjectName(u"welcome_screen_update_publishers_checked_label")

    self.welcome_screen_update_publishers_checked_line_hbox.addWidget(self.welcome_screen_update_publishers_checked_label, 0, Qt.AlignLeft)

    self.welcome_screen_vbox.addWidget(self.welcome_screen_update_publishers_checked_line, 0, Qt.AlignCenter)

    self.welcome_screen_vbox.addStretch()

    self.stackedWidget.addWidget(self.welcome_screen)


def translate_widgets(self):
    self.welcome_screen_icon_label.setText(_(MODULE_INFO['title']))
    self.welcome_screen_update_congregation_info_button.setText(_('Update congregation info'))
    self.welcome_screen_language_checked_label.setText(_('Language selected') + ': {}'.format(_('language_name')))
    self.welcome_screen_update_congregation_info_checked_label.setText(_('Congregation info updated'))
    self.welcome_screen_update_settings_checked_label.setText(_('Settings adjusted'))
    self.welcome_screen_update_settings_button.setText(_('Adjust settings'))
    self.welcome_screen_update_publishers_checked_label.setText(_('List of publishers'))
    self.welcome_screen_update_publishers_button.setText(_('Add publishers'))


def update_welcome_screen(self):
    final_language_list = []
    lang_dict = get_available_language_names()
    for lang in lang_dict:
        final_language_list.append(lang_dict[lang])
    self.welcome_screen_language_combobox.clear()
    self.welcome_screen_language_combobox.addItems(final_language_list)
    self.welcome_screen_language_combobox.setCurrentText(lang_dict[self.selected_language])

    self.welcome_screen_language_combobox.setVisible(not bool(self.settings.get('language', '')))
    self.welcome_screen_language_checked_line.setVisible(bool(self.settings.get('language', '')))

    self.welcome_screen_update_congregation_info_button.setVisible(not bool(self.settings.get('congregation_name', '')))
    self.welcome_screen_update_congregation_info_checked_line.setVisible(bool(self.settings.get('congregation_name', '')))

    self.welcome_screen_update_settings_button.setVisible(not bool(self.settings))
    self.welcome_screen_update_settings_checked_line.setVisible(bool(self.settings))

    self.welcome_screen_update_publishers_button.setVisible(not bool(self.publishers_list))
    self.welcome_screen_update_publishers_checked_line.setVisible(bool(self.publishers_list))


def welcome_screen_update_congregation_info_button_clicked(self):
    self.stackedWidget.setCurrentWidget(self.congregation_page)


def welcome_screen_update_settings_button_clicked(self):
    modules_menu_button_settings_clicked(self)


def welcome_screen_update_publishers_button_clicked(self):
    modules_menu_button_publishers_clicked(self)


def apply_language(self):
    lang_dict = get_available_language_names()
    for lang in lang_dict:
        if lang_dict[lang] == self.welcome_screen_language_combobox.currentText():
            self.selected_language = lang
            break
    self.settings['language'] = self.selected_language
    set_language(self.selected_language)
    self.translate_widgets()