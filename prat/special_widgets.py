#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PySide6.QtWidgets import QLineEdit, QLabel, QComboBox, QWidget, QVBoxLayout, QHBoxLayout, QListWidget, QPushButton
from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QKeyEvent


class QLineEditDC(QLineEdit):
    def __init__(self, *args, **kwargs):
        super(QLineEditDC, self).__init__(*args, **kwargs)

    def mouseDoubleClickEvent(self, event):
        self.setReadOnly(not self.isReadOnly())
        self.setStyleSheet(self.styleSheet())


class QLabelDC(QLabel):
    doubleClicked = Signal()

    def __init__(self, *args, **kwargs):
        super(QLabelDC, self).__init__(*args, **kwargs)

    def mouseDoubleClickEvent(self, event):
        self.doubleClicked.emit()
        self.setStyleSheet(self.styleSheet())


class QComboBoxNoWheel(QComboBox):
    def __init__(self, scrollWidget=None, *args, **kwargs):
        super(QComboBoxNoWheel, self).__init__(*args, **kwargs)
        self.setEditable(True)
        self.scrollWidget = scrollWidget
        self.setFocusPolicy(Qt.StrongFocus)
        # self.setStyleSheet('QComboBox::drop-down { background-image: url("' + get_graphics_path('arrow_down.svg') + '"); }')

    def wheelEvent(self, *args, **kwargs):
        if self.hasFocus():
            return QComboBox.wheelEvent(self, *args, **kwargs)
        else:
            if self.scrollWidget:
                return self.scrollWidget.wheelEvent(*args, **kwargs)


class AssignmentsList(QWidget):
    item_changed = Signal()

    def __init__(widget, *args, **kwargs):
        super(AssignmentsList, widget).__init__(*args, **kwargs)
        widget.list = []

        widget.setLayout(QVBoxLayout())
        widget.layout().setContentsMargins(0, 0, 0, 0)

        class qlistwidget(QListWidget):
            def __init__(self, *args, **kwargs):
                super(qlistwidget, self).__init__(*args, **kwargs)

            def keyPressEvent(self, event):
                if event.key() in [Qt.Key_Delete, Qt.Key_Backspace]:
                    widget.list_item_activated()
                return super().keyPressEvent(event)

        widget.qlistwidget = qlistwidget()
        widget.qlistwidget.activated.connect(widget.list_item_activated)
        widget.layout().addWidget(widget.qlistwidget)

        # widget.buttons_hbox = QHBoxLayout()

        widget.new_name_qcombobox = QComboBoxNoWheel()
        widget.new_name_qcombobox.lineEdit().editingFinished.connect(lambda: widget.new_name_confirm_button_clicked())
        widget.new_name_qcombobox.currentTextChanged.connect(lambda: widget.new_name_confirm_button_clicked())
        # widget.buttons_hbox.addWidget(widget.new_name_qcombobox, 0, Qt.AlignLeft)
        widget.layout().addWidget(widget.new_name_qcombobox)

        # widget.new_name_confirm_button = QPushButton('Confirm')
        # widget.new_name_confirm_button.clicked.connect(lambda: widget.new_name_confirm_button_clicked())
        # widget.buttons_hbox.addWidget(widget.new_name_confirm_button, 0, Qt.AlignLeft)

        # widget.new_name_cancel_button = QPushButton('Cancel')
        # widget.new_name_cancel_button.clicked.connect(lambda: widget.update_content())
        # widget.buttons_hbox.addWidget(widget.new_name_cancel_button, 0, Qt.AlignLeft)

        # widget.add_button = QPushButton('Add')
        # widget.add_button.clicked.connect(lambda: widget.add_button_clicked())
        # widget.buttons_hbox.addWidget(widget.add_button, 0, Qt.AlignLeft)

        # widget.remove_button = QPushButton('Remove')
        # widget.remove_button.clicked.connect(lambda: widget.remove_button_clicked())
        # widget.buttons_hbox.addWidget(widget.remove_button, 0, Qt.AlignLeft)

        # widget.layout().addLayout(widget.buttons_hbox)

    def list_item_activated(widget):
        widget.remove_button_clicked()
        # widget.remove_button.setVisible(bool(widget.qlistwidget.currentItem()))

    def add_button_clicked(widget):
        widget.new_name_qcombobox.setVisible(True)
        widget.new_name_confirm_button.setVisible(True)
        widget.new_name_cancel_button.setVisible(True)
        widget.remove_button.setVisible(False)

    def new_name_confirm_button_clicked(widget):
        if widget.new_name_qcombobox.currentText() and not widget.new_name_qcombobox.currentText() in widget.list:
            widget.list.append(widget.new_name_qcombobox.currentText())
            widget.item_changed.emit()
            widget.update_content()

    def remove_button_clicked(widget):
        if widget.qlistwidget.currentItem():
            widget.list.remove(widget.qlistwidget.currentItem().text())
        widget.item_changed.emit()
        widget.update_content()

    def update_content(widget):
        widget.qlistwidget.clear()
        for item in widget.list:
            widget.qlistwidget.addItem(item)
        widget.qlistwidget.setMinimumHeight(widget.qlistwidget.sizeHintForRow(0) * widget.qlistwidget.count())
        # widget.list_item_activated()
        widget.new_name_qcombobox.setCurrentText('')
        # widget.new_name_qcombobox.setVisible(False)
        # widget.new_name_confirm_button.setVisible(False)
        # widget.new_name_cancel_button.setVisible(False)
        # widget.remove_button.setVisible(False)
        # widget.add_button.setVisible(True)

    def getList(widget):
        return widget.list

    def setList(widget, list):
        widget.list = list
        widget.update_content()

    def setItems(widget, items):
        widget.new_name_qcombobox.blockSignals(True)
        widget.new_name_qcombobox.clear()
        widget.new_name_qcombobox.addItems(items)
        widget.new_name_qcombobox.blockSignals(False)

    def clear(widget):
        widget.setItems([])
