# -*- mode: python -*-
block_cipher = None

a = Analysis(['prat.py'],
    pathex=['/Users/admin/Documents/prat'],
    binaries=[],
    datas=[
        ( 'graphics/*.png', 'graphics' ),
        ( 'documents/*', 'documents' ),
        ( 'modules/*', 'modules' ),
        ( 'indices/*', 'indices' ),
        ( 'resources/*.ttf', 'resources' ),
        ( 'resources/macOS/gpg2', 'resources' ),
        ( 'resources/macOS/gs', '.' ),
        ( 'resources/macOS/Init', 'Init' ),
        ( 'resources/macOS/Font', 'Font' ),
    ],
    hiddenimports=['PySide2.sip', 'exchangelib'],
    hookspath=[],
    runtime_hooks=[],
    excludes=[],
    win_no_prefer_redirects=False,
    win_private_assemblies=False,
    cipher=block_cipher)

pyz = PYZ(a.pure, a.zipped_data,
    cipher=block_cipher)

exe = EXE(pyz,
    a.scripts,
    exclude_binaries=True,
    name='Prat',
    debug=False,
    strip=False,
    upx=True,
    console=False )

coll = COLLECT(exe,
                a.binaries,
                a.zipfiles,
                a.datas,
                strip=False,
                upx=True,
                name='Prat')

app = BUNDLE(coll,
    name='Prat.app',
    icon='pixmaps/prat.icns',
    bundle_identifier='org.jonata.Prat',
    info_plist={
            'CFBundleShortVersionString': '18.07.4',
            'CFBundleVersion':'18.07.4'
    })
