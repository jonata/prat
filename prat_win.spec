# -*- mode: python -*-

a = Analysis(['prat/__main__.py'],
    pathex=['/Users/admin/Documents/prat'],
    binaries=[],
    datas=[
        ( 'prat/graphics/*', 'graphics' ),
        ( 'prat/documents/*', 'documents' ),
        ( 'prat/locale/*', 'locale' ),
    ],
    hiddenimports=[],
    hookspath=[],
    runtime_hooks=[] )

pyz = PYZ(a.pure)

exe = EXE(pyz,
    a.scripts,
    exclude_binaries=True,
    name='Prat.exe',
    strip=False,
    upx=True,
    console=False,
    debug=False,
    icon='prat/graphics/prat.ico' )

coll = COLLECT( exe,
                a.binaries,
                a.zipfiles,
                a.datas,
                strip=False,
                upx=True,
                name='Prat')
