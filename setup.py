#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#######################################################################
#
# Prat
#
#######################################################################

from setuptools import setup

from helpers import SetupHelpers
import prat

# --------------------------------------------------------------------------- #

setup_requires = ['setuptools']
install_requires = [
    'PySide6',
    'requests',
    'beautifulsoup4',
    'odfpy',
    'ebooklib',
    'python-i18n',
    'notify-py'
]

# --------------------------------------------------------------------------- #

try:
    # begin setuptools installer
    result = setup(
        name=prat.__appname__.lower(),
        version=prat.__version__,
        author=prat.__author__,
        author_email=prat.__email__,
        description='Theocratic Aid Software',
        long_description=SetupHelpers.get_description(),
        url=prat.__website__,
        license='Proprietary',
        packages=['prat', 'prat.modules'],
        setup_requires=setup_requires,
        install_requires=install_requires,
        data_files=SetupHelpers.get_data_files(),
        package_data=SetupHelpers.get_package_files(),
        include_package_data=True,
        entry_points={'gui_scripts': ['prat = prat.__main__:main']},
        keywords='prat',
        classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Environment :: X11 Applications :: Qt',
            'Intended Audience :: End Users/Desktop',
            'License :: Proprietary',
            'Natural Language :: English',
            'Operating System :: POSIX',
            'Topic :: Office',
            'Programming Language :: Python :: 3 :: Only'
        ]
    )
except BaseException:
    if prat.__ispypi__:
        SetupHelpers.pip_notes()
    raise
